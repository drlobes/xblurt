/**
 * @file Misc utility functions.
 * @author BeBlurt <https://beblurt.com/@beblurt>
 * @description adaptation from Johan Nordberg <code@johan-nordberg.com> Misc utility functions.
 * @license
 * Copyright (c) 2017 Johan Nordberg. All Rights Reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 *  1. Redistribution of source code must retain the above copyright notice, this
 *     list of conditions and the following disclaimer.
 *
 *  2. Redistribution in binary form must reproduce the above copyright notice,
 *     this list of conditions and the following disclaimer in the documentation
 *     and/or other materials provided with the distribution.
 *
 *  3. Neither the name of the copyright holder nor the names of its contributors
 *     may be used to endorse or promote products derived from this software without
 *     specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * You acknowledge that this software is not designed, licensed or intended for use
 * in the design, construction, operation or maintenance of any military facility.
 */

/* eslint-disable @typescript-eslint/tslint/config */
/* eslint-disable import/order */

import fetch from 'cross-fetch'
import { EventEmitter } from 'events'
import { PassThrough } from 'stream'

const JSBI = require('jsbi')
export const operationOrders = {
  vote: 0,
  comment: 1,

  transfer: 2,
  transfer_to_vesting: 3,
  withdraw_vesting: 4,

  account_create: 5,
  account_update: 6,

  witness_update: 7,
  account_witness_vote: 8,
  account_witness_proxy: 9,

  custom: 10,

  delete_comment: 11,
  custom_json: 12,
  comment_options: 13,
  set_withdraw_vesting_route: 14,
  claim_account: 15,
  create_claimed_account: 16,
  request_account_recovery: 17,
  recover_account: 18,
  change_recovery_account: 19,
  escrow_transfer: 20,
  escrow_dispute: 21,
  escrow_release: 22,
  escrow_approve: 23,
  transfer_to_savings: 24,
  transfer_from_savings: 25,
  cancel_transfer_from_savings: 26,
  custom_binary: 27,
  decline_voting_rights: 28,
  reset_account: 29,
  set_reset_account: 30,
  claim_reward_balance: 31,
  delegate_vesting_shares: 32,
  witness_set_properties: 33,
  create_proposal: 34,
  update_proposal_votes: 35,
  remove_proposal: 36,
  // virtual ops
  author_reward: 37,
  curation_reward: 38,
  comment_reward: 39,
  fill_vesting_withdraw: 40,
  shutdown_witness: 41,
  fill_transfer_from_savings: 42,
  hardfork: 43,
  comment_payout_update: 44,
  return_vesting_delegation: 45,
  comment_benefactor_reward: 46,
  producer_reward: 47,
  clear_null_account_balance: 48,
  proposal_pay: 49,
  sps_fund: 50,
  fee_pay: 51
}
export const virtualOps = {
  // virtual ops
  author_reward: 0,
  curation_reward: 1,
  comment_reward: 2,
  fill_vesting_withdraw: 3,
  shutdown_witness: 4,
  fill_transfer_from_savings: 5,
  hardfork: 6,
  comment_payout_update: 7,
  return_vesting_delegation: 8,
  comment_benefactor_reward: 9,
  producer_reward: 10,
  clear_null_account_balance: 11,
  proposal_pay: 12,
  sps_fund: 13,
  fee_pay: 14
}

const redFunction = ([low, high], allowedOperation: number): [typeof JSBI, any]|[any, typeof JSBI] => {
  if (allowedOperation < 64) {
    return [
      JSBI.bitwiseOr(
        low,
        JSBI.leftShift(JSBI.BigInt(1), JSBI.BigInt(allowedOperation))
      ),
      high
    ]
  } else {
    return [
      low,
      JSBI.bitwiseOr(
        high,
        JSBI.leftShift(JSBI.BigInt(1), JSBI.BigInt(allowedOperation - 64))
      )
    ]
  }
}

/**
 * Make bitmask filter to be used with getAccountHistory call
 * @param allowedOperations Array of operations index numbers
 */
export const makeBitMaskFilter = (allowedOperations: number[]): (number|null)[] => allowedOperations
  .reduce(redFunction, [JSBI.BigInt(0), JSBI.BigInt(0)])
  .map(value =>
    JSBI.notEqual(value, JSBI.BigInt(0)) ? parseInt(value.toString(), 10) : null
)

/**
 * Make bitmask filter to be used with enum_virtual_ops call
 * @param allowedOperations Array of operations index numbers
 */
export const makeBitwiseFilter = (allowedOperations: number[]): number => {
  const [low, high] = allowedOperations.reduce(redFunction, [JSBI.BigInt(0), JSBI.BigInt(0)])
  const value = JSBI.bitwiseOr(low, high)
  return JSBI.notEqual(value, JSBI.BigInt(0)) ? parseInt(value.toString(), 10) : 0
}

// TODO: Add more errors that should trigger a failover
const timeoutErrors = ['timeout', 'ENOTFOUND', 'ECONNREFUSED', 'database lock', 'CERT_HAS_EXPIRED', 'EHOSTUNREACH']

/** Return a promise that will resove when a specific event is emitted. */
export const waitForEvent = <T>(emitter: EventEmitter, eventName: string | symbol): Promise<T> => new Promise(resolve => {
  emitter.once(eventName, resolve)
})

/** Sleep for N milliseconds. */
export const sleep = (ms: number): Promise<void> => new Promise<void>(resolve => { setTimeout(resolve, ms) })

/** Return a stream that emits iterator values. */
export const iteratorStream = <T>(iterator: AsyncIterableIterator<T>): NodeJS.ReadableStream => {
  const stream = new PassThrough({ objectMode: true })
  const iterate = async (): Promise<any> => {
    for await (const item of iterator) {
      if (!stream.write(item)) {
        await waitForEvent(stream, 'drain')
      }
    }
  }
  iterate()
    .then(() => {
      stream.end()
    })
    .catch(error => {
      stream.emit('error', error)
      stream.end()
    })
  return stream
}

/** Return a deep copy of a JSON-serializable object */
export const copy = <T>(object: T): T => JSON.parse(JSON.stringify(object))

/** Fetch API wrapper that retries until timeout is reached. */
const failover = (url: string, urls: string[], currentAddress: string, consoleOnFailover: boolean): string => {
  const index = urls.indexOf(url)
  const targetUrl = urls.length === index + 1 ? urls[0] : urls[index + 1]
  // eslint-disable-next-line no-console
  if (consoleOnFailover) { console.log(`Switched Blurt RPC: ${targetUrl} (previous: ${currentAddress})`) }
  return targetUrl ? targetUrl : url
}

export const retryingFetch = async (
  currentAddress: string,
  allAddresses: string | string[],
  opts: any,
  timeout: number,
  failoverThreshold: number,
  consoleOnFailover: boolean,
  backoff: (tries: number) => number,
  fetchTimeout?: (tries: number) => number
): Promise<{ response: any; currentAddress: string }> => {
  let start = Date.now()
  let tries = 0
  let round = 0

  do {
    try {
      if (fetchTimeout) {
        opts.timeout = fetchTimeout(tries)
      }
      const response = await fetch(currentAddress, opts)
      if (!response.ok) {
        throw new Error(`HTTP ${response.status}: ${response.statusText}`)
      }
      return { response: await response.json(), currentAddress }
    } catch (error) {
      if (timeout !== 0 && Date.now() - start > timeout) {
        if ((!error || !error.code) && Array.isArray(allAddresses)) {
          // If error is empty or not code is present, it means rpc is down => switch
          currentAddress = failover(
            currentAddress,
            allAddresses,
            currentAddress,
            consoleOnFailover
          )
        } else {
          const isFailoverError =
            timeoutErrors.filter(
              fe => error && error.code && error.code.includes(fe)
            ).length > 0
          if (
            isFailoverError &&
            Array.isArray(allAddresses) &&
            allAddresses.length > 1
          ) {
            if (round < failoverThreshold) {
              start = Date.now()
              tries = -1
              if (failoverThreshold > 0) {
                round++
              }
              currentAddress = failover(
                currentAddress,
                allAddresses,
                currentAddress,
                consoleOnFailover
              )
            } else {
              error.message = `[${
                error.code
              }] tried ${failoverThreshold} times with ${allAddresses.join(
                ','
              )}`
              throw error
            }
          } else {
            // eslint-disable-next-line no-console
            console.error(
              `Didn't failover for error ${error.code ? 'code' : 'message'}: [${
                error.code || error.message
              }]`
            )
            throw error
          }
        }
      }
      await sleep(backoff(tries++))
    }
  } while (true)
}

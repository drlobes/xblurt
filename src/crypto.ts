/**
 * @file Blurt crypto helpers.
 * @author BeBlurt <https://beblurt.com/@beblurt>
 * @description adaptation from Johan Nordberg <code@johan-nordberg.com> crypto helpers.
 * @license
 * Copyright (c) 2017 Johan Nordberg. All Rights Reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 *  1. Redistribution of source code must retain the above copyright notice, this
 *     list of conditions and the following disclaimer.
 *
 *  2. Redistribution in binary form must reproduce the above copyright notice,
 *     this list of conditions and the following disclaimer in the documentation
 *     and/or other materials provided with the distribution.
 *
 *  3. Neither the name of the copyright holder nor the names of its contributors
 *     may be used to endorse or promote products derived from this software without
 *     specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * You acknowledge that this software is not designed, licensed or intended for use
 * in the design, construction, operation or maintenance of any military facility.
 */
import assert from 'assert'
import { createCipheriv, createDecipheriv, createHash } from 'crypto'

import bs58 from 'bs58'
import bigInteger from 'bigi'

// eslint-disable-next-line import/order
const ByteBuffer = require('bytebuffer')
const Long = ByteBuffer.Long

import * as secp256k1 from 'secp256k1'
import { Point, getCurveByName } from 'ecurve'
import { VError } from 'verror'
import { Types } from './chain/serializer'
import { EncryptedMemoDeserializer } from './chain/deserializer'
import { SignedTransaction, Transaction } from './chain/transaction'
import { DEFAULT_ADDRESS_PREFIX, DEFAULT_CHAIN_ID } from './client'
import { copy } from './utils'


/** Network id used in WIF-encoding */
const NETWORK_ID = Buffer.from([0x80])

/** Regexp for a graphene account */
const regExpAccount = /^(?=.{3,16}$)[a-z][0-9a-z\-]{1,}[0-9a-z]([\.][a-z][0-9a-z\-]{1,}[0-9a-z]){0,}$/

/** Regexp for a graphene account with @ */
const regExpAtAccount = /^@(?=.{3,16}$)[a-z][0-9a-z\-]{1,}[0-9a-z]([\.][a-z][0-9a-z\-]{1,}[0-9a-z]){0,}$/

/** From Buffer to Uint8Array */
const toUint8Array = (buf: Buffer): Uint8Array => {
  const ab   = new ArrayBuffer(buf.length)
  const view = new Uint8Array(ab)
  for (let i = 0; i < buf.length; ++i) {
    view[i] = buf[i]
  }
  return view
}

/** From ArrayBuffer to Buffer */
const toBuffer = (ab: ArrayBuffer): Buffer => {
  const buf  = Buffer.alloc(ab.byteLength)
  const view = new Uint8Array(ab)
  for (let i = 0; i < buf.length; ++i) {
      buf[i] = view[i]
  }
  return buf
}

/**
 * Converts a string, number or Long to a ByteBuffer object.
 *
 * @param o - The value to be converted.
 * @returns A ByteBuffer object.
 * @throws An error if the input is not a string, number or Long.
 */
const toByteBuffer = (o: string | number | typeof Long): typeof Long => {
  if (typeof o === 'string') {
    return Long.fromString(o)
  } else if (typeof o === 'number') {
    return Long.fromNumber(o)
  } else if (o instanceof ByteBuffer.Long) {
    return o
  } else {
    throw new Error('Input is not a string, number or Long')
  }
}

/**
 * Generates a unique 64 bit unsigned number string. Being time based,
 * this is careful to never choose the same nonce twice. This value could
 * be recorded in the blockchain for a long time.
 * @returns The unique nonce.
 */
let unique_nonce_entropy: number = Math.floor(Math.random() * 0xFFFF)
const uniqueNonce = (): string => {
  let long = BigInt(Date.now())
  const entropy = ++unique_nonce_entropy % 0xFFFF
  if (entropy === 0) {
    const last = Number(long >> BigInt(16))
    const now = Date.now()
    if (now <= last) { long += BigInt(1) }
    unique_nonce_entropy = 0
  }
  long = (long << BigInt(16)) | BigInt(entropy)
  return long.toString(16).padStart(16, '0')
}

/** Return ripemd160 hash of input */
const ripemd160 = (input: Buffer | string): Buffer => createHash('ripemd160').update(input).digest()

/** Return sha256 hash of input */
const sha256 = (input: Buffer | string): Buffer => createHash('sha256').update(input).digest()

/** Return 2-round sha256 hash of input */
const doubleSha256 = (input: Buffer | string): Buffer => sha256(sha256(input))

/** Encode public key with bs58+ripemd160-checksum */
const encodePublic = (key: Buffer, prefix: string): string => {
  const checksum = ripemd160(key)
  return prefix + bs58.encode(Buffer.concat([key, toUint8Array(checksum).slice(0, 4)]))
}

/** Decode bs58+ripemd160-checksum encoded public key */
const decodePublic = (encodedKey: string): { key: Buffer; prefix: string } => {
  const prefix     = encodedKey.slice(0, 3)
  encodedKey       = encodedKey.slice(3)
  const uint8Array = bs58.decode(encodedKey)
  const checksum   = uint8Array.slice(-4)
  const key        = uint8Array.slice(0, -4)
  const bufKey     = Buffer.from(key)
  const checksumVerify = toUint8Array(ripemd160(bufKey)).slice(0, 4)
  if (Buffer.compare(toBuffer(checksumVerify), toBuffer(checksum)) !== 0) {
    throw new Error('public key checksum mismatch')
  }
  return { key: bufKey, prefix }
}

/** Encode bs58+doubleSha256-checksum private key */
const encodePrivate = (key: Buffer): string => {
  assert.strictEqual(key.readUInt8(0), 0x80, 'private key network id mismatch')
  const checksum = doubleSha256(key)
  return bs58.encode(Buffer.concat([key, toUint8Array(checksum).slice(0, 4)]))
}

/** Decode bs58+doubleSha256-checksum encoded private key */
const decodePrivate = (encodedKey: string): Buffer => {
  const uint8Array = bs58.decode(encodedKey)
  const toCompare  = toBuffer(uint8Array.slice(0, 1))
  if (Buffer.compare(NETWORK_ID, toCompare) !== 0) {
    throw new Error('private key network id mismatch')
  }
  const checksum = uint8Array.slice(-4)
  const key      = uint8Array.slice(0, -4)
  const bufKey   = Buffer.from(key)
  const dSha256  = sha256(sha256(bufKey))
  const checksumVerify = toUint8Array(dSha256).slice(0, 4)
  if (Buffer.compare(toBuffer(checksumVerify), toBuffer(checksum)) !== 0) {
    throw new Error('private key checksum mismatch')
  }
  return bufKey
}

/** Return true if signature is canonical, otherwise false */
const isCanonicalSignature = (signature: Buffer): boolean => (
  !(signature[0] & 0x80) &&
  !(signature[0] === 0 && !(signature[1] & 0x80)) &&
  !(signature[32] & 0x80) &&
  !(signature[32] === 0 && !(signature[33] & 0x80))
)

/** Return true if string is wif, otherwise false */
const isWif = (privWif: string): boolean => {
  try {
      const bufWif = Buffer.from(bs58.decode(privWif))
      const privKey = toUint8Array(bufWif).slice(0, -4)
      const checksum = toUint8Array(bufWif).slice(-4)
      let newChecksum = sha256(toBuffer(privKey))
      newChecksum = sha256(newChecksum)
      newChecksum = toBuffer(toUint8Array(newChecksum).slice(0, 4))
      return (checksum.toString() === newChecksum.toString())
  } catch (e) {
      return false
  }
}

/**
 * ECDSA (secp256k1) public key.
 */
export class PublicKey {
  public readonly uncompressed: Buffer

  constructor(
    public readonly key: Buffer,
    public readonly prefix = DEFAULT_ADDRESS_PREFIX
  ) {
    assert(secp256k1.publicKeyVerify(key), 'invalid public key')
    this.uncompressed = Buffer.from(secp256k1.publicKeyConvert(key, false))
  }

  /**
   * Create a new instance from a WIF-encoded key.
   */
  public static fromString(wif: string): PublicKey {
    const { key, prefix } = decodePublic(wif)
    return new PublicKey(key, prefix)
  }

  public static fromBuffer(key: Buffer): { key: Buffer } {
    assert(secp256k1.publicKeyVerify(key), 'invalid buffer as public key')
    return { key }
  }


  /**
   * Create a new instance.
   */
  public static from(value: string | PublicKey): PublicKey {
    if (value instanceof PublicKey) {
      return value
    } else {
      return PublicKey.fromString(value)
    }
  }

  /**
   * Verify a 32-byte signature.
   * @param message 32-byte message to verify.
   * @param signature Signature to verify.
   */
  public verify(message: Buffer, signature: Signature): boolean {
    return secp256k1.ecdsaVerify(signature.data, message,  this.key)
  }

  /**
   * Return a WIF-encoded representation of the key.
   */
  public toString(): string {
    return encodePublic(this.key, this.prefix)
  }

  /**
   * Return JSON representation of this key, same as toString().
   */
  public toJSON(): string {
    return this.toString()
  }

  /**
   * Used by `utils.inspect` and `console.log` in node.js.
   */
  public inspect(): string {
    return `PublicKey: ${ this.toString() }`
  }
}

export type KeyRole = 'owner' | 'active' | 'posting' | 'memo'

/**
 * ECDSA (secp256k1) private key.
 */
export class PrivateKey {
  constructor(private key: Buffer) {
    assert(secp256k1.privateKeyVerify(key), 'invalid private key')
  }

  /**
   * Convenience to create a new instance from WIF string or buffer.
   */
  public static from(value: string | Buffer): PrivateKey {
    if (typeof value === 'string') {
      return PrivateKey.fromString(value)
    } else {
      return new PrivateKey(value)
    }
  }

  /**
   * Create a new instance from a WIF-encoded key.
   */
  public static fromString(wif: string): PrivateKey {
    const decoded = toUint8Array(decodePrivate(wif)).slice(1)
    return new PrivateKey(toBuffer(decoded))
  }

  /**
   * Create a new instance from a seed.
   */
  public static fromSeed(seed: string): PrivateKey {
    return new PrivateKey(sha256(seed))
  }

  /**
   * Create key from username and password.
   */
  public static fromLogin(
    username: string,
    password: string,
    role: KeyRole = 'active'
  ): PrivateKey {
    const seed = username + role + password
    return PrivateKey.fromSeed(seed)
  }

  /**
   * Sign message.
   * @param message 32-byte message.
   */
  public sign(message: Buffer): Signature {
    let rv: { signature: Uint8Array; recid: number }
    let attempts = 0
    do {
      const options = {
        data: sha256(Buffer.concat([message, Buffer.alloc(1, ++attempts)]))
      }
      rv = secp256k1.ecdsaSign(message, this.key, options)
    } while (!isCanonicalSignature(toBuffer(rv.signature)))
    return new Signature(toBuffer(rv.signature), rv.recid)
  }

  /**
   * Derive the public key for this private key.
   */
  public createPublic(prefix?: string): PublicKey {
    return new PublicKey(toBuffer(secp256k1.publicKeyCreate(this.key)), prefix)
  }

  /**
   * Return a WIF-encoded representation of the key.
   */
  public toString(): string {
    return encodePrivate(Buffer.concat([NETWORK_ID, this.key]))
  }

  /**
   * Get shared secret for memo cryptography
   */
  public getSharedSecret(publicKey: PublicKey): Buffer {
    const KBP = Point.fromAffine(
      getCurveByName('secp256k1'),
      bigInteger.fromBuffer(publicKey.uncompressed.slice(1, 33)),
      bigInteger.fromBuffer(publicKey.uncompressed.slice(33, 65))
    )
    const P = KBP.multiply(bigInteger.fromBuffer(this.key))
    const S = P.affineX.toBuffer(32)

    return createHash('sha512').update(S).digest()
  }

  /**
   * Used by `utils.inspect` and `console.log` in node.js. Does not show the full key
   * to get the full encoded key you need to explicitly call {@link toString}.
   */
  public inspect(): string {
    const key = this.toString()
    return `PrivateKey: ${ key.slice(0, 6) }...${ key.slice(-6) }`
  }
}

/**
 * ECDSA (secp256k1) signature.
 */
export class Signature {
  constructor(public data: Buffer, public recovery: number) {
    assert.strictEqual(data.length, 64, 'invalid signature')
  }

  public static fromBuffer(buffer: Buffer): Signature {
    assert.strictEqual(buffer.length, 65, 'invalid signature')
    const recovery = buffer.readUInt8(0) - 31
    const data = toUint8Array(buffer).slice(1)
    return new Signature(toBuffer(data), recovery)
  }

  public static fromString(string: string): Signature {
    return Signature.fromBuffer(Buffer.from(string, 'hex'))
  }

  /**
   * Recover public key from signature by providing original signed message.
   * @param message 32-byte message that was used to create the signature.
   */
  public recover(message: Buffer, prefix?: string): PublicKey {
    return new PublicKey(
      toBuffer(secp256k1.ecdsaRecover(this.data, this.recovery, message)),
      prefix
    )
  }

  public toBuffer(): Buffer {
    const buffer = Buffer.alloc(65)
    buffer.writeUInt8(this.recovery + 31, 0)
    this.data.copy(buffer, 1)
    return buffer
  }

  public toString(): string {
    return this.toBuffer().toString('hex')
  }
}

/**
 * Return the sha256 transaction digest.
 * @param chainId The chain id to use when creating the hash.
 */
 const transactionDigest = (
  transaction: Transaction | SignedTransaction,
  chainId: Buffer = DEFAULT_CHAIN_ID
): Buffer => {
  const buffer = new ByteBuffer(
    ByteBuffer.DEFAULT_CAPACITY,
    ByteBuffer.LITTLE_ENDIAN
  )
  try {
    Types.Transaction(buffer, transaction)
  } catch (cause) {
    throw new VError(
      { cause, name: 'SerializationError' },
      'Unable to serialize transaction'
    )
  }
  buffer.flip()

  const transactionData = Buffer.from(buffer.toBuffer())
  const digest = sha256(Buffer.concat([chainId, transactionData]))
  return digest
}

/**
 * Return copy of transaction with signature appended to signatures array.
 * @param transaction Transaction to sign.
 * @param keys Key(s) to sign transaction with.
 * @param options Chain id and address prefix, compatible with {@link Client}.
 */
// eslint-disable-next-line max-len
const signTransaction = (transaction: Transaction, keys: any, chainId: Buffer = DEFAULT_CHAIN_ID): SignedTransaction => {
  const digest = transactionDigest(transaction, chainId)
  const signedTransaction = copy(transaction) as SignedTransaction
  if (!signedTransaction.signatures) {
    signedTransaction.signatures = []
  }

  if (!Array.isArray(keys)) {
    keys = [keys]
  }
  for (const key of keys) {
    const signature = key.sign(digest)
    signedTransaction.signatures.push(signature.toString())
  }

  return signedTransaction
}

const generateTrxId = (transaction: Transaction): string => {
  const buffer = new ByteBuffer(
    ByteBuffer.DEFAULT_CAPACITY,
    ByteBuffer.LITTLE_ENDIAN
  )
  try {
    Types.Transaction(buffer, transaction)
  } catch (cause) {
    throw new VError(
      { cause, name: 'SerializationError' },
      'Unable to serialize transaction'
    )
  }
  buffer.flip()
  const transactionData = Buffer.from(buffer.toBuffer())
  return cryptoUtils.sha256(transactionData).toString('hex').slice(0, 40)
}

/**
 * Memo Encode/Decode
 */

/**
 * remove varint length prefix
 *
 * @param decryptedMessage - Buffer of the decrypted message
 * @returns the decrypted message minus the varint length prefix
 */
const removeVarintLengthPrefix = (decryptedMessage: Buffer): string => {
  const mbuf = ByteBuffer.fromBinary(decryptedMessage.toString('binary'), ByteBuffer.LITTLE_ENDIAN)
  try {
      mbuf.mark()
      return '#' + mbuf.readVString()
  } catch (e) {
      mbuf.reset()
      // Sender did not length-prefix the memo
      const memo = Buffer.from(mbuf.toString('binary'), 'binary').toString('utf-8')
      return '#' + memo
  }
}

/**
 * Decrypts an encrypted memo.
 *
 * @param memo - The encrypted memo to decrypt (need to start with #).
 * @param receiverPrivateMemoKey - The private Memo key of the recipient.
 * @returns The decrypted message.
 */
export const decodeMemo = (memo: string, receiverPrivateMemoKey: PrivateKey | string): string => {
  try {
    assert(typeof receiverPrivateMemoKey === 'string' || receiverPrivateMemoKey.toString(), 'Invalid Receiver private MEMO key!')
    if (!memo.startsWith('#')) { return memo }

    const privateKey = typeof receiverPrivateMemoKey === 'string' ? PrivateKey.from(receiverPrivateMemoKey) : receiverPrivateMemoKey

    memo = memo.substring(1)
    const memoBuff = bs58.decode(memo)
    const deserialized = EncryptedMemoDeserializer(Buffer.from(memoBuff))
    // const { from, to, nonce, check, encrypted } = deserialized
    const { from, nonce, check, encrypted } = deserialized

    const publicKey = new PublicKey(from.key)
    const nonceLong = toByteBuffer(nonce)

    // Appending nonce to buffer "ebuf" and rehash with sha512
    const S = privateKey.getSharedSecret(publicKey)
    let ebuf: any = new ByteBuffer(ByteBuffer.DEFAULT_CAPACITY, ByteBuffer.LITTLE_ENDIAN)
    ebuf.writeUint64(nonceLong)
    ebuf.append(S.toString('binary'), 'binary')
    ebuf = Buffer.from(ebuf.copy(0, ebuf.offset).toBinary(), 'binary')
    const encryption_key = createHash('sha512').update(ebuf).digest()
    const iv = encryption_key.slice(32, 48)
    const tag = encryption_key.slice(0, 32)

    // check if first 64 bit of sha256 hash treated as uint64_t truncated to 32 bits.
    let checksum: Buffer | Number = createHash('sha256').update(encryption_key).digest()
    checksum = checksum.slice(0, 4)
    const cbuf: any = ByteBuffer.fromBinary(checksum.toString('binary'), ByteBuffer.LITTLE_ENDIAN)
    checksum = cbuf.readUint32() as Number

    assert(check === checksum, 'Invalid nonce!')

    const binaryMessage = toUint8Array(encrypted)
    const decipher = createDecipheriv('aes-256-cbc', tag, iv)
    const decryptedMessage = Buffer.concat([decipher.update(binaryMessage), decipher.final()])

    return removeVarintLengthPrefix(decryptedMessage)
  } catch (e) {
    throw e
  }
}

/**
 * Encrypt a memo.
 *
 * @param memo - The memo to encrypt (need to start with #).
 * @param senderPrivateMemoKey - The private Memo key of the sender.
 * @param receiverPublicMemoKey - The publicKey Memo key of the recipient.
 * @returns The encrypted message.
 */
export const encodeMemo = (memo: string, senderPrivateMemoKey: PrivateKey | string, receiverPublicMemoKey: PublicKey | string): string => {
  try {
    assert(typeof senderPrivateMemoKey === 'string' || senderPrivateMemoKey.toString(), 'Invalid Sender private MEMO key!')
    assert(typeof receiverPublicMemoKey === 'string' || receiverPublicMemoKey.toString(), 'Invalid Receiver public MEMO key!')
    if (!memo.startsWith('#')) { return memo }

    const privateKey = typeof senderPrivateMemoKey === 'string' ? PrivateKey.from(senderPrivateMemoKey) : senderPrivateMemoKey
    const publicKey = typeof receiverPublicMemoKey === 'string' ? PublicKey.from(receiverPublicMemoKey) : receiverPublicMemoKey

    memo = memo.substring(1)
    const mbuf = new ByteBuffer(ByteBuffer.DEFAULT_CAPACITY, ByteBuffer.LITTLE_ENDIAN)
    mbuf.writeVString(memo)
    const memoBuff = Buffer.from(mbuf.flip().toBinary())

    const nonceLong = toByteBuffer(uniqueNonce())
    // Appending nonce to buffer "ebuf" and rehash with sha512
    const S = privateKey.getSharedSecret(publicKey)
    let ebuf: any = new ByteBuffer(ByteBuffer.DEFAULT_CAPACITY, ByteBuffer.LITTLE_ENDIAN)
    ebuf.writeUint64(nonceLong)
    ebuf.append(S.toString('binary'), 'binary')
    ebuf = Buffer.from(ebuf.copy(0, ebuf.offset).toBinary(), 'binary')
    const encryption_key = createHash('sha512').update(ebuf).digest()
    const iv = encryption_key.slice(32, 48)
    const tag = encryption_key.slice(0, 32)

    // check if first 64 bit of sha256 hash treated as uint64_t truncated to 32 bits.
    let check: Buffer | Number = createHash('sha256').update(encryption_key).digest()
    check = check.slice(0, 4)
    const cbuf: any = ByteBuffer.fromBinary(check.toString('binary'), ByteBuffer.LITTLE_ENDIAN)
    check = cbuf.readUint32() as Number

    let message = toUint8Array(memoBuff)
    const cipher = createCipheriv('aes-256-cbc', tag, iv)
    message = Buffer.concat([cipher.update(message), cipher.final()])

    const lbuf = new ByteBuffer(ByteBuffer.DEFAULT_CAPACITY, ByteBuffer.LITTLE_ENDIAN)
    Types.Memo(lbuf, {
        check,
        encrypted: message,
        from: privateKey.createPublic(),
        nonce: nonceLong,
        to: publicKey
    })
    lbuf.flip()
    const data = Buffer.from(lbuf.toBuffer())
    return '#' + bs58.encode(data)
  } catch (e) {
    throw e
  }
}

/** Misc crypto utility functions. */
export const cryptoUtils = {
  decodePrivate,
  doubleSha256,
  encodePrivate,
  encodePublic,
  generateTrxId,
  isCanonicalSignature,
  isWif,
  regExpAccount,
  regExpAtAccount,
  ripemd160,
  sha256,
  signTransaction,
  toBuffer,
  toByteBuffer,
  toUint8Array,
  transactionDigest,
  uniqueNonce
}

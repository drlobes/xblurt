/**
 * @file Blurt Database API helpers.
 * @author BeBlurt <https://beblurt.com/@beblurt>
 * @description adaptation from Johan Nordberg <code@johan-nordberg.com> Database API helpers.
 * @license
 * Copyright (c) 2017 Johan Nordberg. All Rights Reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 *  1. Redistribution of source code must retain the above copyright notice, this
 *     list of conditions and the following disclaimer.
 *
 *  2. Redistribution in binary form must reproduce the above copyright notice,
 *     this list of conditions and the following disclaimer in the documentation
 *     and/or other materials provided with the distribution.
 *
 *  3. Neither the name of the copyright holder nor the names of its contributors
 *     may be used to endorse or promote products derived from this software without
 *     specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * You acknowledge that this software is not designed, licensed or intended for use
 * in the design, construction, operation or maintenance of any military facility.
 */

import { Account } from '../chain/account'
import { RpcNodeConfig } from '../chain/misc'

import { Client } from './../client'

export interface WitnessVotes {id: number; witness: string; account: string}

export class DatabaseAPI {
  constructor(readonly client: Client) {}

  /** Convenience for calling `database_api`. */
   public call(method: string, params?: any[]|{ [key: string]: any }): Promise<any> {
    return this.client.call('database_api', method, params)
  }

  /** Return server config. */
  public getConfig(): Promise<RpcNodeConfig> {
    return this.call('get_config', {})
  }

  /**
   * Return a list of accounts.
   * @param start String or array for pagination
   * @param limit Number of results, max 1000
   * @param order by_next_vesting_withdrawal: in this case start is a date eg: ["1970-01-01T00:00:00", ""]
   * @param delayed_votes_active Default true (optional)
   */
  public getListAccounts(start: string|string[], limit: number, order: 'by_name'|'by_proxy'|'by_next_vesting_withdrawal', delayed_votes_active = true): Promise<{accounts: Account[]}> {
    return this.call('list_accounts', { start, limit, order, delayed_votes_active })
  }

  /**
   * Returns a list of witness votes.
   * @param start Depends on order (see below)
   * @param limit Number of results, max 1000
   * @param order by_account_witness: start is an array of two values: account, witness |
   *              by_witness_account: start is an array of two values: witness, account
   * @example
   * Voters of a witness
   * ```typescript
   * const accounts = await client.database.getListWitnessVotes(['nalexadre', null], 10, 'by_witness_account')
   * ```
   */
  public getListWitnessVotes(start: [string|null, string|null], limit: number, order: 'by_account_witness'|'by_witness_account'): Promise<{votes: WitnessVotes[]}> {
    return this.call('list_witness_votes', { start, limit, order })
  }

  /** return rpc node version */
  public async getVersion(): Promise<object> {
    return this.call('get_version', [])
  }
}

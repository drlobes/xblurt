/**
 * @file Blurt Broadcast API helpers.
 * @author BeBlurt <https://beblurt.com/@beblurt>
 * @description adaptation from Johan Nordberg <code@johan-nordberg.com> Broadcast API helpers.
 * @license
 * Copyright (c) 2017 Johan Nordberg. All Rights Reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 *  1. Redistribution of source code must retain the above copyright notice, this
 *     list of conditions and the following disclaimer.
 *
 *  2. Redistribution in binary form must reproduce the above copyright notice,
 *     this list of conditions and the following disclaimer in the documentation
 *     and/or other materials provided with the distribution.
 *
 *  3. Neither the name of the copyright holder nor the names of its contributors
 *     may be used to endorse or promote products derived from this software without
 *     specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * You acknowledge that this software is not designed, licensed or intended for use
 * in the design, construction, operation or maintenance of any military facility.
 */


import {
  AccountCreateOperation,
  AccountUpdateOperation,
  AccountWitnessVoteOperation,
  CancelTransferFromSavingsOperation,
  ChangeRecoveryAccountOperation,
  ClaimAccountOperation,
  ClaimRewardBalanceOperation,
  CommentOperation,
  CommentOptionsOperation,
  CreateClaimedAccountOperation,
  CustomJsonOperation,
  DelegateVestingSharesOperation,
  Operation,
  TransferOperation,
  VoteOperation
} from '../chain/operation'
import {
  NexusUpdateProps
} from '../chain/nexus'
import {
  SignedTransaction,
  Transaction,
  TransactionConfirmation
} from '../chain/transaction'
import { Client } from '../client'
import { cryptoUtils, PrivateKey } from '../crypto'

export class BroadcastAPI {
  /**
   * How many milliseconds in the future to set the expiry time to when
   * broadcasting a transaction, defaults to 1 minute.
   */
  public expireTime = 120 * 1000

  constructor(readonly client: Client) {}

  /** Convenience for calling `condenser_api`. */
  public call(method: string, params?: any[]): Promise<any> {
    return this.client.call('condenser_api', method, params)
  }

  /**
   * Create account.
   * Broadcasted to the blockchain to create a new account.
   * @param data The account_create payload. See {@link AccountCreateOperation}
   * @param key The Active or Owner private key of the account creator.
   */
  public async accountCreate(data: AccountCreateOperation[1], key: PrivateKey): Promise<TransactionConfirmation> {
    const op: Operation = ['account_create', data]
    return this.sendOperations([op], key)
  }

  /**
   * Update account.
   * Updates account information.
   * @param data The account_update payload. See {@link AccountUpdateOperation}
   * @param key The Active or Owner private key of the account.
   */
  public async accountUpdate(data: AccountUpdateOperation[1], key: PrivateKey): Promise<TransactionConfirmation> {
    const op: Operation = ['account_update', data]
    return this.sendOperations([op], key)
  }

  /**
   * Witness vote.
   * Vote from an account to a witness.
   * @param data The account_witness_vote payload. See {@link AccountWitnessVoteOperation}
   * @param key The private key of the account, should be the Active key at least.
   * @remarks Since HF 0.8 the formula VS/N where VP is the Vesting Share of the account and N the number of witnesses upvoted by the account is applied.
   */
  public async accountWitnessVote(data: AccountWitnessVoteOperation[1], key: PrivateKey): Promise<TransactionConfirmation> {
    const op: Operation = ['account_witness_vote', data]
    return this.sendOperations([op], key)
  }

  /**
   * Cancel transfer from saving.
   * Funds withdrawals from the savings can be canceled at any time before it is executed.
   * @param data The cancel_transfer_from_savings payload. See {@link CancelTransferFromSavingsOperation}
   * @param key The private key of the account, should be the Active key at least.
   */
  public async cancelTransferFromSavings(data: CancelTransferFromSavingsOperation[1], key: PrivateKey): Promise<TransactionConfirmation> {
    const op: Operation = ['cancel_transfer_from_savings', data]
    return this.sendOperations([op], key)
  }

  /**
   * Change recovery account.
   * Funds withdrawals from the savings can be canceled at any time before it is executed.
   * @param data The change_recovery_account payload. See {@link ChangeRecoveryAccountOperation}
   * @param key The Owner private key of the account.
   */
  public async changeRecoveryAccount(data: ChangeRecoveryAccountOperation[1], key: PrivateKey): Promise<TransactionConfirmation> {
    const op: Operation = ['change_recovery_account', data]
    return this.sendOperations([op], key)
  }

  /**
   * Claim account.
   * Requests from users or dApps to get an account creation ticket in order to create new accounts.
   * These operations are issued before the actual creation of the account on the blockchain.
   * @param data The claim_account payload. See {@link ClaimAccountOperation}
   * @param key The Active or Owner private key of the account.
   * @remarks Deactivated Since HF 0.2
   */
  public async claimAccount(data: ClaimAccountOperation[1], key: PrivateKey): Promise<TransactionConfirmation> {
    const op: Operation = ['claim_account', data]
    return this.sendOperations([op], key)
  }

  /**
   * Claim reward balance.
   * Author and curator rewards are not automatically transferred to the account’s balances.
   * One has to issue a `claim_reward_balance` operation to trigger the transfer from the reward pool to its balance.
   * @param data The claim_reward_balance payload. See {@link ClaimRewardBalanceOperation}
   * @param key The Posting, Active or Owner private key of the account.
   */
  public async claimRewardBalance(data: ClaimRewardBalanceOperation[1], key: PrivateKey): Promise<TransactionConfirmation> {
    const op: Operation = ['claim_reward_balance', data]
    return this.sendOperations([op], key)
  }

  /**
   * Comment.
   * Creates a post/comment.
   * @param data The comment payload. See {@link CommentOperation}
   * @param key The Posting, Active or Owner private key of the account.
   * @param options The comment_options payload [optional]. See {@link CommentOptionsOperation}
   * @remarks Rules:
   * - The “title” must not be longer than 256 bytes
   * - The “title” must be UTF-8
   * - The “body” must be larger than 0 bytes
   * - The “body” much also be UTF-8
   * @remarks json_metadata: There is no blockchain enforced validation on `json_metadata`, but the community has adopted a particular structure:
   * - tags - An array of up to 5 strings. Although the blockchain will accept more than 5, the tags plugin only looks at the first five
   * - app - A user agent style application identifier. Typically app_name/version, e.g. beblurt/0.1
   * - format - The format of the body, e.g. markdown
   * @remarks In addition to the above keys, application developers are free to add any other keys they want to help manage the content they broadcast.
   * @remarks When a comment is first broadcast, the permlink must be unique for the author. Otherwise, it is interpreted as an update operation.
   *  Updating will either replace the entire body with the latest operation or patch the body if using {@link [diff-match-patch](https://github.com/google/diff-match-patch)}.
   */
  public async comment(data: CommentOperation[1], key: PrivateKey, options?: CommentOptionsOperation[1]): Promise<TransactionConfirmation> {
    const ops: Operation[] = []
    ops.push(['comment', data])
    if (options) {
      ops.push(['comment_options', options])
    }
    return this.sendOperations(ops, key)
  }

  /**
   * Comment options.
   * Authors of posts may not want all of the benefits that come from creating a post. This operation allows authors to update properties associated with their post.
   * Typically, these options will accompany a comment operation in the same transaction.
   * @param data The comment_options payload. See {@link CommentOptionsOperation}
   * @param key The Active or Owner private key of the account creator.
   */
  public async commentOptions(data: CommentOptionsOperation[1], key: PrivateKey): Promise<TransactionConfirmation> {
    const op: Operation = ['comment_options', data]
    return this.sendOperations([op], key)
  }

  /**
   * Create claimed account.
   * When used with `claim_account`, works identically to `account_create` See {@link accountCreate}.
   * @param data The create_claimed_account payload. See {@link CreateClaimedAccountOperation}
   * @param key The Active or Owner private key of the account creator.
   * @remarks Deactivated Since HF 0.2
   */
  public async createClaimedAccount(data: CreateClaimedAccountOperation[1], key: PrivateKey): Promise<TransactionConfirmation> {
    const op: Operation = ['create_claimed_account', data]
    return this.sendOperations([op], key)
  }

  /**
   * Broadcast custom JSON.
   * Serves the same purpose as custom but also supports required posting authorities. Unlike custom, this operation is designed to be human readable/developer friendly.
   * @param data The custom_json operation payload. See {@link CustomJsonOperation}
   * @param key The Posting, Active or Owner private key of the account.
   */
  public async customJson(data: CustomJsonOperation[1], key: PrivateKey): Promise<TransactionConfirmation> {
    const op: Operation = ['custom_json', data]
    return this.sendOperations([op], key)
  }

  /**
   * Delegate vesting shares from one account to the other. The vesting shares are still owned
   * by the original account, but content voting rights and bandwidth allocation are transferred
   * to the receiving account. This sets the delegation to `vesting_shares`, increasing it or
   * decreasing it as needed. (i.e. a delegation of 0 removes the delegation)
   *
   * When a delegation is removed the shares are placed in limbo for a week to prevent a satoshi
   * of VESTS from voting on the same content twice.
   *
   * @param options Delegation options. See {@link DelegateVestingSharesOperation}
   * @param key Private active key of the delegator.
   */
   public async delegateVestingShares(options: DelegateVestingSharesOperation[1], key: PrivateKey): Promise<TransactionConfirmation> {
    const op: Operation = ['delegate_vesting_shares', options]
    return this.sendOperations([op], key)
  }

  /**
   * Prepare transaction with operations for Sign and broadcast to the network.
   * @param operations List of operations to send.
   */
  public async prepareTransaction(operations: Operation[]): Promise<Transaction> {
    const props = await this.client.condenser.getDynamicGlobalProperties()

    const ref_block_num = props.head_block_number & 0xffff
    const ref_block_prefix = Buffer.from(props.head_block_id, 'hex').readUInt32LE(4)
    const expiration = new Date(new Date(props.time + 'Z').getTime() + this.expireTime).toISOString().slice(0, -5)
    const extensions: any[] = []

    const tx: Transaction = {
      expiration,
      extensions,
      operations,
      ref_block_num,
      ref_block_prefix
    }
    return tx
  }

  /**
   * Reblurt/Undo Reblurt a post
   * @param account The account submitting the custom_json operation.
   * @param author The author of the post.
   * @param permlink The permlink of the post.
   * @param undo if true Undo Reblurt else Reblurt
   * @param key The Posting, Active or Owner private key of the account.
   **/
  public async reblurt(account: string, author: string, permlink: string, undo= false, key: PrivateKey): Promise<TransactionConfirmation> {
    const r: { account: string; author: string; permlink: string; delete?: 'delete' } = { account, author, permlink }
    if (undo) { r.delete = 'delete' }
    const op: Operation = [
      'custom_json',
      { required_auths: [], required_posting_auths: [account], id: 'reblog', json: JSON.stringify(['reblog', r]) }]
    return this.sendOperations([op], key)
  }

  /** Broadcast a read notification to the network. */
  public async readNotification(account: string, date: string, key: PrivateKey): Promise<TransactionConfirmation> {
    const r: { date: string } = { date }
    const op: Operation = [
      'custom_json',
      { required_auths: [], required_posting_auths: [account], id: 'notify', json: JSON.stringify(['setLastRead', r]) }]
    return this.sendOperations([op], key)
  }

  /** Broadcast a signed transaction to the network. */
  public async send(transaction: SignedTransaction): Promise<TransactionConfirmation> {
    const trxId = cryptoUtils.generateTrxId(transaction)
    const result = await this.call('broadcast_transaction', [transaction])
    return Object.assign({ id: trxId }, result)
  }

  /**
   * Sign and broadcast transaction with operations to the network. Throws if the transaction expires.
   * @param operations List of operations to send.
   * @param key Private key(s) used to sign transaction.
   */
  // eslint-disable-next-line max-len
  public async sendOperations(operations: Operation[], key: PrivateKey | PrivateKey[]): Promise<TransactionConfirmation> {

    const tx = await this.prepareTransaction(operations)
    const result = await this.send(this.sign(tx, key))
    // assert(result.expired === false, 'transaction expired')

    return result
  }

  /** Sign a transaction with key(s) */
  public sign(transaction: Transaction, key: PrivateKey | PrivateKey[]): SignedTransaction {
    return cryptoUtils.signTransaction(transaction, key, this.client.chainId)
  }

  /**
   * Broadcast a transfer.
   * @param data The transfer operation payload.
   * @param key Private active key of sender.
   */
  public async transfer(data: TransferOperation[1], key: PrivateKey): Promise<TransactionConfirmation> {
    const op: Operation = ['transfer', data]
    return this.sendOperations([op], key)
  }

  /**
   * Broadcast a vote.
   * @param vote The vote to send.
   * @param key Private posting key of the voter.
   */
  public async vote(vote: VoteOperation[1], key: PrivateKey): Promise<TransactionConfirmation> {
    const op: Operation = ['vote', vote]
    return this.sendOperations([op], key)
  }

  // ____________________ NEXUS

  /**
   * Mute a post (Mods or higher). Can be a topic or a comment.
   * @param community The community account concerned.
   * @param authority The account submitting the custom_json operation.
   * @param account The author of the post.
   * @param permlink The permlink of the post.
   * @param notes short notes
   * @param key The Posting, Active or Owner private key of the account.
   */
  public async nexusMutePost(community: string, authority: string, account: string, permlink: string, notes: string, key: PrivateKey): Promise<TransactionConfirmation> {
    const op: Operation = [
      'custom_json', { required_auths: [], required_posting_auths: [authority], id: 'community', json: JSON.stringify(['mutePost', { community, account, permlink, notes }]) }]
    return this.sendOperations([op], key)
  }

  /**
   * Stickies a post to the top of the community homepage (Mods or higher). If multiple posts are stickied, the newest ones are shown first.
   * @param community The community account concerned.
   * @param authority The account submitting the custom_json operation.
   * @param account The author of the post.
   * @param permlink The permlink of the post.
   * @param key The Posting, Active or Owner private key of the account.
   */
   public async nexusPinPost(community: string, authority: string, account: string, permlink: string, key: PrivateKey): Promise<TransactionConfirmation> {
    const op: Operation = [
      'custom_json', { required_auths: [], required_posting_auths: [authority], id: 'community', json: JSON.stringify(['pinPost', { community, account, permlink }]) }]
    return this.sendOperations([op], key)
  }

  /**
   * Stickies a post to the top of the community homepage (Mods or higher). If multiple posts are stickied, the newest ones are shown first.
   * @param community The community account concerned.
   * @param authority The account submitting the custom_json operation.
   * @param account The account submitting the custom_json operation.
   * @param role The author of the post.
   * @param key The Posting, Active or Owner private key of the account.
   */
  public async nexusSetRole(community: string, authority: string, account: string, role: 'admin'|'mod'|'member'|'guest'|'muted', key: PrivateKey): Promise<TransactionConfirmation> {
    const op: Operation = [
      'custom_json', { required_auths: [], required_posting_auths: [authority], id: 'community', json: JSON.stringify(['setRole', { community, account, role }]) }]
    return this.sendOperations([op], key)
  }

  /**
   * Stickies a post to the top of the community homepage (Mods or higher). If multiple posts are stickied, the newest ones are shown first.
   * @param community The community account concerned.
   * @param authority The account submitting the custom_json operation.
   * @param account The account submitting the custom_json operation.
   * @param title title for the account
   * @param key The Posting, Active or Owner private key of the account.
   */
   public async nexusSetUserTitle(community: string, authority: string, account: string, title: string, key: PrivateKey): Promise<TransactionConfirmation> {
    const op: Operation = [
      'custom_json', { required_auths: [], required_posting_auths: [authority], id: 'community', json: JSON.stringify(['setUserTitle', { community, account, title }]) }]
    return this.sendOperations([op], key)
  }

  /**
   * Unmute a post (Mods or higher).
   * @param community The community account concerned.
   * @param authority The account submitting the custom_json operation.
   * @param account The author of the post.
   * @param permlink The permlink of the post.
   * @param notes short notes
   * @param key The Posting, Active or Owner private key of the account.
   */
  public async nexusUnmutePost(community: string, authority: string, account: string, permlink: string, notes: string, key: PrivateKey): Promise<TransactionConfirmation> {
    const op: Operation = [
      'custom_json', { required_auths: [], required_posting_auths: [authority], id: 'community', json: JSON.stringify(['unmutePost', { community, account, permlink, notes }]) }]
    return this.sendOperations([op], key)
  }

  /**
   * Removes a post to the top of the community homepage (Mods or higher).
   * @param community The community account concerned.
   * @param authority The account submitting the custom_json operation.
   * @param account The author of the post.
   * @param permlink The permlink of the post.
   * @param key The Posting, Active or Owner private key of the account.
   */
  public async nexusUnpinPost(community: string, authority: string, account: string, permlink: string, key: PrivateKey): Promise<TransactionConfirmation> {
    const op: Operation = [
      'custom_json', { required_auths: [], required_posting_auths: [authority], id: 'community', json: JSON.stringify(['unpinPost', { community, account, permlink }]) }]
    return this.sendOperations([op], key)
  }

  /**
   * Broadcast Nexus update properties (Admin Operations).
   * Update display settings of a Community.
   * @param data The custom_json operation payload. See {@link NexusUpdateProps}
   * @param authority The account submitting the custom_json operation.
   * @param key The Posting, Active or Owner private key of the account.
   */
  public async nexusUpdateProps(data: NexusUpdateProps, authority: string, key: PrivateKey): Promise<TransactionConfirmation> {
    const op: Operation = ['custom_json', { required_auths: [], required_posting_auths: [authority], id: 'community', json: JSON.stringify(['updateProps', data]) }]
    return this.sendOperations([op], key)
  }

  // ____________________ NEXUS Guest Operations

  /**
   * Used by guests to suggest a post for the review queue. It’s up to the community to define what constitutes flagging.
   * @param community The community account concerned.
   * @param authority The account submitting the custom_json operation.
   * @param author The author of the post.
   * @param permlink The permlink of the post.
   * @param notes short notes
   * @param key The Posting, Active or Owner private key of the account.
   */
  public async nexusFlagPost(community: string, authority: string, account: string, permlink: string, notes: string, key: PrivateKey): Promise<TransactionConfirmation> {
    const op: Operation = [
      'custom_json', { required_auths: [], required_posting_auths: [authority], id: 'community', json: JSON.stringify(['flagPost', { community, account, permlink, notes }]) }]
    return this.sendOperations([op], key)
  }

  /**
   * Un/subscribe to a community (Guest Operations)
   * @param community The community account concerned.
   * @param action Un/subscribe to a community.
   * @param account The account submitting the custom_json operation.
   * @param key The Posting, Active or Owner private key of the account.
   **/
  public async nexusSubscription(community: string, action: 'subscribe'|'unsubscribe', account: string, key: PrivateKey): Promise<TransactionConfirmation> {
    const op: Operation = ['custom_json', { required_auths: [], required_posting_auths: [account], id: 'community', json: JSON.stringify([action, { community }]) }]
    return this.sendOperations([op], key)
  }

}

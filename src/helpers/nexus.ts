/**
 * @file Blurt Nexus (A.k.a. bridge) helpers.
 * @author BeBlurt <https://beblurt.com/@beblurt>
 * @description adaptation from Johan Nordberg <code@johan-nordberg.com> Database API helpers
 * @license
 * Copyright (c) 2022 BeBlurt. All Rights Reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 *  1. Redistribution of source code must retain the above copyright notice, this
 *     list of conditions and the following disclaimer.
 *
 *  2. Redistribution in binary form must reproduce the above copyright notice,
 *     this list of conditions and the following disclaimer in the documentation
 *     and/or other materials provided with the distribution.
 *
 *  3. Neither the name of the copyright holder nor the names of its contributors
 *     may be used to endorse or promote products derived from this software without
 *     specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * You acknowledge that this software is not designed, licensed or intended for use
 * in the design, construction, operation or maintenance of any military facility.
 */

import {
  NexusAccountNotifications,
  NexusPost,
  NexusCommunity,
  NexusCommunityExtended,
  NexusCommunityContext,
  NexusProfile,
  NexusPayoutStats,
  NexusSortGetRankedPost
} from '../chain/nexus'

import { Client } from '../client'

export class Nexus {
  constructor(readonly client: Client) {}

  /** Convenience for calling `bridge`. */
  public call(method: string, params?: any[]|{ [key: string]: any }): Promise<any> {
    return this.client.call('bridge', method, params)
  }

  /**
   * Return array of account notification objects for the account passed.
   *
   * Returned values for type:
   * - new_community - a new community was created
   * - set_role - mod/admin adds a role to an account
   * - set_props - properties set for a community
   * - set_label - a title/badge/label has been set for an account
   * - mute_post - a post has been muted, with a reason
   * - unmute_post - a post has been unmuted, with a reason
   * - pin_post - a post has been pinned
   * - unpin_post - a post has been unpinned
   * - flag_post - a post has been flagged by a member, with a reason
   * - error - provides feedback to developers for ops that cannot be interpreted
   * - subscribe - an account has subscribed to a community
   * - reply - a post has been replied to
   * - reblog - a post has been reblogged/reblogged
   * - follow - an account has followed another account
   * - mention - author mentions an account
   * - vote - voter votes for an author
   *
   * The score value is based on the originating account’s rank.
   * @param account The account to fetch.
   * @param min_score Minimum score of notification, default: 25 [optional].
   * @param last_id Last notification ID, paging mechanism [optional].
   * @param limit Number of results, default: 100 [optional].
   */
  public accountNotifications(account: string, min_score = 25, last_id: number|null = null, limit = 100): Promise<NexusAccountNotifications[]> {
    return this.call('account_notifications', { account, min_score, last_id, limit })
  }

  /**
   * Return a list of posts related to a given account.
   * @param sort Values accepted:
   * - blog: top posts authored by given account (excluding posts to communities - unless explicitely reblogged) plus reblogs ranked by creation/reblog time.
   * - feed: top posts from blogs of accounts that given account is following ranked by creation/reblog time, not older than last month.
   * - posts: top posts authored by given account, newer first comments - replies authored by given account, newer first.
   * - comments:
   * - replies: replies to posts of given account, newer first.
   * - payout: all posts authored by given account that were not yet cashed out.
   * @param account The account to fetch.
   * @param start_author Name of author to start from, used for paging. Should be used in conjunction with `start_permlink`.
   * @param start_permlink Permalink of post to start from, used for paging. Should be used in conjunction with `start_author`.
   * @param limit Number of results, max 1000.
   * @param observer A valid account [optional].
   */
  public getAccountPosts(
    sort: 'blog'|'feed'|'posts'|'comments'|'replies'|'payout',
    account: string,
    start_author: string|null,
    start_permlink: string|null,
    limit: number,
    observer: string|null = null
  ): Promise<NexusPost[]> {
    return this.call('get_account_posts', { sort, account, start_author, start_permlink, limit, observer })
  }

  /**
   * Gets the full community object. Includes metadata, leadership team. If `observer` is provided, get subcription status, user title, user role.
   * @param name The community account to fetch.
   * @param observer A valid account [optional].
   */
   public getCommunity(name: string, observer: string|null = null): Promise<NexusCommunityExtended> {
    return this.call('get_community', { name, observer })
  }

  /**
   * For a community/account: returns role, title, subscribed state.
   * @param name The community account concerned.
   * @param account The account to fetch.
   */
  public getCommunityContext(name: string, account: string): Promise<NexusCommunityContext> {
    return this.call('get_community_context', { name, account })
  }

  /**
   * Gives a flattened discussion tree starting at given post. Modified `get_state` thread implementation.
   * Returns an Object whose key is "author/permlink".
   */
  public getDiscussion(author: string, permlink: string): Promise<{[key: string]: NexusPost}> {
    return this.call('get_discussion', { author, permlink })
  }

  /**
   * Get payout stats for building treemap.
   * @param limit Number of result, default: 250 [optional].
   */
  public getPayoutStats(limit = 250): Promise<NexusPayoutStats> {
    return this.call('get_payout_stats', { limit })
  }

  /**
   * Fetch a single post.
   */
  public getPost(author: string, permlink: string, observer: string|null = null): Promise<NexusPost> {
    return this.call('get_post', { author, permlink, observer })
  }

  /**
   * Returns a resume of a profile with metadata parsed.
   */
  public getProfile(account: string, observer: string|null = null): Promise<NexusProfile> {
    return this.call('get_profile', { account, observer })
  }

  /**
   * Query posts, sorted by given method.
   * @param sort Sorting of results, valid options are: 'trending', 'hot', 'created', 'promoted', 'payout', 'payout_comments', 'muted'.
   * @param start_author Name of author to start from, used for paging. Should be used in conjunction with `start_permlink` [optional].
   * @param start_permlink Permalink of post to start from, used for paging. Should be used in conjunction with `start_author` [optional].
   * @param limit Number of results, default: 20 [optional].
   * @param tag Specify a Tag [optional].
   * @param observer A valid account [optional].
   */
   public getRankedPosts(
    sort: NexusSortGetRankedPost,
    start_author: string|null = null,
    start_permlink: string|null = null,
    limit = 20,
    tag: string|null = null,
    observer: string|null = null
  ): Promise<NexusPost[]> {
    return this.call('get_ranked_posts', { sort, start_author, start_permlink, limit, tag, observer })
  }

  /**
   * Returns a lists of all communities `account` subscribes to, plus role and title in each.
   * The content of a community `account` subscribes to is [community name, community title, role_id, title].
   * @param account The account to fetch.
   */
   public listAllSubscriptions(account: string): Promise<[string, string, string, string|null][]> {
    return this.call('list_all_subscriptions', { account })
  }

  /**
   * Returns a list of communities.
   * @param last Name of community, paging mechanism [optional].
   * @param limit Number of listed communities, default: 100 [optional].
   * @param query Filters against title and about community fields [optional].
   * @param sort Sorting of results, default: rank [optional].
   * - rank: sort by community rank.
   * - new: sort by newest community.
   * - subs: sort by subscriptions.
   * @param observer A valid account [optional].
   */
  public listCommunities(last: string|null = null, limit = 100, query: string|null = null, sort: 'rank'|'new'|'subs' = 'rank', observer: string|null = null): Promise<NexusCommunity[]> {
    return this.call('list_communities', { last, limit, query, sort, observer })
  }

  /**
   * Returns a list of community account-roles (anyone with non-guest status).
   * The content of a community account-roles array is [community name, role_id, title].
   * @param community Community category name (account).
   * @param last Name of subscriber, paging mechanism [optional].
   * @param limit limit Number of listed communities, default: 100 [optional].
   */
   public listCommunityRoles(community: string, last: string|null = null, limit = 100): Promise<[string, string, string|null][]> {
    return this.call('list_community_roles', { community, last, limit })
  }

  /**
   * Returns a list communities by new subscriber count.
   * The content of a community array is [community name, community title].
   * @param limit Number of listed communities, default: 25 [optional].
   */
  public listPopComunities(limit = 25): Promise<[string, string][]> {
    return this.call('list_pop_communities', { limit })
  }

  /**
   * Returns a list of subscribers for a given community.
   * The content of a subscriber array is [account, role_id, title, created_at].
   * @param community Community category name (account).
   * @param last  Last account, paging mechanism [optional].
   * @param limit Number of results, default: 100 [optional].
   */
  public listSubscribers(community: string, last: string|null, limit = 100): Promise<[string, string, string|null, string][]> {
    return this.call('list_subscribers', { community, last, limit })
  }

  /**
   * Load notifications for a specific post.
   * @param author Name of author.
   * @param permlink Permlink of post.
   * @param min_score Minimum score of notification, default: 25 [optional].
   * @param last_id Last notification ID, paging mechanism [optional].
   * @param limit Number of results, default: 100 [optional].
   */
   public postNotifications(author: string, permlink: string, min_score = 25, last_id: number|null = null, limit = 100): Promise<NexusAccountNotifications[]> {
    return this.call('post_notifications', { author, permlink, min_score, last_id, limit })
  }

  /**
   * Load notifications for a specific post.
   * @param account The account to fetch.
   * @param min_score Minimum score of notification, default: 25 [optional].
   */
   public unreadNotifications(account: string, min_score = 25): Promise<{ lastread: string; unread: number }> {
    return this.call('unread_notifications', { account, min_score })
  }
}

/**
 * @file Blurt Condenser API helpers.
 * @author BeBlurt <https://beblurt.com/@beblurt>
 * @description adaptation from Johan Nordberg <code@johan-nordberg.com> Database API helpers.
 * @license
 * Copyright (c) 2022 BeBlurt. All Rights Reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 *  1. Redistribution of source code must retain the above copyright notice, this
 *     list of conditions and the following disclaimer.
 *
 *  2. Redistribution in binary form must reproduce the above copyright notice,
 *     this list of conditions and the following disclaimer in the documentation
 *     and/or other materials provided with the distribution.
 *
 *  3. Neither the name of the copyright holder nor the names of its contributors
 *     may be used to endorse or promote products derived from this software without
 *     specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * You acknowledge that this software is not designed, licensed or intended for use
 * in the design, construction, operation or maintenance of any military facility.
 */

import { ExtendedAccount } from '../chain/account'
import { BlockHeader, SignedBlock } from '../chain/block'
import { BlogEntry } from '../chain/blog'
import { Post, Discussion } from '../chain/comment'
import { DynamicGlobalProperties, ChainProperties, RpcNodeConfig, VestingDelegation, RewardFund } from '../chain/misc'
import { AppliedOperation } from '../chain/operation'
import { Proposal, ProposalVote } from '../chain/proposal'
import { SignedTransaction } from '../chain/transaction'
import { Witness } from '../chain/witness'

import { Client } from '../client'

/** Possible categories for `get_discussions_by_*`. */
export type DiscussionQueryCategory =
| 'active'
| 'blog'
| 'cashout'
| 'children'
| 'comments'
| 'feed'
| 'hot'
| 'promoted'
| 'trending'
| 'votes'
| 'created'

export interface DisqussionQuery {
/** Name of author or tag to fetch. */
tag?: string
/** Number of results, max 100. */
limit: number
filter_tags?: string[]
select_authors?: string[]
select_tags?: string[]
/** Number of bytes of post body to fetch, default 0 (all) */
truncate_body?: number
/**
 * Name of author to start from, used for paging.
 * Should be used in conjunction with `start_permlink`.
 */
start_author?: string
/**
 * Permalink of post to start from, used for paging.
 * Should be used in conjunction with `start_author`.
 */
start_permlink?: string
parent_author?: string
parent_permlink?: string
}

export interface FollowCount { account: string; follower_count: number; following_count: number }
export interface Followers { follower: string; following: string; what: ['blog'|'ignore'|null] }
export interface Following { follower: string; following: string; what: ['blog'|'ignore'|null] }

type PARAMS_ACCOUNT_HISTORY = [string, number, number]|[string, number, number, number|null, number|null]|[string, number, number, number|null]

export class CondenserAPI {
  constructor(readonly client: Client) {}

  /** Convenience for calling `condenser_api`. */
  public call(method: string, params?: any[]|{ [key: string]: any }): Promise<any> {
    return this.client.call('condenser_api', method, params)
  }

  /**
   * Returns one or more account history objects for account operations
   *
   * @param account The account to fetch
   * @param from The starting index
   * @param limit The maximum number of results to return
   * @param operations_bitmask Generated by utils.makeBitMaskFilter() - see example below (not yet usable)
   * @example
   * ```typescript
   * import { utils } from "@beblurt/dblurt"
   * const op = dblurt.utils.operationOrders
   * const operationsBitmask = dblurt.utils.makeBitMaskFilter([
   *   op.vote,
   *   op.comment,
   *   op.delete_comment,
   *   op.comment_options,
   *   op.claim_reward_balance,
   *   op.author_reward,
   *   op.curation_reward,
   *   op.comment_reward,
   *   op.producer_reward,
   * ])
   * const accountHistory = await client.condenser.getAccountHistory('beblurt', -1, 10, operationsBitmask)
   * ```
   */
   public getAccountHistory(account: string, from: number, limit: number, operation_bitmask?: (number|null)[]): Promise<[[number, AppliedOperation]]> {
    // const params = [account, from, limit]
    // Future usage when filter will be availabe
    let params: PARAMS_ACCOUNT_HISTORY = [account, from, limit]
    if (operation_bitmask && Array.isArray(operation_bitmask)) {
      if (operation_bitmask.length !== 2) {
        throw Error('operation_bitmask should be generated by the helper function')
      } else {
        params = operation_bitmask[1] ? [...params, operation_bitmask[0], operation_bitmask[1]] : [...params, operation_bitmask[0]]
      }
    }
    return this.call('get_account_history', params)
  }

  /** Return array of account info objects for the usernames passed. @param usernames The accounts to fetch. */
  public getAccounts(usernames: string[]): Promise<ExtendedAccount[]> {
    return this.call('get_accounts', [usernames])
  }

  /** Returns all votes for the given post. */
  public async getActiveVotes(author: string, permlink: string): Promise<Post['active_votes']> {
    return this.call('get_active_votes', [author, permlink])
  }

  /** Returns the list of active witnesses */
  public async getActiveWitnesses(): Promise<Witness[]> {
    return this.call('get_active_witnesses', [])
  }

  /** Return block *blockNum*. */
  public getBlock(blockNum: number): Promise<SignedBlock> {
    return this.call('get_block', [blockNum])
  }

  /** Return header for *blockNum*. */
  public getBlockHeader(blockNum: number): Promise<BlockHeader> {
    return this.call('get_block_header', [blockNum])
  }

  /** Returns a list of blog entries for an account. */
  public getBlogEntries(account: string, 	start_entry_id: number, limit: number): Promise<BlogEntry[]> {
    return this.call('get_blog_entries', [account, start_entry_id, limit])
  }

  /** Return median chain properties decided by witness. */
  public async getChainProperties(): Promise<ChainProperties> {
    return this.call('get_chain_properties')
  }

  /** Return server config. See:
   * https://gitlab.com/blurt/blurt/-/blob/master/libraries/protocol/include/blurt/protocol/config.hpp
   */
  public getConfig(): Promise<RpcNodeConfig> {
    return this.call('get_config', [])
  }

  /** Returns a content (post or comment). */
  public getContent(author: string, permlink: string): Promise<Post> {
    return this.call('get_content', [author, permlink])
  }

  /** Returns a list of replies. */
  public getContentReplies(author: string, permlink: string): Promise<Post[]> {
    return this.call('get_content_replies', [author, permlink])
  }

  /** Return Dynamic Global Properties. */
  public getDynamicGlobalProperties(): Promise<DynamicGlobalProperties> {
    return this.call('get_dynamic_global_properties')
  }

  /**
   * Return array of discussions (a.k.a. posts).
   * @param by The type of sorting for the discussions, valid options are:
   *           `active` `blog` `cashout` `children` `comments` `created`
   *           `feed` `hot` `promoted` `trending` `votes`. Note that
   *           for `blog` and `feed` the tag is set to a username.
   */
  public getDiscussions(by: DiscussionQueryCategory, query: DisqussionQuery): Promise<Discussion[]> {
    return this.call(`get_discussions_by_${by}`, [query])
  }

  /** return the count of followers/following for an account */
  public async getFollowCount(accounts: string[]): Promise<FollowCount> {
    return this.call('get_follow_count', accounts)
  }

  /** return the list of the followers of an account */
  public async getFollowers(account: string, start: string|null, type: 'blog'|'ignore'|null, limit: number): Promise<Followers[]> {
    return this.call('get_followers', [account, start, type, limit])
  }

  /** return the list of accounts that are following an account */
  public async getFollowing(account: string, start: string|null, type: 'blog'|'ignore'|null, limit: number): Promise<Following[]> {
    return this.call('get_following', [account, start, type, limit])
  }

  /** Returns the list of accounts that are reblogged a content (post). */
  public getRebloggedBy(author: string, permlink: string): Promise<string[]> {
    return this.call('get_reblogged_by', [author, permlink])
  }

  /** Return all applied operations in *blockNum*. */
  public getOperations(blockNum: number, onlyVirtual = false): Promise<AppliedOperation[]> {
    return this.call('get_ops_in_block', [blockNum, onlyVirtual])
  }

  /**
   * Returns an array of proposals filtered by the specified parameters.
   * @param start - Depends on order (see below)
   * - creator - creator of the proposal (account name string)
   * - start_date - start date of the proposal (date string)
   * - end_date - end date of the proposal (date string)
   * - total_votes - total votes of the proposal (int)
   * @param limit - The maximum number of proposals to return (max 1000).
   * @param order - can be one of:
   * - by_creator - order by proposal creator
   * - by_start_date - order by proposal start date
   * - by_end_date - order by proposal end date
   * - by_total_votes - order by proposal total votes
   * @param order_direction - The direction in which to order the results. Can be ascending or descending
   * @param status - The status of proposals to return.
   * @returns A Promise that resolves to an array of Proposal objects.
   */
  public getProposals(
    start: string[]|number[],
    limit: number,
    order: 'by_creator'|'by_start_date'|'by_end_date'|'by_total_votes',
    order_direction: 'ascending'|'descending',
    status: 'all'|'inactive'|'active'|'expired'|'votable'
  ): Promise<Proposal[]> {
    return this.call('list_proposals', [start, limit, order, order_direction, status])
  }

  /**
   * Returns all proposal votes, starting with the specified voter or proposal.id.
   * @param start - Depends on order (see below)
   * - voter - voter of the proposal (account name string)
   * - proposal.id - id the proposal (int)
   * @param limit - The maximum number of proposals to return (max 1000).
   * @param order - can be one of:
   * - by_voter_proposal - order by proposal voter
   * - by_proposal_voter - order by proposal.id
   * @param order_direction - The direction in which to order the results. Can be ascending or descending
   * @param status - The status of proposals to return.
   * @returns A Promise that resolves to an array of ProposalVote objects.
   */
  public getProposalVotes(
    start: string[]|number[],
    limit: number,
    order: 'by_voter_proposal'|'by_proposal_voter',
    order_direction: 'ascending'|'descending',
    status: 'all'|'inactive'|'active'|'expired'|'votable'
  ): Promise<ProposalVote[]> {
    return this.call('list_proposal_votes', [start, limit, order, order_direction, status])
  }

  /** Returns information about the current reward funds */
  public async getRewardFund(fund: 'post'): Promise<RewardFund> {
    return this.call('get_reward_fund', [fund])
  }

  /**
   * Return all of the state required for a particular url path.
   * @param path Path component of url conforming to condenser's scheme
   *             e.g. `@beblurt` or `trending/travel`
   */
  public async getState(path: string): Promise<any> {
    return this.call('get_state', [path])
  }

  /** Returns the details of a transaction based on a transaction id. */
  public async getTransaction(txId: string): Promise<SignedTransaction> {
    return this.call('get_transaction', [txId])
  }

  /** return rpc node version */
  public async getVersion(): Promise<object> {
    return this.call('get_version', [])
  }

  /**
   * Get list of delegations made by account.
   * @param account Account delegating
   * @param from Delegatee start offset, used for paging.
   * @param limit Number of results, max 1000.
   */
  public async getVestingDelegations(account: string, from = '', limit = 1000): Promise<VestingDelegation[]> {
    return this.call('get_vesting_delegations', [account, from, limit])
  }

  /** Returns the current witness schedule */
  public async getWitnesseSchedule(): Promise<Witness[]> {
    return this.call('get_witness_schedule', [])
  }

  /** Returns the current number of witnesses */
  public async getWitnessesCount(): Promise<Witness[]> {
    return this.call('get_witness_count', [])
  }

  /** Returns current witnesses by vote */
  public async getWitnessesByVote(account: string|null, limit: number): Promise<Witness[]> {
    return this.call('get_witnesses_by_vote', [account, limit])
  }

  /** Looks up accounts starting with name */
  public async lookupAccounts(account: string, limit: number): Promise<string[]> {
    return this.call('lookup_accounts', [account, limit])
  }

  /** Looks up accounts starting with name */
  public async lookupWitnessAccounts(account: string, limit: number): Promise<string[]> {
    return this.call('lookup_witness_accounts', [account, limit])
  }

  /** Verify signed transaction. */
  public async verifyAuthority(stx: SignedTransaction): Promise<boolean> {
    return this.call('verify_authority', [stx])
  }
}

/**
 * @file Blurt type definitions related to comments and posting.
 * @author BeBlurt <https://beblurt.com/@beblurt>
 * @description adaptation from Johan Nordberg <code@johan-nordberg.com> type definitions related to comments and posting.
 * @license
 * Copyright (c) 2017 Johan Nordberg. All Rights Reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 *  1. Redistribution of source code must retain the above copyright notice, this
 *     list of conditions and the following disclaimer.
 *
 *  2. Redistribution in binary form must reproduce the above copyright notice,
 *     this list of conditions and the following disclaimer in the documentation
 *     and/or other materials provided with the distribution.
 *
 *  3. Neither the name of the copyright holder nor the names of its contributors
 *     may be used to endorse or promote products derived from this software without
 *     specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * You acknowledge that this software is not designed, licensed or intended for use
 * in the design, construction, operation or maintenance of any military facility.
 */

import { Asset } from './asset'

// share_type => number in condenser

export interface Comment {
  id:                     number // comment_id_type
  author:                 string // account_name_type
  permlink:               string
  category:               string
  parent_author:          string // account_name_type
  parent_permlink:        string
  title:                  string
  body:                   string
  json_metadata:          string
  last_update:            string // time_point_sec
  created:                string // time_point_sec
  active:                 string // time_point_sec
  last_payout:            string // time_point_sec
  depth:                  number // uint8_t
  children:               number // uint32_t
  net_rshares:            number | string // share_type
  abs_rshares:            number | string // share_type
  vote_rshares:           number | string // share_type
  children_abs_rshares:   number | string // share_type
  cashout_time:           string // time_point_sec
  max_cashout_time:       string // time_point_sec
  total_vote_weight:      number // uint64_t
  reward_weight:          number // uint16_t
  total_payout_value:     Asset | string
  curator_payout_value:   Asset | string
  author_rewards:         number | string // share_type
  net_votes:              number // int32_t
  root_author:            string
  root_permlink:          string
  max_accepted_payout:    Asset | string
  percent_blurt:          number // uint16_t
  allow_replies:          boolean
  allow_votes:            boolean
  allow_curation_rewards: boolean
  beneficiaries:          BeneficiaryRoute[]
  active_votes:           []
}
export interface ActiveVote {
  voter:   string
  weight:  number
  rshares: number | string // share_type
  percent: number
  time:    string
}
export interface Post extends Omit<Comment, 'active_votes'> {
  url:                        string           // /category/@rootauthor/root_permlink#author/permlink
  root_title:                 string
  pending_payout_value:       Asset | string
  total_pending_payout_value: Asset | string
  active_votes: ActiveVote[]
  replies:                    string[]        // /author/slug mapping
  promoted:                   Asset | string
  body_length:                string          // Bignum
  reblogged_by:               any[]           // account_name_type[]
}

/**
 * Discussion a.k.a. Post.
 */
export interface Discussion {
  id:                     number
  author:                 string
  permlink:               string
  category:               string
  parent_author:          string|''
  parent_permlink:        string|''
  title:                  string|''
  body:                   string
  json_metadata:          string
  last_update:            string
  created:                string
  active:                 string
  last_payout:            string
  depth:                  number
  children:               number
  net_rshares:            number | string // share_type
  abs_rshares:            number | string // share_type
  vote_rshares:           number | string // share_type
  children_abs_rshares:   number | string // share_type
  cashout_time:           string
  max_cashout_time:       string
  total_vote_weight:      number
  reward_weight:          number
  total_payout_value:     Asset | string
  curator_payout_value:   Asset | string
  author_rewards:         number
  net_votes:              number
  root_author:            string
  root_permlink:          string
  max_accepted_payout:    string
  percent_blurt:          number
  allow_replies:          boolean
  allow_votes:            boolean
  allow_curation_rewards: boolean
  beneficiaries:          [[string, number]]
  url:                    string
  root_title:             string
  pending_payout_value:   Asset | string
  total_pending_payout_value: Asset | string
  active_votes: ActiveVote[]
  replies: []
  promoted: Asset | string
  body_length: 0
  reblogged_by: []
}

export interface BeneficiaryRoute {
  account: string // account_name_type
  weight:  number // uint16_t
}

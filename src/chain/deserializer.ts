/**
 * @file Blurt protocol deserialization.
 * @author Johan Nordberg <code@johan-nordberg.com>
 * @license
 * Copyright (c) 2017 Johan Nordberg. All Rights Reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 *  1. Redistribution of source code must retain the above copyright notice, this
 *     list of conditions and the following disclaimer.
 *
 *  2. Redistribution in binary form must reproduce the above copyright notice,
 *     this list of conditions and the following disclaimer in the documentation
 *     and/or other materials provided with the distribution.
 *
 *  3. Neither the name of the copyright holder nor the names of its contributors
 *     may be used to endorse or promote products derived from this software without
 *     specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * You acknowledge that this software is not designed, licensed or intended for use
 * in the design, construction, operation or maintenance of any military facility.
 */
import ByteBuffer from 'bytebuffer'
import { PublicKey } from '../crypto'

export type Deserializer = (buffer: ByteBuffer) => void

const PublicKeyDeserializer = (buf: ByteBuffer): { key: Buffer } => {
  const c = fixed_buf(buf, 33)
  return PublicKey.fromBuffer(c)
}

const UInt64Deserializer = (b: ByteBuffer): Long => b.readUint64()

const UInt32Deserializer = (b: ByteBuffer): number => b.readUint32()

const BinaryDeserializer = (b: ByteBuffer): Buffer => {
  const len = b.readVarint32()
  const b_copy = b.copy(b.offset, b.offset + len)
  b.skip(len)
  return Buffer.from(b_copy.toBinary(), 'binary')
}

const BufferDeserializer = (keyDeserializers: [string, Deserializer][]): any => (buf: ByteBuffer | Buffer): any => {
  const obj = {}
  for (const [key, deserializer] of keyDeserializers) {
      try {
          // Decodes a binary encoded string to a ByteBuffer.
          buf = ByteBuffer.fromBinary(buf.toString('binary'), ByteBuffer.LITTLE_ENDIAN)
          obj[key] = deserializer(buf)
      } catch (error) {
          error.message = `${key}: ${error.message}`
          throw error
      }
  }
  return obj
}

const fixed_buf = (b: ByteBuffer, len: number): Buffer => {
  if (!b) {
      throw Error('No buffer found on first parameter')
  } else {
      const b_copy = b.copy(b.offset, b.offset + len)
      b.skip(len)
      return Buffer.from(b_copy.toBinary(), 'binary')
  }
}

export const EncryptedMemoDeserializer = BufferDeserializer([
  ['from', PublicKeyDeserializer],
  ['to', PublicKeyDeserializer],
  ['nonce', UInt64Deserializer],
  ['check', UInt32Deserializer],
  ['encrypted', BinaryDeserializer]
])

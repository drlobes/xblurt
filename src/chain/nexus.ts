/**
 * @file Blurt type definitions related to Nexus.
 * @author BeBlurt <https://beblurt.com/@beblurt>
 * @description adaptation from Johan Nordberg <code@johan-nordberg.com> type definitions related to comments and posting.
 * @license
 * Copyright (c) 2017 Johan Nordberg. All Rights Reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 *  1. Redistribution of source code must retain the above copyright notice, this
 *     list of conditions and the following disclaimer.
 *
 *  2. Redistribution in binary form must reproduce the above copyright notice,
 *     this list of conditions and the following disclaimer in the documentation
 *     and/or other materials provided with the distribution.
 *
 *  3. Neither the name of the copyright holder nor the names of its contributors
 *     may be used to endorse or promote products derived from this software without
 *     specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * You acknowledge that this software is not designed, licensed or intended for use
 * in the design, construction, operation or maintenance of any military facility.
 */
import { Asset } from './asset'
import { BeneficiaryRoute } from './comment'

/** Community Identifier */
export const communityAccountRegexp = /^blurt-[1-3]\d{4,6}$/
export const isValidCommunityAccountRegexp = (communityAccount: string): boolean => {
  const re = new RegExp(communityAccountRegexp)
  return re.test(communityAccount)
}

export type NexusTypeNotification =
|'new_community'
|'set_role'
|'set_props'
|'set_label'
|'mute_post'
|'unmute_post'
|'pin_post'
|'unpin_post'
|'flag_post'
|'error'
|'subscribe'
|'reply'
|'reblog'
|'follow'
|'mention'
|'vote'
export interface NexusAccountNotifications { id: number; type: NexusTypeNotification; score: number; date: string; msg: string; url: string }

export interface NexusPost {
  post_id: number // post_id_type
  author:                 string // account_name_type
  permlink:               string
  category:               string
  title:                  string
  body:                   string
  json_metadata: {
    tags?:        string[]
    users?:       string[]
    image?:       string[]
    links?:       string[]
    app?:         string
    format?:      string
    description?: string
    community?:   string // liketu
    flow?: { // liketu
      tags: string[]
      pictures: {
        id:      number
        name:    string
        mime:    string
        url:     string
        tags:    string[]
        caption: string
      }[]
    }
    type?: string|'3speak/video'
    video: {
      info: {
        platform:      string|'3speak'
        title:         string
        author:        string
        permlink:      string
        duration:      number
        filesize:      number
        file:          string
        lang:          string // ISO 639-1 Code
        firstUpload:   boolean
        ipfs:          string|null
        ipfsThumbnail: string|null
        video_v2:      string|null
        sourceMap: {
            type:   string|'video'
            url:    string
            format: string|'m3u8'
        }[]
      }
      content: {
        description: string
        tags: string[]
      }
    }
  }
  created:                string // time_point_sec
  updated:                string // time_point_sec
  depth:                  number // uint8_t
  children:               number // uint32_t
  net_rshares:            number | string // share_type
  is_paidout:             boolean
  payout_at:              string // time_point_sec
  payout:                 number
  pending_payout_value:   Asset | string
  author_payout_value:    Asset | string
  curator_payout_value:   Asset | string
  promoted:               Asset | string
  replies:                []
  active_votes: {
    voter:   string
    rshares: number | string // share_type
  }[]
  stats: {
    hide:        boolean
    gray:        boolean
    total_votes: number
    flag_weight: number
    is_pinned?:  boolean
    is_muted?:   boolean
  }
  beneficiaries:          BeneficiaryRoute[]
  max_accepted_payout:    Asset | string
  percent_blurt:          number // uint16_t

  parent_author?:         string
  parent_permlink?:       string

  url:                    string           // /category/@rootauthor/root_permlink#author/permlink
  blacklists:             []
  reblogged_by?:          string[]
  community?:             string
  community_title?:       string
  author_role?:           string
  author_title?:          string
}

export interface NexusCommunityContext { role: string; subscribed: boolean; title: string }

export interface NexusProfile {
  id:         number
  name:       string
  created:    string
  active:     string
  post_count: number
  blacklists: []
  stats: {
      sp:        number // BLURT Power
      rank:      number
      following: number
      followers: number
  }
  metadata: {
      profile: {
          name:          string
          about:         string
          website:       string
          location:      string
          cover_image:   string
          profile_image: string
      }
  }
  context?: { // Returned if an observer is provided
      followed: boolean
  }
}

export interface NexusCommunity {
  id:          number
  name:        string
  title:       string
  about:       string
  lang:        string // ISO 639‑1 Code language
  type_id:     number
  is_nsfw:     boolean
  subscribers: number
  sum_pending: number
  num_pending: number
  num_authors: number
  created_at:  string
  avatar_url:  string
  cover_url?:  string
  context: {
      subscribed: boolean
      role:       string
      title:      string
  }
  admins: string[]
}
export interface NexusCommunityExtended extends Omit<NexusCommunity, 'admins'> {
  description: string
  flag_text:   string
  settings: {
    avatar_url:      string
    cover_url?:      string
    default_view:    'list'|'blog'|'grid'
    comment_display: 'votes'|'trending'|'age'|'forum'
  }
  team: [string, string, string|''][] // [account, role_id, title]
}

export interface NexusPayoutStats {
  items: [
    string,     // account (community or user)
    string,     // title
    number,     // payout
    number,     // number of posts
    number|null // authors ?
  ][]
  total: number
  blogs: number
}

/** Possible sort for `get_ranked_posts`. */
export type NexusSortGetRankedPost =
| 'trending'
| 'hot'
| 'created'
| 'promoted'
| 'payout'
| 'payout_comments'
| 'muted'

export interface NexusUpdateProps {
  community: string
  props: {
    /** 32 chars maximum */
    title: string
    /** 120 chars maximum */
    about: string
    /** ISO 639-1 (en, fr, de) */
    lang:  string
    /** true if this community is 18+. UI to automatically tag all posts/comments nsfw */
    is_nsfw: boolean
    /** a blob of markdown to describe purpose, enumerate rules, etc. (5000 chars) */
    description: string
    /** custom text for reporting content */
    flag_text: string
    settings: {
      /** same format as account avatars; usually rendered as a circle */
      avatar_url: string
      /** same format as account covers; used as header background image */
      cover_url: string
      default_view: 'list' | 'blog' | 'grid'
    }
  }
}

export interface NexusUpdatePropsOperation {
  0: 'updateProps'
  1: NexusUpdateProps
}

export interface NexusSetRoleOperation {
  0: 'setRole'
  1: {
    community: string
    account:   string
    role:      'muted'|'guest'|'member'|'mod'|'admin'|'owner'
  }
}

export interface NexusSetUserTitleOperation {
  0: 'setUserTitle'
  1: {
    community: string
    account:   string
    title:     string
  }
}

export interface NexusMutePostOperation {
  0: 'mutePost'
  1: {
    community: string
    account:   string
    permlink:  string
    notes:     string
  }
}

export interface NexusUnMutePostOperation {
  0: 'unmutePost'
  1: {
    community: string
    account:   string
    permlink:  string
    notes:     string
  }
}

export interface NexusPinPostOperation {
  0: 'pinPost'
  1: {
    community: string
    account:   string
    permlink:  string
  }
}

export interface NexusUnPinPostOperation {
  0: 'unpinPost'
  1: {
    community: string
    account:   string
    permlink:  string
  }
}

export interface NexusFlagPostOperation {
  0: 'flagPost'
  1: {
    community: string
    account:   string
    permlink:  string
    notes:     string
  }
}
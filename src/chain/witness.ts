/**
 * @file Blurt type definitions related to witness.
 * @author BeBlurt <https://beblurt.com/@beblurt>
 * @description adaptation from Johan Nordberg <code@johan-nordberg.com> type definitions related to comments and posting.
 * @license
 * Copyright (c) 2017 Johan Nordberg. All Rights Reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 *  1. Redistribution of source code must retain the above copyright notice, this
 *     list of conditions and the following disclaimer.
 *
 *  2. Redistribution in binary form must reproduce the above copyright notice,
 *     this list of conditions and the following disclaimer in the documentation
 *     and/or other materials provided with the distribution.
 *
 *  3. Neither the name of the copyright holder nor the names of its contributors
 *     may be used to endorse or promote products derived from this software without
 *     specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * You acknowledge that this software is not designed, licensed or intended for use
 * in the design, construction, operation or maintenance of any military facility.
 */

import { Asset } from './asset'

export interface Witness {
  _id:                      number
  owner:                    string
  created:                  string
  url:                      string|''
  votes:                    string
  virtual_last_update:      string
  virtual_position:         string
  virtual_scheduled_time:   string
  total_missed:             number
  last_aslot:               number
  last_confirmed_block_num: number
  signing_key:              string
  props: {
      account_creation_fee:   Asset | string
      maximum_block_size:     number
      account_subsidy_budget: number
      account_subsidy_decay:  number
      operation_flat_fee:     Asset | string
      bandwidth_kbytes_fee:   Asset | string
      proposal_fee:           Asset | string
  }
  running_version:          string
  hardfork_version_vote:    string
  hardfork_time_vote:       string
  available_witness_account_subsidies: number
}

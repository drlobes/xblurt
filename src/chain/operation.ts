/**
 * @file Blurt operation type definitions.
 * @author BeBlurt <https://beblurt.com/@beblurt>
 * @description adaptation of Johan Nordberg <code@johan-nordberg.com> Blurt operation type definitions.
 * @license
 * Copyright (c) 2017 Johan Nordberg. All Rights Reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 *  1. Redistribution of source code must retain the above copyright notice, this
 *     list of conditions and the following disclaimer.
 *
 *  2. Redistribution in binary form must reproduce the above copyright notice,
 *     this list of conditions and the following disclaimer in the documentation
 *     and/or other materials provided with the distribution.
 *
 *  3. Neither the name of the copyright holder nor the names of its contributors
 *     may be used to endorse or promote products derived from this software without
 *     specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * You acknowledge that this software is not designed, licensed or intended for use
 * in the design, construction, operation or maintenance of any military facility.
 */

import { PublicKey } from '../crypto'
import { AuthorityType } from './account'
import { Asset } from './asset'
import { BeneficiaryRoute } from './comment'
import { WitnessUpdateProperties, HexBuffer } from './misc'

/**
 * Operation name.
 * Ref: https://gitlab.com/blurt/blurt/-/blob/dev/libraries/protocol/include/blurt/protocol/operations.hpp
 */
export type OperationName =  // ordered by <id>
  | 'vote' // 0
  | 'comment' // 1

  | 'transfer' // 2
  | 'transfer_to_vesting' // 3
  | 'withdraw_vesting' // 4

  | 'account_create' // 5
  | 'account_update' // 6

  | 'witness_update' // 7
  | 'account_witness_vote' // 8
  | 'account_witness_proxy' // 9

  | 'custom' // 10

  | 'delete_comment' // 11
  | 'custom_json' // 12
  | 'comment_options' // 13
  | 'set_withdraw_vesting_route' // 14
  | 'claim_account' // 15
  | 'create_claimed_account' // 16
  | 'request_account_recovery' // 17
  | 'recover_account' // 18
  | 'change_recovery_account' // 19
  | 'escrow_transfer' // 20
  | 'escrow_dispute' // 21
  | 'escrow_release' // 22
  | 'escrow_approve' // 23
  | 'transfer_to_savings' // 24
  | 'transfer_from_savings' // 25
  | 'cancel_transfer_from_savings' // 26
  | 'custom_binary' // 27
  | 'decline_voting_rights' // 28
  | 'reset_account' // 29
  | 'set_reset_account' // 30
  | 'claim_reward_balance' // 31
  | 'delegate_vesting_shares' // 32
  | 'witness_set_properties' // 33
  | 'create_proposal' // 34
  | 'update_proposal_votes' // 35
  | 'remove_proposal' // 36

export const OperationNameRegexp = new RegExp(
  '^vote$'
  + '|^comment$'
  + '|^transfer$'
  + '|^transfer_to_vesting$'
  + '|^withdraw_vesting$'
  + '|^account_create$'
  + '|^account_update$'
  + '|^witness_update$'
  + '|^account_witness_vote$'
  + '|^account_witness_proxy$'
  + '|^custom$'
  + '|^delete_comment$'
  + '|^custom_json$'
  + '|^comment_options$'
  + '|^set_withdraw_vesting_route$'
  + '|^claim_account$'
  + '|^create_claimed_account$'
  + '|^request_account_recovery$'
  + '|^recover_account$'
  + '|^change_recovery_account$'
  + '|^escrow_transfer$'
  + '|^escrow_dispute$'
  + '|^escrow_release$'
  + '|^escrow_approve$'
  + '|^transfer_to_savings$'
  + '|^transfer_from_savings$'
  + '|^cancel_transfer_from_savings$'
  + '|^custom_binary$'
  + '|^decline_voting_rights$'
  + '|^reset_account$'
  + '|^set_reset_account$'
  + '|^claim_reward_balance$'
  + '|^delegate_vesting_shares$'
  + '|^witness_set_properties$'
  + '|^create_proposal$'
  + '|^update_proposal_votes$'
  + '|^remove_proposal$'
)

/**
 * Virtual operation name.
 */
 export type VirtualOperationName =  // ordered by <id>
  | 'author_reward' // 37
  | 'curation_reward' // 38
  | 'comment_reward' // 39
  | 'fill_vesting_withdraw' // 40
  | 'shutdown_witness' // 41
  | 'fill_transfer_from_savings' // 42
  | 'hardfork' // 43
  | 'comment_payout_update' // 44
  | 'return_vesting_delegation' // 45
  | 'comment_benefactor_reward' // 46
  | 'producer_reward' // 47
  | 'clear_null_account_balance' // 48
  | 'proposal_pay' // 49
  | 'sps_fund' // 50
  | 'fee_pay' // 51

export const VirtualOperationNameRegexp = new RegExp(
  '^author_reward$'
  + '|^curation_reward$'
  + '|^comment_reward$'
  + '|^fill_vesting_withdraw$'
  + '|^shutdown_witness$'
  + '|^fill_transfer_from_savings$'
  + '|^hardfork$'
  + '|^comment_payout_update$'
  + '|^return_vesting_delegation$'
  + '|^comment_benefactor_reward$'
  + '|^producer_reward$'
  + '|^clear_null_account_balance$'
  + '|^proposal_pay$'
  + '|^sps_fund$'
  + '|^fee_pay$'
)

/**  Generic operation. */
export interface Operation {
  0: OperationName | VirtualOperationName
  1: { [key: string]: any }
}

export interface AppliedOperation {
  trx_id: string
  block: number
  trx_in_block: number
  op_in_trx: number
  virtual_op: number
  timestamp: string
  op: Operation
}

/** Account Create. */
export interface AccountCreateOperation extends Operation {
  0: 'account_create'
  1: {
      fee: string | Asset
      creator: string // account_name_type
      new_account_name: string // account_name_type
      owner: AuthorityType
      active: AuthorityType
      posting: AuthorityType
      memo_key: string | PublicKey // public_key_type
      json_metadata: string
  }
}

/** Account Update. */
export interface AccountUpdateOperation extends Operation {
  0: 'account_update'
  1: {
    account: string
    owner?: AuthorityType
    active?: AuthorityType
    posting?: AuthorityType
    memo_key?: string | PublicKey
    json_metadata: string | ''
    posting_json_metadata: string | ''
    extensions: any[]
  }
}

export interface AccountWitnessProxyOperation extends Operation {
  0: 'account_witness_proxy'
  1: {
    account: string // account_name_type
    proxy: string // account_name_type
  }
}

/** Vote/Unvote for a Witness. */
export interface AccountWitnessVoteOperation extends Operation {
  0: 'account_witness_vote'
  1: {
    account: string
    witness: string
    approve: boolean
  }
}

/** Author receive reward for his content. */
export interface AuthorRewardOperation extends Operation {
  0: 'author_reward'
  1: {
    author:          string // account_name_type
    permlink:        string
    blurt_payout:    string | Asset
    vesting_payout:  string | Asset
  }
}

export interface CancelTransferFromSavingsOperation extends Operation {
  0: 'cancel_transfer_from_savings'
  1: {
    from: string // account_name_type
    request_id: number // uint32_t
  }
}

export interface ChangeRecoveryAccountOperation extends Operation {
  0: 'change_recovery_account'
  1: {
    /** The account that would be recovered in case of compromise. */
    account_to_recover: string // account_name_type
    /** The account that creates the recover request. */
    new_recovery_account: string // account_name_type
    /** Extensions. Not currently used. */
    extensions: any[] // extensions_type
  }
}

export interface ClaimAccountOperation extends Operation {
  0: 'claim_account'
  1: {
    creator: string // account_name_type
    fee: string | Asset
    /** Extensions. Not currently used. */
    extensions: any[] // extensions_type
  }
}

export interface ClaimRewardBalanceOperation extends Operation {
  0: 'claim_reward_balance'
  1: {
    account: string // account_name_type
    reward_blurt: string | Asset
    reward_vests: string | Asset
  }
}

export interface CommentBenefactorRewardOperation extends Operation {
  0: 'comment_benefactor_reward'
  1: {
    benefactor:     string // account_name_type
    author:         string // account_name_type
    permlink:       string
    blurt_payout:   string | Asset
    vesting_payout: string | Asset
  }
}

export interface CommentOperation extends Operation {
  0: 'comment'
  1: {
    /** the author that comment is being submitted to, when posting a new blog this is an empty string. */
    parent_author: string // account_name_type
    /** specific post that comment is being submitted to, when posting a new blog this become the category of the post (tags[0]). */
    parent_permlink: string
    /** author of the post/comment being submitted (account name). */
    author: string // account_name_type
    /** unique string identifier for the post, linked to the author of the post. */
    permlink: string
    /** human readable title of the post being submitted, this is often blank when commenting. */
    title: string
    /** body of the post/comment being submitted, or diff-match-patch when updating. */
    body: string
    /** JSON object string. */
    json_metadata: string
  }
}

export interface CommentOptionsOperation extends Operation {
  0: 'comment_options'
  1: {
    /** author of the post/comment being submitted (account name). */
    author: string // account_name_type
    /** human readable title of the post being submitted, this is often blank when commenting. */
    permlink: string
    /** the maximum payout this post will receive. asset( 1000000000, BLURT_SYMBOL ) */
    max_accepted_payout: Asset | string
    /** The percent of BLURT to key, unkept amounts will be received as BLURT Power. */
    // percent_blurt: number
    /** Whether to allow post to receive votes. */
    allow_votes: boolean
    /** Whether to allow post to recieve curation rewards. If false rewards return to reward fund. */
    allow_curation_rewards: boolean
    extensions: [
      number,
      /** Must be specified in sorted order (account ascending; no duplicates) */
      { beneficiaries: BeneficiaryRoute[] } | { percent_blurt: number }][] // flat_set< comment_options_extension >
  }
}

export interface CreateClaimedAccountOperation extends Operation {
  0: 'create_claimed_account'
  1: {
    creator: string // account_name_type
    new_account_name: string // account_name_type
    owner: AuthorityType
    active: AuthorityType
    posting: AuthorityType
    memo_key: string | PublicKey // public_key_type
    json_metadata: string
    /** Extensions. Not currently used. */
    extensions: any[] // extensions_type
  }
}

export interface CreateProposalOperation extends Operation {
  0: 'create_proposal'
  1: {
    creator: string
    receiver: string
    start_date: string // time_point_sec
    end_date: string // time_point_sec
    daily_pay: Asset | string
    subject: string
    permlink: string
    /** Extensions. Not currently used. */
    extensions: any[] // extensions_type
  }
}

/** Account receive curation reward for his upvote. */
export interface CurationRewardOperation extends Operation {
  0: 'curation_reward'
  1: {
    curator:          string // account_name_type
    reward:           string | Asset // VESTS
    comment_author:   string // account_name_type
    comment_permlink: string
  }
}

/**
 * @brief provides a generic way to add higher level protocols on top of witness consensus
 * @ingroup operations
 *
 * There is no validation for this operation other than that required auths are valid
 */
export interface CustomOperation extends Operation {
  0: 'custom'
  1: {
    required_auths: string[]
    id: number // uint16
    data: Buffer | HexBuffer | number[]
  }
}

export interface CustomJsonOperation extends Operation {
  0: 'custom_json'
  1: {
      required_auths: string[] // flat_set< account_name_type >
      required_posting_auths: string[] // flat_set< account_name_type >
      /** ID string, must be less than 32 characters long. */
      id: string
      /** JSON encoded string, must be valid JSON. */
      json: string
  }
}

export interface DeclineVotingRightsOperation extends Operation {
  0: 'decline_voting_rights'
  1: {
    account: string // account_name_type
    decline: boolean
  }
}

export interface DelegateVestingSharesOperation extends Operation {
  0: 'delegate_vesting_shares'
  1: {
    /** The account delegating vesting shares. */
    delegator: string // account_name_type
    /** The account receiving vesting shares. */
    delegatee: string // account_name_type
    /** The amount of vesting shares delegated. */
    vesting_shares: string | Asset
  }
}

export interface DeleteCommentOperation extends Operation {
  0: 'delete_comment'
  1: {
    author: string // account_name_type
    permlink: string
  }
}

/**
 * The agent and to accounts must approve an escrow transaction for it to be valid on
 * the blockchain. Once a part approves the escrow, the cannot revoke their approval.
 * Subsequent escrow approve operations, regardless of the approval, will be rejected.
 */
 export interface EscrowApproveOperation extends Operation {
  0: 'escrow_approve'
  1: {
    from: string // account_name_type
    to: string // account_name_type
    agent: string // account_name_type
    /** Either to or agent. */
    who: string // account_name_type
    escrow_id: number // uint32_t
    approve: boolean
  }
}

/**
 * If either the sender or receiver of an escrow payment has an issue, they can
 * raise it for dispute. Once a payment is in dispute, the agent has authority over
 * who gets what.
 */
 export interface EscrowDisputeOperation extends Operation {
  0: 'escrow_dispute'
  1: {
    from: string // account_name_type
    to: string // account_name_type
    agent: string // account_name_type
    who: string // account_name_type
    escrow_id: number // uint32_t
  }
}

/**
 * This operation can be used by anyone associated with the escrow transfer to
 * release funds if they have permission.
 *
 * The permission scheme is as follows:
 * If there is no dispute and escrow has not expired, either party can release funds to the other.
 * If escrow expires and there is no dispute, either party can release funds to either party.
 * If there is a dispute regardless of expiration, the agent can release funds to either party
 *    following whichever agreement was in place between the parties.
 */
 export interface EscrowReleaseOperation extends Operation {
  0: 'escrow_release'
  1: {
    from: string // account_name_type
    /** The original 'to'. */
    to: string // account_name_type
    agent: string // account_name_type
    /** The account that is attempting to release the funds, determines valid 'receiver'. */
    who: string // account_name_type
    /** The account that should receive funds (might be from, might be to). */
    receiver: string // account_name_type
    escrow_id: number // uint32_t
    /** The amount of blurt to release. */
    blurt_amount: Asset | string
  }
}

/**
 * The purpose of this operation is to enable someone to send money contingently to
 * another individual. The funds leave the *from* account and go into a temporary balance
 * where they are held until *from* releases it to *to* or *to* refunds it to *from*.
 *
 * In the event of a dispute the *agent* can divide the funds between the to/from account.
 * Disputes can be raised any time before or on the dispute deadline time, after the escrow
 * has been approved by all parties.
 *
 * This operation only creates a proposed escrow transfer. Both the *agent* and *to* must
 * agree to the terms of the arrangement by approving the escrow.
 *
 * The escrow agent is paid the fee on approval of all parties. It is up to the escrow agent
 * to determine the fee.
 *
 * Escrow transactions are uniquely identified by 'from' and 'escrow_id', the 'escrow_id' is defined
 * by the sender.
 */
 export interface EscrowTransferOperation extends Operation {
  0: 'escrow_transfer'
  1: {
    from: string // account_name_type
    to: string // account_name_type
    agent: string // account_name_type
    escrow_id: number // uint32_t
    blurt_amount: Asset | string
    fee: Asset | string
    ratification_deadline: string // time_point_sec
    escrow_expiration: string // time_point_sec
    json_meta: string
  }
}

export interface FillTransferFromSavingOperation extends Operation {
  0: 'fill_transfer_from_savings'
  1: {
    from:       string // account_name_type
    to:         string // account_name_type
    amount:     Asset | string // VESTS
    request_id: number
    memo:       string
  }
}

export interface FillVestingWithdrawOperation extends Operation {
  0: 'fill_vesting_withdraw'
  1: {
    from_account: string // account_name_type
    to_account:   string // account_name_type
    withdrawn: Asset | string // VESTS
    deposited: Asset | string // BLURT
  }
}

export interface HardforkOperation extends Operation {
  0: 'hardfork'
  1: {
    hardfork_id: number
  }
}

export interface ProducerRewardOperation extends Operation {
  0: 'producer_reward'
  1: {
    /** The witness account. */
    producer: string // account_name_type
    /** Reward in VESTS. */
    vesting_shares: Asset | string
  }
}

/** Payment of a proposal */
export interface ProposalPayOperation extends Operation {
  0: 'proposal_pay'
  1: {
    receiver:  string // account_name_type
    payment:   Asset | string // BLURT
    trx_id:    string
    op_in_trx: number
  }
}

export interface RecoverAccountOperation extends Operation {
  0: 'recover_account'
  1: {
    /** The account to be recovered. */
    account_to_recover: string // account_name_type
    /** The new owner authority as specified in the request account recovery operation. */
    new_owner_authority: AuthorityType
    /**
     * A previous owner authority that the account holder will use to prove
     * past ownership of the account to be recovered.
     */
    recent_owner_authority: AuthorityType
    /** Extensions. Not currently used. */
    extensions: any[] // extensions_type
  }
}

export interface RemoveProposalOperation extends Operation {
  0: 'remove_proposal'
  1: {
    proposal_owner: string
    /** IDs of proposals to be removed. Nonexisting IDs are ignored. */
    proposal_ids: number[] // flat_set_ex<int64_t>
    extensions: any[]
  }
}

/**
 * All account recovery requests come from a listed recovery account. This
 * is secure based on the assumption that only a trusted account should be
 * a recovery account. It is the responsibility of the recovery account to
 * verify the identity of the account holder of the account to recover by
 * whichever means they have agreed upon. The blockchain assumes identity
 * has been verified when this operation is broadcast.
 *
 * This operation creates an account recovery request which the account to
 * recover has 24 hours to respond to before the request expires and is
 * invalidated.
 *
 * There can only be one active recovery request per account at any one time.
 * Pushing this operation for an account to recover when it already has
 * an active request will either update the request to a new new owner authority
 * and extend the request expiration to 24 hours from the current head block
 * time or it will delete the request. To cancel a request, simply set the
 * weight threshold of the new owner authority to 0, making it an open authority.
 *
 * Additionally, the new owner authority must be satisfiable. In other words,
 * the sum of the key weights must be greater than or equal to the weight
 * threshold.
 *
 * This operation only needs to be signed by the the recovery account.
 * The account to recover confirms its identity to the blockchain in
 * the recover account operation.
 */
 export interface RequestAccountRecoveryOperation extends Operation {
  0: 'request_account_recovery'
  1: {
    /** The recovery account is listed as the recovery account on the account to recover. */
    recovery_account: string // account_name_type
    /** The account to recover. This is likely due to a compromised owner authority. */
    account_to_recover: string // account_name_type
    /**
     * The new owner authority the account to recover wishes to have. This is secret
     * known by the account to recover and will be confirmed in a recover_account_operation.
     */
    new_owner_authority: AuthorityType
    /** Extensions. Not currently used. */
    extensions: any[] // extensions_type
  }
}

/**
 * This operation allows recovery_account to change account_to_reset's owner authority to
 * new_owner_authority after 60 days of inactivity.
 */
 export interface ResetAccountOperation extends Operation {
  0: 'reset_account'
  1: {
    reset_account: string // account_name_type
    account_to_reset: string // account_name_type
    new_owner_authority: AuthorityType
  }
}

export interface ReturnVestingDelegationOperation extends Operation {
  0: 'return_vesting_delegation'
  1: {
    account:        string // account_name_type
    vesting_shares: Asset | string
  }
}

/**
 * This operation allows 'account' owner to control which account has the power
 * to execute the 'reset_account_operation' after 60 days.
 */
 export interface SetResetAccountOperation extends Operation {
  0: 'set_reset_account'
  1: {
    account: string // account_name_type
    current_reset_account: string // account_name_type
    reset_account: string // account_name_type
  }
}

/**
 * Allows an account to setup a vesting withdraw but with the additional
 * request for the funds to be transferred directly to another account's
 * balance rather than the withdrawing account. In addition, those funds
 * can be immediately vested again, circumventing the conversion from
 * vests to blurt and back, guaranteeing they maintain their value.
 */
 export interface SetWithdrawVestingRouteOperation extends Operation {
  0: 'set_withdraw_vesting_route'
  1: {
    from_account: string // account_name_type
    to_account: string // account_name_type
    percent: number // uint16_t (100% = 100_PERCENT = 10000)
    auto_vest: boolean
  }
}

export interface SpsFundOperation extends Operation {
  0: 'sps_fund'
  1: {
    additional_funds: Asset | string // BLURT
  }
}

/** Transfers asset from one account to another. */
 export interface TransferOperation extends Operation {
  0: 'transfer'
  1: {
    from:   string
    to:     string
    amount: string | Asset
    memo:   string
  }
}

export interface TransferFromSavingsOperation extends Operation {
  0: 'transfer_from_savings'
  1: {
    from: string // account_name_type
    request_id: number // uint32_t
    to: string // account_name_type
    amount: string | Asset
    memo: string
  }
}

export interface TransferToSavingsOperation extends Operation {
  0: 'transfer_to_savings'
  1: {
    from: string // account_name_type
    to: string // account_name_type
    amount: string | Asset
    memo: string
  }
}


/**
 *  This operation converts liquid token (BLURT or liquid SMT) into VFS (Vesting Fund Shares,
 *  VESTS or vesting SMT) at the current exchange rate. With this operation it is possible to
 *  give another account vesting shares so that faucets can pre-fund new accounts with vesting shares.
 */
export interface TransferToVestingOperation extends Operation {
  0: 'transfer_to_vesting'
  1: {
    from: string // account_name_type
    to: string // account_name_type
    /** must be BLURT or liquid variant of SMT */
    amount: string | Asset
  }
}

export interface UpdateProposalVotesOperation extends Operation {
  0: 'update_proposal_votes'
  1: {
    /** IDs of proposals to vote for/against. Nonexisting IDs are ignored. */
    voter: string
    proposal_ids: number[] // flat_set_ex<int64_t>
    approve: boolean
    extensions: any[]
  }
}

/** Vote on a comment. */
export interface VoteOperation extends Operation {
  0: 'vote'
  1: {
    voter: string
    author: string
    permlink: string
    /**
     * Voting weight, 100% = 10000 (100_PERCENT).
     */
    weight: number
  }
}

export interface WithdrawVestingOperation extends Operation {
  0: 'withdraw_vesting'
  1: {
    account: string // account_name_type
    /** Amount to power down, must be VESTS. */
    vesting_shares: string | Asset
  }
}

export interface WitnessSetPropertiesOperation extends Operation {
  0: 'witness_set_properties'
  1: {
    owner: string
    props: [string, Buffer][]
    extensions: any[]
  }
}

/**
 * Users who wish to become a witness must pay a fee acceptable to
 * the current witnesses to apply for the position and allow voting
 * to begin.
 *
 * If the owner isn't a witness they will become a witness.  Witnesses
 * are charged a fee equal to 1 weeks worth of witness pay which in
 * turn is derived from the current share supply.  The fee is
 * only applied if the owner is not already a witness.
 *
 * If the block_signing_key is null then the witness is removed from
 * contention.  The network will pick the top 21 witnesses for
 * producing blocks.
 */
 export interface WitnessUpdateOperation extends Operation {
  0: 'witness_update'
  1: {
    owner: string // account_name_type
    /** URL for witness, usually a link to a post in the witness-category tag. */
    url: string
    block_signing_key: string | PublicKey | null // public_key_type
    props: WitnessUpdateProperties
    /** The fee paid to register a new witness, should be 10x current block production pay. */
    fee: string | Asset
  }
}

/**
 * @file Blurt RPC client implementation.
 * @author BeBlurt <https://beblurt.com/@beblurt>
 * @description adaptation from Johan Nordberg <code@johan-nordberg.com> RPC client implementation.
 * @license
 * Copyright (c) 2017 Johan Nordberg. All Rights Reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 *  1. Redistribution of source code must retain the above copyright notice, this
 *     list of conditions and the following disclaimer.
 *
 *  2. Redistribution in binary form must reproduce the above copyright notice,
 *     this list of conditions and the following disclaimer in the documentation
 *     and/or other materials provided with the distribution.
 *
 *  3. Neither the name of the copyright holder nor the names of its contributors
 *     may be used to endorse or promote products derived from this software without
 *     specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * You acknowledge that this software is not designed, licensed or intended for use
 * in the design, construction, operation or maintenance of any military facility.
 */
import * as assert from 'assert'

import { VError } from 'verror'
import packageVersion from './version'

import { AccountHistoryAPI } from './helpers/account_history' 
import { Blockchain } from './helpers/blockchain'
import { BroadcastAPI } from './helpers/broadcast'
import { CondenserAPI } from './index-browser'
import { DatabaseAPI } from './helpers/database'
import { Nexus } from './helpers/nexus'
import { Tools } from './helpers/tools'

import { copy, retryingFetch } from './utils'


/** Library version. */
export const VERSION = packageVersion

/** Main Blurt network chain id */
export const DEFAULT_CHAIN_ID = Buffer.from(
  'cd8d90f29ae273abec3eaa7731e25934c63eb654d55080caff2ebb7f5df6381f',
  'hex'
)

/** Main Blurt network address prefix */
export const DEFAULT_ADDRESS_PREFIX = 'BLT'

interface RPCRequest {
  id: number | string // Request sequence number.
  method: 'call' | 'notice' | 'callback' // RPC method.
  jsonrpc: '2.0'
  params: any[] // Array of parameters to pass to the method.
}

interface RPCCall extends RPCRequest {
  method: 'call' | any
  params: [number | string, string, any[]]
}

interface RPCError {
  code: number
  message: string
  data?: any
}

interface RPCResponse {
  id: number
  error?: RPCError
  result?: any
}

/**
 * RPC Client options
 * ------------------
 */
export interface ClientOptions {
  /** Blurt chain id. Defaults `cd8d90f29ae273abec3eaa7731e25934c63eb654d55080caff2ebb7f5df6381f` */
  chainId?: string
  /** Blurt address prefix. Defaults to main network: `BLT`*/
  addressPrefix?: string
  /** how long to wait in milliseconds before giving up on a rpc call. Defaults to 60 * 1000 ms. */
  timeout?: number
  /**
   * Specifies the amount of times the urls (RPC nodes) should be
   * iterated and retried in case of timeout errors.
   * (important) Requires url parameter to be an array (string[])!
   * Can be set to 0 to iterate and retry forever. Defaults to 3 rounds.
   */
  failoverThreshold?: number
  /** Whether a console.log should be made when RPC failed over to another one */
  consoleOnFailover?: boolean
  /** Retry backoff function, returns milliseconds. Default = {@link defaultBackoff}. */
  backoff?: (tries: number) => number
  /** Node.js http(s) agent, use if you want http keep-alive. @see https://nodejs.org/api/http.html#http_new_agent_options. */
  agent?: any
}

/** Default backoff function. ```min(tries*10^2, 10 seconds)``` */
const defaultBackoff = (tries: number): number => Math.min(Math.pow(tries * 10, 2), 10 * 1000)

/**
 * RPC Client
 * ----------
 * Can be used in both node.js and the browser. Also see {@link ClientOptions}.
 */
export class Client {
  /** Client options, *read-only*. */
  public readonly options: ClientOptions
  /** Address to Blurt RPC server. String or String[] *read-only* */
  public address: string | string[]

  /** Account History API helper */
  public readonly accountHistory: AccountHistoryAPI
  /** Blockchain helper */
  public readonly blockchain: Blockchain
  /** Broadcast API helper */
  public readonly broadcast: BroadcastAPI
  /** Condenser API helper */
  public readonly condenser: CondenserAPI
  /** Database API helper. */
  public readonly database: DatabaseAPI
  /** Nexus (A.k.a. Bridge) helper */
  public readonly nexus: Nexus
  /** Tools helper */
  public readonly tools: Tools

  /** Chain ID for current network. */
  public readonly chainId: Buffer
  /** Address prefix for current network. */
  public readonly addressPrefix: string

  public currentAddress: string

  private timeout: number
  private backoff: typeof defaultBackoff

  private failoverThreshold: number

  private consoleOnFailover: boolean

  /**
   * @param address The address to the Blurt RPC server,
   * e.g. `https://rpc.blurt.world`. or [`https://rpc.blurt.world`, `https://another.api.com`]
   * @param options Client options.
   */
  constructor(address: string | string[], options: ClientOptions = {}) {

    this.currentAddress = Array.isArray(address) ? address[0] ? address[0] : 'https://rpc.blurt.world' : address
    this.address = address
    this.options = options

    this.chainId = options.chainId ? Buffer.from(options.chainId, 'hex') : DEFAULT_CHAIN_ID
    assert.strictEqual(this.chainId.length, 32, 'invalid chain id')
    this.addressPrefix = options.addressPrefix || DEFAULT_ADDRESS_PREFIX

    this.timeout = options.timeout || 60 * 1000
    this.backoff = options.backoff || defaultBackoff
    this.failoverThreshold = options.failoverThreshold || 3
    this.consoleOnFailover = options.consoleOnFailover || false

    this.accountHistory = new AccountHistoryAPI(this)
    this.blockchain = new Blockchain(this)
    this.broadcast  = new BroadcastAPI(this)
    this.condenser  = new CondenserAPI(this)
    this.database   = new DatabaseAPI(this)
    this.nexus      = new Nexus(this)
    this.tools      = new Tools(this)
  }

  /**
   * Make a RPC call to the server.
   *
   * @param api     The API to call, e.g. `database_api`.
   * @param method  The API method, e.g. `get_dynamic_global_properties`.
   * @param params  Array of parameters to pass to the method, optional.
   *
   */
  public async call(api: string, method: string, params: any = []): Promise<any> {
    const request: RPCCall = { id: 0, jsonrpc: '2.0', method: `${api}.${method}`, params }
    const body = JSON.stringify(request, (key, value) => {
      // encode Buffers as hex strings instead of an array of bytes
      if (key && value && typeof value === 'object' && value.type === 'Buffer') {
          return Buffer.from(value.data).toString('hex')
      }
      return value
    })
    const opts: any = {
      body,
      cache: 'no-cache',
      headers: {
          'Accept': 'application/json, text/plain, */*',
          'Content-Type': 'application/json'
      },
      method: 'POST',
      mode: 'cors'
    }

    // Self is not defined within Node environments
    // This check is needed because the user agent cannot be set in a browser
    if (typeof self === undefined) { opts.headers = { 'User-Agent': `dblurt/${packageVersion}` } }
    if (this.options.agent) { opts.agent = this.options.agent }
    let fetchTimeout: any
    if ( api !== 'network_broadcast_api' && !method.startsWith('broadcast_transaction') ) {
      // bit of a hack to work around some nodes high error rates
      // only effective in node.js (until timeout spec lands in browsers)
      fetchTimeout = (tries: number): number => (tries + 1) * 500
    }

    const { response, currentAddress }: { response: RPCResponse; currentAddress: string } = await retryingFetch(
      this.currentAddress,
      this.address,
      opts,
      this.timeout,
      this.failoverThreshold,
      this.consoleOnFailover,
      this.backoff,
      fetchTimeout
    )

    // After failover, change the currently active address
    if (currentAddress !== this.currentAddress) { this.currentAddress = currentAddress }
    // resolve FC error messages into something more readable
    if (response.error) {
      const formatValue = (value: any): string => {
        switch (typeof value) {
          case 'object':
            return JSON.stringify(value)
          default:
            return String(value)
        }
      }
      const { data } = response.error
      let { message } = response.error
      if (data && data.stack && data.stack.length > 0) {
        const top = data.stack[0]
        const topData = copy(top.data)
        message = top.format.replace( /\$\{([a-z_]+)\}/gi, (match: string, key: string) => {
          let rv = match
          if (topData[key]) {
            rv = formatValue(topData[key])
              delete topData[key]
          }
          return rv
        })
        const unformattedData = Object.keys(topData)
          .map(key => ({ key, value: formatValue(topData[key]) }))
          .map(item => `${item.key}=${item.value}`)
        if (unformattedData.length > 0) {
          message += ' ' + unformattedData.join(' ')
        }
      }
      throw new VError({ info: data, name: 'RPCError' }, message)
    }
    assert.strictEqual(response.id, request.id, 'got invalid response id')
    return response.result
  }

}

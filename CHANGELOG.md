# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [0.9.29](https://gitlab.com/beblurt/dblurt/compare/v0.9.28...v0.9.29) (2023-08-13)


### Features

* **lookup_accounts:** add condenser methods lookup_accounts & lookup_witness_accounts ([b9c2fc8](https://gitlab.com/beblurt/dblurt/commit/b9c2fc8206872768e6eba8f840fe5736156c2ffd))

### [0.9.28](https://gitlab.com/beblurt/dblurt/compare/v0.9.27...v0.9.28) (2023-08-13)


### Features

* **accounthistoryapi:** add account_history_api methods ([67cd333](https://gitlab.com/beblurt/dblurt/commit/67cd33308e668f5626797977eb850d5676c6d5b9))

### [0.9.27](https://gitlab.com/beblurt/dblurt/compare/v0.9.26...v0.9.27) (2023-08-01)


### Features

* **witness:** add buildWitnessSetPropertiesOp Tools function ([a5d2732](https://gitlab.com/beblurt/dblurt/commit/a5d2732114dee0edea0922809d99c376ef25945d))

### [0.9.26](https://gitlab.com/beblurt/dblurt/compare/v0.9.25...v0.9.26) (2023-06-10)


### Code Refactoring

* **nexus:** add cover_url to NexusCommunity and NexusCommunityExtended interfaces ([dead488](https://gitlab.com/beblurt/dblurt/commit/dead48848fa1136cfcb81a0e154f03b6727376ab))

### [0.9.25](https://gitlab.com/beblurt/dblurt/compare/v0.9.24...v0.9.25) (2023-06-08)

### [0.9.24](https://gitlab.com/beblurt/dblurt/compare/v0.9.23...v0.9.24) (2023-05-28)


### Features

* **nexus:** nexus interfaces ([79827b7](https://gitlab.com/beblurt/dblurt/commit/79827b76a6e8ef7ed713620333dd9dd69bd262f8))

### [0.9.23](https://gitlab.com/beblurt/dblurt/compare/v0.9.22...v0.9.23) (2023-05-25)


### Features

* **listsubscribers:** add param last & limit listSubscribers ([be27819](https://gitlab.com/beblurt/dblurt/commit/be2781903aa7fe94908b916ee770adaf822679cb))

### [0.9.22](https://gitlab.com/beblurt/dblurt/compare/v0.9.21...v0.9.22) (2023-05-14)


### Bug Fixes

* **nexuspost:** missing optional parent_author and parent_permlink ([fb93c95](https://gitlab.com/beblurt/dblurt/commit/fb93c95e6fb93dea8af5ab5395ea42bfc4917650))

### [0.9.21](https://gitlab.com/beblurt/dblurt/compare/v0.9.20...v0.9.21) (2023-04-30)


### Bug Fixes

* **pinned:** is_pinned missing in NexusPost ([52ca8dd](https://gitlab.com/beblurt/dblurt/commit/52ca8dda05f15b4e80c9fd905e3aa62b3c0b2703))

### [0.9.20](https://gitlab.com/beblurt/dblurt/compare/v0.9.19...v0.9.20) (2023-04-29)


### Bug Fixes

* **reblogged_by:** reblogged_by ([83d538f](https://gitlab.com/beblurt/dblurt/commit/83d538ff95ca0505443d1d17b56ba902bb0ce7c0))

### [0.9.19](https://gitlab.com/beblurt/dblurt/compare/v0.9.18...v0.9.19) (2023-04-29)


### Bug Fixes

* **reblogged:** reblogged_by missing ([d801ded](https://gitlab.com/beblurt/dblurt/commit/d801ded6bd9e03ffb01984c36b2f7a57102997f9))

### [0.9.18](https://gitlab.com/beblurt/dblurt/compare/v0.9.17...v0.9.18) (2023-04-29)


### Code Refactoring

* **nexuspost:** refactor NexusPost type definition ([5b3f130](https://gitlab.com/beblurt/dblurt/commit/5b3f1302639ad06e33666e3e7769bfe49c797007))

### [0.9.17](https://gitlab.com/beblurt/dblurt/compare/v0.9.16...v0.9.17) (2023-04-27)


### Bug Fixes

* **long:** typeof Long ([2d1e94f](https://gitlab.com/beblurt/dblurt/commit/2d1e94f18f51b47c902e55481eb1b98c77cac3cb))

### [0.9.16](https://gitlab.com/beblurt/dblurt/compare/v0.9.14...v0.9.16) (2023-04-12)


### Features

* **serializer:** delegate_vesting_shares serializer ([e8ee625](https://gitlab.com/beblurt/dblurt/commit/e8ee625f3bb650d63d4011de524818bf993f5465))

### [0.9.14](https://gitlab.com/beblurt/dblurt/compare/v0.9.13...v0.9.14) (2023-03-25)


### Features

* **rewards:** serializer for claim_reward_balance operation ([2c71810](https://gitlab.com/beblurt/dblurt/commit/2c718109a7c7a402446c3a3a69d79ebf7e622c16))

### [0.9.13](https://gitlab.com/beblurt/dblurt/compare/v0.9.12...v0.9.13) (2023-03-15)


### Bug Fixes

* **createproposal:** add extensions to CreateProposalOperation ([46ab675](https://gitlab.com/beblurt/dblurt/commit/46ab6752d10274e757d0830b68f37c0db9d3341f))

### [0.9.12](https://gitlab.com/beblurt/dblurt/compare/v0.9.11...v0.9.12) (2023-03-15)


### Features

* **proposals:** proposal broadcast operations ([b49a561](https://gitlab.com/beblurt/dblurt/commit/b49a5611a214d276067d77257ce3259e1d614d01))

### [0.9.11](https://gitlab.com/beblurt/dblurt/compare/v0.9.10...v0.9.11) (2023-03-12)


### Features

* **encode:** function to encode/decode with memo keys ([92481d1](https://gitlab.com/beblurt/dblurt/commit/92481d177270e395941f41fef69e2e78a1384a66))

### [0.9.10](https://gitlab.com/beblurt/dblurt/compare/v0.9.9...v0.9.10) (2022-12-26)


### Features

* **virtual:** add RegExp operation name, definition virtual op, convertVESTS() ([4a3ab40](https://gitlab.com/beblurt/dblurt/commit/4a3ab40b1837c2b2b99baf79cae2c18e350b2e39))

### [0.9.9](https://gitlab.com/beblurt/dblurt/compare/v0.9.8...v0.9.9) (2022-12-13)


### Bug Fixes

* **stringarray:** fix getRebloggedBy output by string array ([569037b](https://gitlab.com/beblurt/dblurt/commit/569037b14abdd85fb7134b8885afc5b25af72a86))

### [0.9.8](https://gitlab.com/beblurt/dblurt/compare/v0.9.7...v0.9.8) (2022-12-13)


### Features

* **getrebloggedby:** add getRebloggedBy ([314b5a3](https://gitlab.com/beblurt/dblurt/commit/314b5a33e91d00cd70bc562034375984dd029f08))

### [0.9.7](https://gitlab.com/beblurt/dblurt/compare/v0.9.6...v0.9.7) (2022-12-07)


### Code Refactoring

* **upvote:** refactor upvote calculation ([bee05ea](https://gitlab.com/beblurt/dblurt/commit/bee05eab6ef4079a3663b6cc0fbbf0a310638d94))

### [0.9.6](https://gitlab.com/beblurt/dblurt/compare/v0.9.5...v0.9.6) (2022-12-04)


### Features

* **tools:** adding some tools (mana, vote value) ([8f4b79c](https://gitlab.com/beblurt/dblurt/commit/8f4b79ccf357670b5d76908f2871671379609da0))

### [0.9.5](https://gitlab.com/beblurt/dblurt/compare/v0.9.4...v0.9.5) (2022-11-29)


### Bug Fixes

* **objectserializer:** objectSerializer percent_blurt in comment_options ([109ea00](https://gitlab.com/beblurt/dblurt/commit/109ea00825cad1adab101a98bb5bbe015b485dde))

### [0.9.4](https://gitlab.com/beblurt/dblurt/compare/v0.9.3...v0.9.4) (2022-11-27)


### Bug Fixes

* **serializer:** operationSerializers comment_options percent_blurt ([a88392c](https://gitlab.com/beblurt/dblurt/commit/a88392c55153ed13fb510a180ecf298652fb7adf))

### [0.9.3](https://gitlab.com/beblurt/dblurt/compare/v0.9.2...v0.9.3) (2022-11-27)


### Features

* **setusertitle:** add setUserTitle Nexus function ([7a55c3f](https://gitlab.com/beblurt/dblurt/commit/7a55c3f1a357c05dc78f458029ed8ccb8a5597d5))

### [0.9.2](https://gitlab.com/beblurt/dblurt/compare/v0.9.1...v0.9.2) (2022-11-27)


### Features

* **setrole:** setRole Nexus function ([a663a24](https://gitlab.com/beblurt/dblurt/commit/a663a24c4def53cf6fa129dbcb31b785f2cc92f2))


### Docs

* **docs:** regenerate docs ([c819aa4](https://gitlab.com/beblurt/dblurt/commit/c819aa4e5b1f245f666cf8e19d2dc9f070c6c5a5))

### 0.9.1 (2022-11-27)


### Features

* **delete:** add some more nexus functions ([1e69507](https://gitlab.com/beblurt/dblurt/commit/1e695078024d5779d0640d1203983ac8c926f563))

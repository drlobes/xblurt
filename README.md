# [@beblurt/dblurt](https://gitlab.com/beblurt/dblurt)

Client library for the Blurt blockchain (JSON-RPC connector) that runs in both node.js and the browser. Client library implementing Nexus connector for community management as well.

## Installation

### Via npm

For node.js or the browser with [browserify](https://github.com/substack/node-browserify) or [webpack](https://github.com/webpack/webpack).

```bash
$ npm i @beblurt/dblurt --save
```

### Using browser (cdn or self-hosted script):

Grab `dist/dblurt.js` from git and include in your html:

```html
<script src="@beblurt/dblurt.js"></script>
```

Or from the [unpkg](https://unpkg.com) cdn:

```html
<script src="https://unpkg.com/@beblurt/dblurt@latest/dist/dblurt.js"></script>
```

Make sure to set the version you want when including from the cdn, you can also use `dblurt@latest` but that is not always desirable. See [unpkg.com](https://unpkg.com) for more information.

## Usage

### In the browser

```html
<script src="https://unpkg.com/@beblurt/dblurt@latest/dist/dblurt.js"></script>
<script>
  var client = new dblurt.Client([
    "https://rpc.beblurt.com",
    "https://rpc.blurt.world",
    "https://blurt-rpc.saboin.com"
  ], { timeout: 1500 });
  client.condenser
    .getDiscussions("trending", { tag: "blurtography", limit: 1 })
    .then(function(discussions) {
      document.body.innerHTML += "<h1>" + discussions[0].title + "</h1>";
      document.body.innerHTML += "<h2>by " + discussions[0].author + "</h2>";
      document.body.innerHTML +=
        '<pre style="white-space: pre-wrap">' + discussions[0].body + "</pre>";
    });
</script>
```

This example on [stackblitz.io](https://stackblitz.com/edit/dblurt-browser-js-vanilla?file=index.html)

### In node.js

With TypeScript:

```typescript
import { Client } from "@beblurt/dblurt";

const client = new Client([
    "https://rpc.beblurt.com",
    "https://rpc.blurt.world",
    "https://blurt-rpc.saboin.com"
  ], { timeout: 1500 });

for await (const block of client.blockchain.getBlocks()) {
  console.log(`New block, id: ${block.block_id}`);
}
```

With ES2016 (node.js 7+):

```javascript
const { Client } = require("@beblurt/dblurt");

const client = new Client([
    "https://rpc.beblurt.com",
    "https://rpc.blurt.world",
    "https://blurt-rpc.saboin.com"
  ], { timeout: 1500 });

async function main() {
  const props = await client.database.getChainProperties();
  console.log(`Maximum blocksize consensus: ${props.maximum_block_size} bytes`);
}

main().catch(console.error);
```

With node.js streams:

```javascript
var dblurt = require("@beblurt/dblurt");
var es = require("event-stream"); // npm install event-stream
var util = require("util");

var client = new dblurt.Client([
    "https://rpc.beblurt.com",
    "https://rpc.blurt.world",
    "https://blurt-rpc.saboin.com"
  ], { timeout: 1500 });

var stream = client.blockchain.getBlockStream();

stream
  .pipe(
    es.map(function(block, callback) {
      callback(null, util.inspect(block, { colors: true, depth: null }) + "\n");
    })
  )
  .pipe(process.stdout);
```

## Bundling

The easiest way to bundle dblurt (with browserify, webpack etc.) is to just `npm install @beblurt/dblurt` and `require('@beblurt/dblurt')`. However, that is not always desirable since it will not allow your bundler to de-duplicate any shared dependencies dblurt and your app might have.

To allow for deduplication you can `require('@beblurt/dblurt/lib/index-browser')`, or if you plan to provide your own polyfills: `require('@beblurt/dblurt/lib/index')`. See `src/index-browser.ts` for a list of polyfills expected.

---

## Documentation

Detailed documentation: https://dblurt.beblurt.com

### Client
RPC Client, can be used in both node.js and the browser.

#### Client Options
options for the Client (Object)

```typescript
{
  /** Blurt chain id. Defaults `cd8d90f29ae273abec3eaa7731e25934c63eb654d55080caff2ebb7f5df6381f` */
  chainId?: string
  /** Blurt address prefix. Defaults to main network: `BLT`*/
  addressPrefix?: string
  /** how long to wait in milliseconds before giving up on a rpc call. Defaults to 60 * 1000 ms. */
  timeout?: number
  /**
   * Specifies the amount of times the urls (RPC nodes) should be
   * iterated and retried in case of timeout errors.
   * (important) Requires url parameter to be an array (string[])!
   * Can be set to 0 to iterate and retry forever. Defaults to 3 rounds.
   */
  failoverThreshold?: number
  /** Whether a console.log should be made when RPC failed over to another one */
  consoleOnFailover?: boolean
  /** Retry backoff function, returns milliseconds. Default = {@link defaultBackoff}. */
  backoff?: (tries: number) => number
  /** Node.js http(s) agent, use if you want http keep-alive. @see https://nodejs.org/api/http.html#http_new_agent_options. */
  agent?: any
}
```

#### Client Usage

```typescript
new Client(address: url | url[], options: ClientOptions = {});
```

### Utilities

#### PublicKey
To manipulate a 'owner' | 'active' | 'posting' | 'memo' account public key

eg: verification of the signature of a user message

```typescript
import { PublicKey } from "@beblurt/dblurt";
const pKey = PublicKey.from("account public key")
const verified = pKey.verify(message, signature)
```

#### PrivateKey
To manipulate a 'owner' | 'active' | 'posting' | 'memo' account private key

eg: signature of a message

```typescript
import { PrivateKey } from "@beblurt/dblurt";
const pKey = PrivateKey.from("account private key")
const signature = pKey.sign(message)
```

#### Encoding/Decoding Memo

For private memo messages between accounts using private/public memo keys

eg: Blurt transfer with encrypted memo

The `encodeMemo()` and `decodeMemo()` functions are called directly from the library.

```typescript
import { encodeMemo, decodeMemo } from "@beblurt/dblurt"
```

##### Encode Memo

The function `encodeMemo()` takes as parameters:

- memo - The memo to encrypt (need to start with #).
- senderPrivateMemoKey - The private Memo key of the sender (PrivateKey | string).
- receiverPublicMemoKey - The publicKey Memo key of the recipient (PublicKey | string).

```typescript
const memo = "#MY MEMO TO ENCODE"
const encryptedMemo = encodeMemo(memo, senderPrivateMemoKey, receiverPublicMemoKey)
```

##### Decode Memo

The function `decodeMemo()` takes as parameters:

- memo - The encrypted memo to decrypt (need to start with #).
- receiverPrivateMemoKey - The private Memo key of the recipient (PrivateKey | string).

```typescript
const encryptedMemo = "#XYZXYZXYZXYZXYZXYZXYZXYZXYZXYZXYZXYZ"
const memo = decodeMemo(encryptedMemo, receiverPrivateMemoKey)
```


#### cryptoUtils
Misc crypto utility functions.

```typescript
const cryptoUtils = {
  decodePrivate,
  doubleSha256,
  encodePrivate,
  encodePublic,
  generateTrxId,
  isCanonicalSignature,
  isWif,
  regExpAccount,
  ripemd160,
  sha256,
  signTransaction,
  transactionDigest
}
```

### Tools Class
Some useful functions like mana with regeneration calculation, vote value calculation, etc...

example for mana:

```typescript
import { Client } from "@beblurt/dblurt";

const client = new Client([
    "https://rpc.beblurt.com",
    "https://rpc.blurt.world",
    "https://blurt-rpc.saboin.com"
  ], { timeout: 1500 });

client.tools.getAccountMana('beblurt').then(mana => {
  const percentMana = ((mana.current_mana / mana.max_mana) * 100).toFixed(2);
  console.log(`voting mana: ${percentMana} %`);
});
```

example for convert VESTS to BLURT:

```typescript
import { Client } from "@beblurt/dblurt";

const client = new Client([
    "https://rpc.beblurt.com",
    "https://rpc.blurt.world",
    "https://blurt-rpc.saboin.com"
  ], { timeout: 1500 });

/** Get Dynamic Global Properties */
const DGP   = await client.condenser.getDynamicGlobalProperties() 

/** Convert 1,000,000 VESTS to BLURT */
client.tools.convertVESTS(1000000, DGP).then(BLURT => {
  console.log(`1,000,000 VESTS = ${BLURT} BLURT`);
});
```


### BLURT APPBASE API documentation

  - [Account History API](https://dblurt.beblurt.com/classes/accounthistoryapi.html)
  - [Blockchain](https://dblurt.beblurt.com/classes/blockchain.html)
  - [Broadcast Ops](https://dblurt.beblurt.com/classes/broadcastapi.html)
  - [Condenser Api](https://dblurt.beblurt.com/classes/condenserapi.html)
  - [Database Api](https://dblurt.beblurt.com/classes/databaseapi.html)
  - [Nexus](https://dblurt.beblurt.com/classes/nexus.html)
  - [Tools](https://dblurt.beblurt.com/classes/tools.html)

_Share and Enjoy!_

## Contributing

Pull requests for new features, bug fixes, and suggestions are welcome!
declare module 'dblurt/version' {
	 const _default: string;
	export default _default;

}
declare module 'dblurt/chain/asset' {
	/**
	 * @file Blurt asset type definitions and helpers.
	 * @author Johan Nordberg <code@johan-nordberg.com>
	 * @license
	 * Copyright (c) 2017 Johan Nordberg. All Rights Reserved.
	 *
	 * Redistribution and use in source and binary forms, with or without modification,
	 * are permitted provided that the following conditions are met:
	 *
	 *  1. Redistribution of source code must retain the above copyright notice, this
	 *     list of conditions and the following disclaimer.
	 *
	 *  2. Redistribution in binary form must reproduce the above copyright notice,
	 *     this list of conditions and the following disclaimer in the documentation
	 *     and/or other materials provided with the distribution.
	 *
	 *  3. Neither the name of the copyright holder nor the names of its contributors
	 *     may be used to endorse or promote products derived from this software without
	 *     specific prior written permission.
	 *
	 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
	 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
	 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
	 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
	 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
	 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
	 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
	 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
	 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
	 * OF THE POSSIBILITY OF SUCH DAMAGE.
	 *
	 * You acknowledge that this software is not designed, licensed or intended for use
	 * in the design, construction, operation or maintenance of any military facility.
	 */
	export interface SMTAsset {
	    amount: string | number;
	    precision: number;
	    nai: string;
	}
	/**
	 * Asset symbol string.
	 */
	export type AssetSymbol = 'HIVE' | 'VESTS' | 'HBD' | 'TESTS' | 'TBD' | 'STEEM' | 'SBD' | 'BLURT';
	export const AssetSymbolRegexp: RegExp;
	/**
	 * Class representing a blurt asset, e.g. `1.000 BLURT` or `12.112233 VESTS`.
	 */
	export class Asset {
	    readonly amount: number;
	    readonly symbol: AssetSymbol;
	    constructor(amount: number, symbol: AssetSymbol);
	    /**
	     * Create a new Asset instance from a string, e.g. `42.000 BLURT`.
	     */
	    static fromString(string: string, expectedSymbol?: AssetSymbol): Asset;
	    /**
	     * Convenience to create new Asset.
	     * @param symbol Symbol to use when created from number. Will also be used to validate
	     *               the asset, throws if the passed value has a different symbol than this.
	     */
	    static from(value: string | Asset | number, symbol?: AssetSymbol): Asset;
	    /**
	     * Return the smaller of the two assets.
	     */
	    static min(a: Asset, b: Asset): Asset;
	    /**
	     * Return the larger of the two assets.
	     */
	    static max(a: Asset, b: Asset): Asset;
	    /**
	     * Return asset precision.
	     */
	    getPrecision(): number;
	    /**
	     * returns a representation of this asset using only STEEM SBD for
	     * legacy purposes
	     */
	    symbols(): Asset;
	    /**
	     * Return a string representation of this asset, e.g. `42.000 BLURT`.
	     */
	    toString(): string;
	    /**
	     * Return a new Asset instance with amount added.
	     */
	    add(amount: Asset | string | number): Asset;
	    /**
	     * Return a new Asset instance with amount subtracted.
	     */
	    subtract(amount: Asset | string | number): Asset;
	    /**
	     * Return a new Asset with the amount multiplied by factor.
	     */
	    multiply(factor: Asset | string | number): Asset;
	    /**
	     * Return a new Asset with the amount divided.
	     */
	    divide(divisor: Asset | string | number): Asset;
	    /**
	     * For JSON serialization, same as toString().
	     */
	    toJSON(): string;
	}
	export type PriceType = Price | {
	    base: Asset | string;
	    quote: Asset | string;
	};
	/**
	 * Represents quotation of the relative value of asset against another asset.
	 * Similar to 'currency pair' used to determine value of currencies.
	 *
	 *  For example:
	 *  1 EUR / 1.25 USD where:
	 *  1 EUR is an asset specified as a base
	 *  1.25 USD us an asset specified as a qute
	 *
	 *  can determine value of EUR against USD.
	 */
	export class Price {
	    readonly base: Asset;
	    readonly quote: Asset;
	    /**
	     * @param base  - represents a value of the price object to be expressed relatively to quote
	     *                asset. Cannot have amount == 0 if you want to build valid price.
	     * @param quote - represents an relative asset. Cannot have amount == 0, otherwise
	     *                asertion fail.
	     *
	     * Both base and quote shall have different symbol defined.
	     */
	    constructor(base: Asset, quote: Asset);
	    /**
	     * Convenience to create new Price.
	     */
	    static from(value: PriceType): Price;
	    /**
	     * Return a string representation of this price pair.
	     */
	    toString(): string;
	    /**
	     * Return a new Asset with the price converted between the symbols in the pair.
	     * Throws if passed asset symbol is not base or quote.
	     */
	    convert(asset: Asset): Asset;
	}

}
declare module 'dblurt/chain/account' {
	/**
	 * @file Blurt account type definitions.
	 * @author Johan Nordberg <code@johan-nordberg.com>
	 * @license
	 * Copyright (c) 2017 Johan Nordberg. All Rights Reserved.
	 *
	 * Redistribution and use in source and binary forms, with or without modification,
	 * are permitted provided that the following conditions are met:
	 *
	 *  1. Redistribution of source code must retain the above copyright notice, this
	 *     list of conditions and the following disclaimer.
	 *
	 *  2. Redistribution in binary form must reproduce the above copyright notice,
	 *     this list of conditions and the following disclaimer in the documentation
	 *     and/or other materials provided with the distribution.
	 *
	 *  3. Neither the name of the copyright holder nor the names of its contributors
	 *     may be used to endorse or promote products derived from this software without
	 *     specific prior written permission.
	 *
	 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
	 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
	 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
	 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
	 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
	 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
	 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
	 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
	 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
	 * OF THE POSSIBILITY OF SUCH DAMAGE.
	 *
	 * You acknowledge that this software is not designed, licensed or intended for use
	 * in the design, construction, operation or maintenance of any military facility.
	 */
	import { PublicKey } from 'dblurt/crypto';
	import { Asset } from 'dblurt/chain/asset';
	export interface AuthorityType {
	    weight_threshold: number;
	    account_auths: [string, number][];
	    key_auths: [string | PublicKey, number][];
	}
	export class Authority implements AuthorityType {
	    weight_threshold: number;
	    account_auths: [string, number][];
	    key_auths: [string | PublicKey, number][];
	    constructor({ weight_threshold, account_auths, key_auths }: AuthorityType);
	    /** Convenience to create a new instance from PublicKey or authority object. */
	    static from(value: string | PublicKey | AuthorityType): Authority;
	}
	export interface Account {
	    id: number;
	    name: string;
	    owner: Authority;
	    active: Authority;
	    posting: Authority;
	    memo_key: string;
	    json_metadata: string;
	    posting_json_metadata: string;
	    proxy: string;
	    last_owner_update: string;
	    last_account_update: string;
	    created: string;
	    mined: boolean;
	    recovery_account: string;
	    last_account_recovery: string;
	    reset_account: string;
	    comment_count: number;
	    lifetime_vote_count: number;
	    post_count: number;
	    can_vote: boolean;
	    voting_manabar: {
	        current_mana: string | number;
	        last_update_time: number;
	    };
	    voting_power: number;
	    balance: string | Asset;
	    savings_balance: string | Asset;
	    savings_withdraw_requests: number;
	    reward_blurt_balance: string | Asset;
	    reward_vesting_balance: string | Asset;
	    reward_vesting_blurt: string | Asset;
	    vesting_shares: string | Asset;
	    delegated_vesting_shares: string | Asset;
	    received_vesting_shares: string | Asset;
	    vesting_withdraw_rate: string | Asset;
	    next_vesting_withdrawal: string;
	    withdrawn: number | string;
	    to_withdraw: number | string;
	    withdraw_routes: number;
	    curation_rewards: number | string;
	    posting_rewards: number | string;
	    proxied_vsf_votes: number[];
	    witnesses_voted_for: number;
	    last_post: string;
	    last_root_post: string;
	    last_vote_time: string;
	    post_bandwidth: number;
	    pending_claimed_accounts: number;
	}
	export interface ExtendedAccount extends Account {
	    /**
	     * Convert vesting_shares to vesting blurt.
	     */
	    vesting_balance: string | Asset;
	    /**
	     * Transfer to/from vesting.
	     */
	    transfer_history: any[];
	    /**
	     * Limit order / cancel / fill.
	     */
	    market_history: any[];
	    post_history: any[];
	    vote_history: any[];
	    other_history: any[];
	    witness_votes: string[];
	    tags_usage: string[];
	    guest_bloggers: string[];
	    open_orders?: any[];
	    comments?: any[];
	    blog?: any[];
	    feed?: any[];
	    recent_replies?: any[];
	    recommended?: any[];
	}

}
declare module 'dblurt/chain/misc' {
	/**
	 * @file Misc blurt type definitions.
	 * @author BeBlurt <https://beblurt.com/@beblurt>
	 * @description adaptation of Johan Nordberg <code@johan-nordberg.com> Misc blurt type definitions.
	 * @license
	 * Copyright (c) 2017 Johan Nordberg. All Rights Reserved.
	 *
	 * Redistribution and use in source and binary forms, with or without modification,
	 * are permitted provided that the following conditions are met:
	 *
	 *  1. Redistribution of source code must retain the above copyright notice, this
	 *     list of conditions and the following disclaimer.
	 *
	 *  2. Redistribution in binary form must reproduce the above copyright notice,
	 *     this list of conditions and the following disclaimer in the documentation
	 *     and/or other materials provided with the distribution.
	 *
	 *  3. Neither the name of the copyright holder nor the names of its contributors
	 *     may be used to endorse or promote products derived from this software without
	 *     specific prior written permission.
	 *
	 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
	 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
	 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
	 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
	 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
	 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
	 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
	 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
	 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
	 * OF THE POSSIBILITY OF SUCH DAMAGE.
	 *
	 * You acknowledge that this software is not designed, licensed or intended for use
	 * in the design, construction, operation or maintenance of any military facility.
	 */
	/// <reference types="node" />
	import { PublicKey } from 'dblurt/crypto';
	import { Account } from 'dblurt/chain/account';
	import { Asset, Price } from 'dblurt/chain/asset';
	/**
	 * Large number that may be unsafe to represent natively in JavaScript.
	 */
	export type Bignum = string;
	/**
	 * Buffer wrapper that serializes to a hex-encoded string.
	 */
	export class HexBuffer {
	    buffer: Buffer;
	    constructor(buffer: Buffer);
	    /**
	     * Convenience to create a new HexBuffer, does not copy data if value passed is already a buffer.
	     */
	    static from(value: Buffer | HexBuffer | number[] | string): HexBuffer;
	    toString(encoding?: string): string;
	    toJSON(): string;
	}
	/** Return the vesting share price. */
	export const getVestingSharePrice: (props: DynamicGlobalProperties) => Price;
	/** Returns the vests of specified account. Default: Subtract delegated & add received */
	export const getVests: (account: Account, subtract_delegated?: boolean, add_received?: boolean) => number;
	/** Chain roperties that are decided by the witnesses. */
	export interface ChainProperties {
	    account_creation_fee: string | Asset;
	    maximum_block_size: number;
	    account_subsidy_budget: number;
	    account_subsidy_decay: number;
	    operation_flat_fee: string | Asset;
	    bandwidth_kbytes_fee: string | Asset;
	    proposal_fee: string | Asset;
	}
	export interface WitnessUpdateProperties {
	    account_creation_fee: string | Asset;
	    maximum_block_size: number;
	}
	export interface WitnessSetProperties {
	    account_creation_fee: string | Asset;
	    maximum_block_size: number;
	    account_subsidy_budget: number;
	    account_subsidy_decay: number;
	    operation_flat_fee: string | Asset;
	    bandwidth_kbytes_fee: string | Asset;
	    proposal_fee?: string | Asset;
	    url: string;
	    key?: PublicKey | string;
	    new_signing_key?: PublicKey | string | null;
	}
	/** Config of Blurt RPC node */
	export interface RpcNodeConfig {
	    IS_TEST_NET: false;
	    BLURT_REDUCED_VOTE_POWER_RATE: number;
	    BLURT_100_PERCENT: number;
	    BLURT_1_PERCENT: number;
	    BLURT_ACCOUNT_RECOVERY_REQUEST_EXPIRATION_PERIOD: string;
	    BLURT_ACTIVE_CHALLENGE_COOLDOWN: string;
	    BLURT_ACTIVE_CHALLENGE_FEE: {
	        amount: string;
	        precision: number;
	        nai: string;
	    };
	    BLURT_ADDRESS_PREFIX: string;
	    BLURT_APR_PERCENT_MULTIPLY_PER_BLOCK: string;
	    BLURT_APR_PERCENT_MULTIPLY_PER_HOUR: string;
	    BLURT_APR_PERCENT_MULTIPLY_PER_ROUND: string;
	    BLURT_APR_PERCENT_SHIFT_PER_BLOCK: number;
	    BLURT_APR_PERCENT_SHIFT_PER_HOUR: number;
	    BLURT_APR_PERCENT_SHIFT_PER_ROUND: number;
	    BLURT_BANDWIDTH_AVERAGE_WINDOW_SECONDS: number;
	    BLURT_BANDWIDTH_PRECISION: number;
	    BLURT_BENEFICIARY_LIMIT: number;
	    BLURT_BLOCKCHAIN_PRECISION: number;
	    BLURT_BLOCKCHAIN_PRECISION_DIGITS: number;
	    BLURT_BLOCKCHAIN_HARDFORK_VERSION: string;
	    BLURT_BLOCKCHAIN_VERSION: string;
	    BLURT_BLOCK_INTERVAL: number;
	    BLURT_BLOCKS_PER_DAY: number;
	    BLURT_BLOCKS_PER_HOUR: number;
	    BLURT_BLOCKS_PER_YEAR: number;
	    BLURT_CASHOUT_WINDOW_SECONDS: number;
	    BLURT_CHAIN_ID: string;
	    BLURT_COMMENT_TITLE_LIMIT: number;
	    BLURT_REWARD_CONSTANT: string;
	    BLURT_CONTENT_REWARD_PERCENT_HF21: number;
	    BLURT_CUSTOM_OP_DATA_MAX_LENGTH: number;
	    BLURT_CUSTOM_OP_ID_MAX_LENGTH: number;
	    BLURT_GENESIS_TIME: string;
	    BLURT_HARDFORK_REQUIRED_WITNESSES: number;
	    BLURT_HF21_CONVERGENT_LINEAR_RECENT_CLAIMS: string;
	    BLURT_INFLATION_NARROWING_PERIOD: number;
	    BLURT_INFLATION_RATE_START_PERCENT: number;
	    BLURT_INFLATION_RATE_STOP_PERCENT: number;
	    BLURT_INIT_MINER_NAME: string;
	    BLURT_INIT_PUBLIC_KEY_STR: string;
	    BLURT_INIT_SUPPLY: string;
	    BLURT_IRREVERSIBLE_THRESHOLD: number;
	    BLURT_MAX_ACCOUNT_CREATION_FEE: number;
	    BLURT_MAX_ACCOUNT_NAME_LENGTH: number;
	    BLURT_MAX_ACCOUNT_WITNESS_VOTES: number;
	    BLURT_MAX_ASSET_WHITELIST_AUTHORITIES: number;
	    BLURT_MAX_AUTHORITY_MEMBERSHIP: number;
	    BLURT_MAX_BLOCK_SIZE: number;
	    BLURT_SOFT_MAX_BLOCK_SIZE: number;
	    BLURT_MAX_CASHOUT_WINDOW_SECONDS: number;
	    BLURT_MAX_COMMENT_DEPTH: number;
	    BLURT_MAX_INSTANCE_ID: string;
	    BLURT_MAX_MEMO_SIZE: number;
	    BLURT_MAX_WITNESSES: number;
	    BLURT_MAX_PERMLINK_LENGTH: number;
	    BLURT_MAX_PROXY_RECURSION_DEPTH: number;
	    BLURT_MAX_RUNNER_WITNESSES_HF17: number;
	    BLURT_MAX_SATOSHIS: string;
	    BLURT_MAX_SHARE_SUPPLY: string;
	    BLURT_MAX_SIG_CHECK_DEPTH: number;
	    BLURT_MAX_SIG_CHECK_ACCOUNTS: number;
	    BLURT_MAX_TIME_UNTIL_EXPIRATION: number;
	    BLURT_MAX_TRANSACTION_SIZE: number;
	    BLURT_MAX_UNDO_HISTORY: number;
	    BLURT_MAX_URL_LENGTH: number;
	    BLURT_MAX_VOTE_CHANGES: number;
	    BLURT_MAX_VOTED_WITNESSES_HF17: number;
	    BLURT_MAX_WITHDRAW_ROUTES: number;
	    BLURT_MAX_WITNESS_URL_LENGTH: number;
	    BLURT_MIN_ACCOUNT_CREATION_FEE: number;
	    BLURT_MIN_ACCOUNT_NAME_LENGTH: number;
	    BLURT_MIN_BLOCK_SIZE_LIMIT: number;
	    BLURT_MIN_BLOCK_SIZE: number;
	    BLURT_MIN_CONTENT_REWARD: {
	        amount: string;
	        precision: number;
	        nai: string;
	    };
	    BLURT_MIN_CURATE_REWARD: {
	        amount: string;
	        precision: number;
	        nai: string;
	    };
	    BLURT_MIN_PERMLINK_LENGTH: number;
	    BLURT_MIN_REPLY_INTERVAL_HF20: number;
	    BLURT_MIN_ROOT_COMMENT_INTERVAL: number;
	    BLURT_MIN_COMMENT_EDIT_INTERVAL: number;
	    BLURT_MIN_VOTE_INTERVAL_SEC: number;
	    BLURT_MINER_ACCOUNT: string;
	    BLURT_MIN_PAYOUT: {
	        amount: string;
	        precision: number;
	        nai: string;
	    };
	    BLURT_MIN_PRODUCER_REWARD: {
	        amount: string;
	        precision: number;
	        nai: string;
	    };
	    BLURT_MIN_TRANSACTION_EXPIRATION_LIMIT: number;
	    BLURT_MIN_TRANSACTION_SIZE_LIMIT: number;
	    BLURT_MIN_UNDO_HISTORY: number;
	    BLURT_NULL_ACCOUNT: string;
	    BLURT_OWNER_AUTH_RECOVERY_PERIOD: string;
	    BLURT_OWNER_CHALLENGE_COOLDOWN: string;
	    BLURT_OWNER_CHALLENGE_FEE: {
	        amount: string;
	        precision: number;
	        nai: string;
	    };
	    BLURT_OWNER_UPDATE_LIMIT: number;
	    BLURT_POST_REWARD_FUND_NAME: string;
	    BLURT_PROXY_TO_SELF_ACCOUNT: string;
	    BLURT_SECONDS_PER_YEAR: number;
	    BLURT_PROPOSAL_FUND_PERCENT_HF21: number;
	    BLURT_RECENT_RSHARES_DECAY_TIME_HF19: string;
	    BLURT_REVERSE_AUCTION_WINDOW_SECONDS_HF21: number;
	    BLURT_ROOT_POST_PARENT: string;
	    BLURT_SAVINGS_WITHDRAW_REQUEST_LIMIT: number;
	    BLURT_SAVINGS_WITHDRAW_TIME: string;
	    BLURT_SOFT_MAX_COMMENT_DEPTH: number;
	    BLURT_TEMP_ACCOUNT: string;
	    BLURT_UPVOTE_LOCKOUT_HF17: string;
	    BLURT_UPVOTE_LOCKOUT_SECONDS: number;
	    BLURT_VESTING_FUND_PERCENT_HF16: number;
	    BLURT_VESTING_WITHDRAW_INTERVALS: number;
	    BLURT_VESTING_WITHDRAW_INTERVALS_HF5: number;
	    BLURT_VESTING_WITHDRAW_INTERVAL_SECONDS: number;
	    BLURT_VOTE_DUST_THRESHOLD: number;
	    BLURT_VOTING_MANA_REGENERATION_SECONDS: number;
	    BLURT_SYMBOL: {
	        nai: string;
	        decimals: number;
	    };
	    VESTS_SYMBOL: {
	        nai: string;
	        decimals: number;
	    };
	    BLURT_VIRTUAL_SCHEDULE_LAP_LENGTH2: string;
	    BLURT_DELEGATION_RETURN_PERIOD_HF20: number;
	    BLURT_RD_MIN_DECAY_BITS: number;
	    BLURT_RD_MAX_DECAY_BITS: number;
	    BLURT_RD_DECAY_DENOM_SHIFT: number;
	    BLURT_RD_MAX_POOL_BITS: number;
	    BLURT_RD_MAX_BUDGET_1: string;
	    BLURT_RD_MAX_BUDGET_2: number;
	    BLURT_RD_MAX_BUDGET_3: number;
	    BLURT_RD_MAX_BUDGET: number;
	    BLURT_RD_MIN_DECAY: number;
	    BLURT_RD_MIN_BUDGET: number;
	    BLURT_RD_MAX_DECAY: number;
	    BLURT_ACCOUNT_SUBSIDY_PRECISION: number;
	    BLURT_WITNESS_SUBSIDY_BUDGET_PERCENT: number;
	    BLURT_WITNESS_SUBSIDY_DECAY_PERCENT: number;
	    BLURT_DEFAULT_ACCOUNT_SUBSIDY_DECAY: number;
	    BLURT_DEFAULT_ACCOUNT_SUBSIDY_BUDGET: number;
	    BLURT_DECAY_BACKSTOP_PERCENT: number;
	    BLURT_BLOCK_GENERATION_POSTPONED_TX_LIMIT: number;
	    BLURT_PENDING_TRANSACTION_EXECUTION_LIMIT: number;
	    BLURT_TREASURY_ACCOUNT: string;
	    BLURT_TREASURY_FEE: number;
	    BLURT_PROPOSAL_MAINTENANCE_PERIOD: number;
	    BLURT_PROPOSAL_MAINTENANCE_CLEANUP: number;
	    BLURT_PROPOSAL_SUBJECT_MAX_LENGTH: number;
	    BLURT_PROPOSAL_MAX_IDS_NUMBER: number;
	    BLURT_PROPOSAL_MAX_END_DATE: number;
	    BLURT_PROPOSAL_EXPIRATION_UNFUNDED: number;
	    BLURT_INIT_POST_REWARD_BALANCE: number;
	}
	/** Vesting Delegation */
	export interface VestingDelegation {
	    /** Delegation id. */
	    id: number;
	    /** Account that is delegating vests to delegatee. */
	    delegator: string;
	    /** Account that is receiving vests from delegator. */
	    delegatee: string;
	    /** Amount of VESTS delegated. */
	    vesting_shares: Asset | string;
	    /** Earliest date delegation can be removed. */
	    min_delegation_time: string;
	}
	/** Dynamic Global Properties. */
	export interface DynamicGlobalProperties {
	    id: number;
	    /** Current block height. */
	    head_block_number: number;
	    head_block_id: string;
	    /** UTC Server time, e.g. 2020-01-15T00:42:00 */
	    time: string;
	    /** Currently elected witness. */
	    current_witness: string;
	    /** Current Supply */
	    current_supply: string;
	    /** Total asset held. */
	    total_vesting_fund_blurt: Asset;
	    total_vesting_shares: Asset;
	    total_reward_fund_blurt: Asset;
	    /** The running total of REWARD^2. */
	    total_reward_shares2: string;
	    pending_rewarded_vesting_shares: Asset;
	    pending_rewarded_vesting_blurt: Asset;
	    /**
	     * Maximum block size is decided by the set of active witnesses which change every round.
	     * Each witness posts what they think the maximum size should be as part of their witness
	     * properties, the median size is chosen to be the maximum block size for the round.
	     *
	     * @note the minimum value for maximum_block_size is defined by the protocol to prevent the
	     * network from getting stuck by witnesses attempting to set this too low.
	     */
	    maximum_block_size: number;
	    /**
	     * The current absolute slot number. Equal to the total
	     * number of slots since genesis. Also equal to the total
	     * number of missed slots plus head_block_number.
	     */
	    current_aslot: number;
	    /**
	     * Used to compute witness participation.
	     */
	    recent_slots_filled: Bignum;
	    participation_count: number;
	    last_irreversible_block_num: number;
	    vote_power_reserve_rate: number;
	    delegation_return_period: number;
	    reverse_auction_seconds: number;
	    available_account_subsidies: number;
	    next_maintenance_time: string;
	    last_budget_time: string;
	    content_reward_percent: number;
	    vesting_reward_percent: number;
	    sps_fund_percent: number;
	    sps_interval_ledger: string;
	}
	/** Reward Fund (post) */
	export interface RewardFund {
	    id: number;
	    name: 'post';
	    reward_balance: Asset;
	    recent_claims: string;
	    last_update: string;
	    content_constant: string;
	    percent_curation_rewards: number;
	    percent_content_rewards: number;
	    author_reward_curve: 'convergent_linear';
	    curation_reward_curve: 'convergent_square_root';
	}

}
declare module 'dblurt/chain/serializer' {
	/**
	 * @file Blurt protocol serialization.
	 * @author Johan Nordberg <code@johan-nordberg.com>
	 * @license
	 * Copyright (c) 2017 Johan Nordberg. All Rights Reserved.
	 *
	 * Redistribution and use in source and binary forms, with or without modification,
	 * are permitted provided that the following conditions are met:
	 *
	 *  1. Redistribution of source code must retain the above copyright notice, this
	 *     list of conditions and the following disclaimer.
	 *
	 *  2. Redistribution in binary form must reproduce the above copyright notice,
	 *     this list of conditions and the following disclaimer in the documentation
	 *     and/or other materials provided with the distribution.
	 *
	 *  3. Neither the name of the copyright holder nor the names of its contributors
	 *     may be used to endorse or promote products derived from this software without
	 *     specific prior written permission.
	 *
	 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
	 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
	 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
	 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
	 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
	 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
	 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
	 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
	 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
	 * OF THE POSSIBILITY OF SUCH DAMAGE.
	 *
	 * You acknowledge that this software is not designed, licensed or intended for use
	 * in the design, construction, operation or maintenance of any military facility.
	 */
	/// <reference types="node" />
	import * as ByteBuffer from 'bytebuffer';
	import { PublicKey } from 'dblurt/crypto';
	import { Asset } from 'dblurt/chain/asset';
	import { HexBuffer } from 'dblurt/chain/misc';
	export type Serializer = (buffer: ByteBuffer, data: any) => void;
	export const Types: {
	    Array: (itemSerializer: Serializer) => (buffer: ByteBuffer, data: any[]) => void;
	    Asset: (buffer: ByteBuffer, data: Asset | string | number) => void;
	    Authority: (buffer: ByteBuffer, data: {
	        [key: string]: any;
	    }) => void;
	    Binary: (size?: number | undefined) => (buffer: ByteBuffer, data: Buffer | HexBuffer) => void;
	    Boolean: (buffer: ByteBuffer, data: boolean) => void;
	    Date: (buffer: ByteBuffer, data: string) => void;
	    FlatMap: (keySerializer: Serializer, valueSerializer: Serializer) => (buffer: ByteBuffer, data: [any, any][]) => void;
	    Int16: (buffer: ByteBuffer, data: number) => void;
	    Int32: (buffer: ByteBuffer, data: number) => void;
	    Int64: (buffer: ByteBuffer, data: number) => void;
	    Int8: (buffer: ByteBuffer, data: number) => void;
	    Memo: (buffer: ByteBuffer, data: {
	        [key: string]: any;
	    }) => void;
	    Object: (keySerializers: [string, Serializer][]) => (buffer: ByteBuffer, data: {
	        [key: string]: any;
	    }) => void;
	    Operation: (buffer: ByteBuffer, operation: any) => void;
	    Optional: (valueSerializer: Serializer) => (buffer: ByteBuffer, data: any) => void;
	    PublicKey: (buffer: ByteBuffer, data: PublicKey | string | null) => void;
	    StaticVariant: (itemSerializers: Serializer[]) => (buffer: ByteBuffer, data: [number, any]) => void;
	    String: (buffer: ByteBuffer, data: string) => void;
	    Transaction: (buffer: ByteBuffer, data: {
	        [key: string]: any;
	    }) => void;
	    UInt16: (buffer: ByteBuffer, data: number) => void;
	    UInt32: (buffer: ByteBuffer, data: number) => void;
	    UInt64: (buffer: ByteBuffer, data: number) => void;
	    UInt8: (buffer: ByteBuffer, data: number) => void;
	    Void: (buffer: ByteBuffer) => void;
	};

}
declare module 'dblurt/chain/deserializer' {
	/**
	 * @file Blurt protocol deserialization.
	 * @author Johan Nordberg <code@johan-nordberg.com>
	 * @license
	 * Copyright (c) 2017 Johan Nordberg. All Rights Reserved.
	 *
	 * Redistribution and use in source and binary forms, with or without modification,
	 * are permitted provided that the following conditions are met:
	 *
	 *  1. Redistribution of source code must retain the above copyright notice, this
	 *     list of conditions and the following disclaimer.
	 *
	 *  2. Redistribution in binary form must reproduce the above copyright notice,
	 *     this list of conditions and the following disclaimer in the documentation
	 *     and/or other materials provided with the distribution.
	 *
	 *  3. Neither the name of the copyright holder nor the names of its contributors
	 *     may be used to endorse or promote products derived from this software without
	 *     specific prior written permission.
	 *
	 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
	 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
	 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
	 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
	 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
	 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
	 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
	 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
	 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
	 * OF THE POSSIBILITY OF SUCH DAMAGE.
	 *
	 * You acknowledge that this software is not designed, licensed or intended for use
	 * in the design, construction, operation or maintenance of any military facility.
	 */
	import ByteBuffer from 'bytebuffer';
	export type Deserializer = (buffer: ByteBuffer) => void;
	export const EncryptedMemoDeserializer: any;

}
declare module 'dblurt/chain/transaction' {
	/**
	 * @file Blurt transaction type definitions.
	 * @author Johan Nordberg <code@johan-nordberg.com>
	 * @license
	 * Copyright (c) 2017 Johan Nordberg. All Rights Reserved.
	 *
	 * Redistribution and use in source and binary forms, with or without modification,
	 * are permitted provided that the following conditions are met:
	 *
	 *  1. Redistribution of source code must retain the above copyright notice, this
	 *     list of conditions and the following disclaimer.
	 *
	 *  2. Redistribution in binary form must reproduce the above copyright notice,
	 *     this list of conditions and the following disclaimer in the documentation
	 *     and/or other materials provided with the distribution.
	 *
	 *  3. Neither the name of the copyright holder nor the names of its contributors
	 *     may be used to endorse or promote products derived from this software without
	 *     specific prior written permission.
	 *
	 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
	 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
	 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
	 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
	 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
	 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
	 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
	 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
	 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
	 * OF THE POSSIBILITY OF SUCH DAMAGE.
	 *
	 * You acknowledge that this software is not designed, licensed or intended for use
	 * in the design, construction, operation or maintenance of any military facility.
	 */
	import { Operation } from 'dblurt/chain/operation';
	export interface Transaction {
	    ref_block_num: number;
	    ref_block_prefix: number;
	    expiration: string;
	    operations: Operation[];
	    extensions: any[];
	}
	export interface SignedTransaction extends Transaction {
	    signatures: string[];
	    transaction_id: string;
	    block_num: number;
	    transaction_num: number;
	}
	export interface TransactionConfirmation {
	    id: string;
	    jse_info?: any;
	    jse_shortmsg?: any;
	    message?: string;
	    name?: string;
	}

}
declare module 'dblurt/utils' {
	/**
	 * @file Misc utility functions.
	 * @author BeBlurt <https://beblurt.com/@beblurt>
	 * @description adaptation from Johan Nordberg <code@johan-nordberg.com> Misc utility functions.
	 * @license
	 * Copyright (c) 2017 Johan Nordberg. All Rights Reserved.
	 *
	 * Redistribution and use in source and binary forms, with or without modification,
	 * are permitted provided that the following conditions are met:
	 *
	 *  1. Redistribution of source code must retain the above copyright notice, this
	 *     list of conditions and the following disclaimer.
	 *
	 *  2. Redistribution in binary form must reproduce the above copyright notice,
	 *     this list of conditions and the following disclaimer in the documentation
	 *     and/or other materials provided with the distribution.
	 *
	 *  3. Neither the name of the copyright holder nor the names of its contributors
	 *     may be used to endorse or promote products derived from this software without
	 *     specific prior written permission.
	 *
	 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
	 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
	 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
	 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
	 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
	 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
	 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
	 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
	 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
	 * OF THE POSSIBILITY OF SUCH DAMAGE.
	 *
	 * You acknowledge that this software is not designed, licensed or intended for use
	 * in the design, construction, operation or maintenance of any military facility.
	 */
	/// <reference types="node" />
	/// <reference types="mocha" />
	import { EventEmitter } from 'events';
	export const operationOrders: {
	    vote: number;
	    comment: number;
	    transfer: number;
	    transfer_to_vesting: number;
	    withdraw_vesting: number;
	    account_create: number;
	    account_update: number;
	    witness_update: number;
	    account_witness_vote: number;
	    account_witness_proxy: number;
	    custom: number;
	    delete_comment: number;
	    custom_json: number;
	    comment_options: number;
	    set_withdraw_vesting_route: number;
	    claim_account: number;
	    create_claimed_account: number;
	    request_account_recovery: number;
	    recover_account: number;
	    change_recovery_account: number;
	    escrow_transfer: number;
	    escrow_dispute: number;
	    escrow_release: number;
	    escrow_approve: number;
	    transfer_to_savings: number;
	    transfer_from_savings: number;
	    cancel_transfer_from_savings: number;
	    custom_binary: number;
	    decline_voting_rights: number;
	    reset_account: number;
	    set_reset_account: number;
	    claim_reward_balance: number;
	    delegate_vesting_shares: number;
	    witness_set_properties: number;
	    create_proposal: number;
	    update_proposal_votes: number;
	    remove_proposal: number;
	    author_reward: number;
	    curation_reward: number;
	    comment_reward: number;
	    fill_vesting_withdraw: number;
	    shutdown_witness: number;
	    fill_transfer_from_savings: number;
	    hardfork: number;
	    comment_payout_update: number;
	    return_vesting_delegation: number;
	    comment_benefactor_reward: number;
	    producer_reward: number;
	    clear_null_account_balance: number;
	    proposal_pay: number;
	    sps_fund: number;
	    fee_pay: number;
	};
	export const virtualOps: {
	    author_reward: number;
	    curation_reward: number;
	    comment_reward: number;
	    fill_vesting_withdraw: number;
	    shutdown_witness: number;
	    fill_transfer_from_savings: number;
	    hardfork: number;
	    comment_payout_update: number;
	    return_vesting_delegation: number;
	    comment_benefactor_reward: number;
	    producer_reward: number;
	    clear_null_account_balance: number;
	    proposal_pay: number;
	    sps_fund: number;
	    fee_pay: number;
	};
	/**
	 * Make bitmask filter to be used with getAccountHistory call
	 * @param allowedOperations Array of operations index numbers
	 */
	export const makeBitMaskFilter: (allowedOperations: number[]) => (number | null)[];
	/**
	 * Make bitmask filter to be used with enum_virtual_ops call
	 * @param allowedOperations Array of operations index numbers
	 */
	export const makeBitwiseFilter: (allowedOperations: number[]) => number;
	/** Return a promise that will resove when a specific event is emitted. */
	export const waitForEvent: <T>(emitter: EventEmitter, eventName: string | symbol) => Promise<T>;
	/** Sleep for N milliseconds. */
	export const sleep: (ms: number) => Promise<void>;
	/** Return a stream that emits iterator values. */
	export const iteratorStream: <T>(iterator: AsyncIterableIterator<T>) => NodeJS.ReadableStream;
	/** Return a deep copy of a JSON-serializable object */
	export const copy: <T>(object: T) => T;
	export const retryingFetch: (currentAddress: string, allAddresses: string | string[], opts: any, timeout: number, failoverThreshold: number, consoleOnFailover: boolean, backoff: (tries: number) => number, fetchTimeout?: ((tries: number) => number) | undefined) => Promise<{
	    response: any;
	    currentAddress: string;
	}>;

}
declare module 'dblurt/crypto' {
	 const Long: any;
	import { SignedTransaction, Transaction } from 'dblurt/chain/transaction';
	/**
	 * ECDSA (secp256k1) public key.
	 */
	export class PublicKey {
	    readonly key: Buffer;
	    readonly prefix: string;
	    readonly uncompressed: Buffer;
	    constructor(key: Buffer, prefix?: string);
	    /**
	     * Create a new instance from a WIF-encoded key.
	     */
	    static fromString(wif: string): PublicKey;
	    static fromBuffer(key: Buffer): {
	        key: Buffer;
	    };
	    /**
	     * Create a new instance.
	     */
	    static from(value: string | PublicKey): PublicKey;
	    /**
	     * Verify a 32-byte signature.
	     * @param message 32-byte message to verify.
	     * @param signature Signature to verify.
	     */
	    verify(message: Buffer, signature: Signature): boolean;
	    /**
	     * Return a WIF-encoded representation of the key.
	     */
	    toString(): string;
	    /**
	     * Return JSON representation of this key, same as toString().
	     */
	    toJSON(): string;
	    /**
	     * Used by `utils.inspect` and `console.log` in node.js.
	     */
	    inspect(): string;
	}
	export type KeyRole = 'owner' | 'active' | 'posting' | 'memo';
	/**
	 * ECDSA (secp256k1) private key.
	 */
	export class PrivateKey {
	    private key;
	    constructor(key: Buffer);
	    /**
	     * Convenience to create a new instance from WIF string or buffer.
	     */
	    static from(value: string | Buffer): PrivateKey;
	    /**
	     * Create a new instance from a WIF-encoded key.
	     */
	    static fromString(wif: string): PrivateKey;
	    /**
	     * Create a new instance from a seed.
	     */
	    static fromSeed(seed: string): PrivateKey;
	    /**
	     * Create key from username and password.
	     */
	    static fromLogin(username: string, password: string, role?: KeyRole): PrivateKey;
	    /**
	     * Sign message.
	     * @param message 32-byte message.
	     */
	    sign(message: Buffer): Signature;
	    /**
	     * Derive the public key for this private key.
	     */
	    createPublic(prefix?: string): PublicKey;
	    /**
	     * Return a WIF-encoded representation of the key.
	     */
	    toString(): string;
	    /**
	     * Get shared secret for memo cryptography
	     */
	    getSharedSecret(publicKey: PublicKey): Buffer;
	    /**
	     * Used by `utils.inspect` and `console.log` in node.js. Does not show the full key
	     * to get the full encoded key you need to explicitly call {@link toString}.
	     */
	    inspect(): string;
	}
	/**
	 * ECDSA (secp256k1) signature.
	 */
	export class Signature {
	    data: Buffer;
	    recovery: number;
	    constructor(data: Buffer, recovery: number);
	    static fromBuffer(buffer: Buffer): Signature;
	    static fromString(string: string): Signature;
	    /**
	     * Recover public key from signature by providing original signed message.
	     * @param message 32-byte message that was used to create the signature.
	     */
	    recover(message: Buffer, prefix?: string): PublicKey;
	    toBuffer(): Buffer;
	    toString(): string;
	}
	/**
	 * Decrypts an encrypted memo.
	 *
	 * @param memo - The encrypted memo to decrypt (need to start with #).
	 * @param receiverPrivateMemoKey - The private Memo key of the recipient.
	 * @returns The decrypted message.
	 */
	export const decodeMemo: (memo: string, receiverPrivateMemoKey: PrivateKey | string) => string;
	/**
	 * Encrypt a memo.
	 *
	 * @param memo - The memo to encrypt (need to start with #).
	 * @param senderPrivateMemoKey - The private Memo key of the sender.
	 * @param receiverPublicMemoKey - The publicKey Memo key of the recipient.
	 * @returns The encrypted message.
	 */
	export const encodeMemo: (memo: string, senderPrivateMemoKey: PrivateKey | string, receiverPublicMemoKey: PublicKey | string) => string;
	/** Misc crypto utility functions. */
	export const cryptoUtils: {
	    decodePrivate: (encodedKey: string) => Buffer;
	    doubleSha256: (input: Buffer | string) => Buffer;
	    encodePrivate: (key: Buffer) => string;
	    encodePublic: (key: Buffer, prefix: string) => string;
	    generateTrxId: (transaction: Transaction) => string;
	    isCanonicalSignature: (signature: Buffer) => boolean;
	    isWif: (privWif: string) => boolean;
	    regExpAccount: RegExp;
	    regExpAtAccount: RegExp;
	    ripemd160: (input: Buffer | string) => Buffer;
	    sha256: (input: Buffer | string) => Buffer;
	    signTransaction: (transaction: Transaction, keys: any, chainId?: Buffer) => SignedTransaction;
	    toBuffer: (ab: ArrayBuffer) => Buffer;
	    toByteBuffer: (o: string | number | typeof Long) => typeof Long;
	    toUint8Array: (buf: Buffer) => Uint8Array;
	    transactionDigest: (transaction: Transaction | SignedTransaction, chainId?: Buffer) => Buffer;
	    uniqueNonce: () => string;
	};
	export {};

}
declare module 'dblurt/chain/comment' {
	/**
	 * @file Blurt type definitions related to comments and posting.
	 * @author BeBlurt <https://beblurt.com/@beblurt>
	 * @description adaptation from Johan Nordberg <code@johan-nordberg.com> type definitions related to comments and posting.
	 * @license
	 * Copyright (c) 2017 Johan Nordberg. All Rights Reserved.
	 *
	 * Redistribution and use in source and binary forms, with or without modification,
	 * are permitted provided that the following conditions are met:
	 *
	 *  1. Redistribution of source code must retain the above copyright notice, this
	 *     list of conditions and the following disclaimer.
	 *
	 *  2. Redistribution in binary form must reproduce the above copyright notice,
	 *     this list of conditions and the following disclaimer in the documentation
	 *     and/or other materials provided with the distribution.
	 *
	 *  3. Neither the name of the copyright holder nor the names of its contributors
	 *     may be used to endorse or promote products derived from this software without
	 *     specific prior written permission.
	 *
	 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
	 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
	 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
	 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
	 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
	 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
	 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
	 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
	 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
	 * OF THE POSSIBILITY OF SUCH DAMAGE.
	 *
	 * You acknowledge that this software is not designed, licensed or intended for use
	 * in the design, construction, operation or maintenance of any military facility.
	 */
	import { Asset } from 'dblurt/chain/asset';
	export interface Comment {
	    id: number;
	    author: string;
	    permlink: string;
	    category: string;
	    parent_author: string;
	    parent_permlink: string;
	    title: string;
	    body: string;
	    json_metadata: string;
	    last_update: string;
	    created: string;
	    active: string;
	    last_payout: string;
	    depth: number;
	    children: number;
	    net_rshares: number | string;
	    abs_rshares: number | string;
	    vote_rshares: number | string;
	    children_abs_rshares: number | string;
	    cashout_time: string;
	    max_cashout_time: string;
	    total_vote_weight: number;
	    reward_weight: number;
	    total_payout_value: Asset | string;
	    curator_payout_value: Asset | string;
	    author_rewards: number | string;
	    net_votes: number;
	    root_author: string;
	    root_permlink: string;
	    max_accepted_payout: Asset | string;
	    percent_blurt: number;
	    allow_replies: boolean;
	    allow_votes: boolean;
	    allow_curation_rewards: boolean;
	    beneficiaries: BeneficiaryRoute[];
	    active_votes: [];
	}
	export interface ActiveVote {
	    voter: string;
	    weight: number;
	    rshares: number | string;
	    percent: number;
	    time: string;
	}
	export interface Post extends Omit<Comment, 'active_votes'> {
	    url: string;
	    root_title: string;
	    pending_payout_value: Asset | string;
	    total_pending_payout_value: Asset | string;
	    active_votes: ActiveVote[];
	    replies: string[];
	    promoted: Asset | string;
	    body_length: string;
	    reblogged_by: any[];
	}
	/**
	 * Discussion a.k.a. Post.
	 */
	export interface Discussion {
	    id: number;
	    author: string;
	    permlink: string;
	    category: string;
	    parent_author: string | '';
	    parent_permlink: string | '';
	    title: string | '';
	    body: string;
	    json_metadata: string;
	    last_update: string;
	    created: string;
	    active: string;
	    last_payout: string;
	    depth: number;
	    children: number;
	    net_rshares: number | string;
	    abs_rshares: number | string;
	    vote_rshares: number | string;
	    children_abs_rshares: number | string;
	    cashout_time: string;
	    max_cashout_time: string;
	    total_vote_weight: number;
	    reward_weight: number;
	    total_payout_value: Asset | string;
	    curator_payout_value: Asset | string;
	    author_rewards: number;
	    net_votes: number;
	    root_author: string;
	    root_permlink: string;
	    max_accepted_payout: string;
	    percent_blurt: number;
	    allow_replies: boolean;
	    allow_votes: boolean;
	    allow_curation_rewards: boolean;
	    beneficiaries: [[string, number]];
	    url: string;
	    root_title: string;
	    pending_payout_value: Asset | string;
	    total_pending_payout_value: Asset | string;
	    active_votes: ActiveVote[];
	    replies: [];
	    promoted: Asset | string;
	    body_length: 0;
	    reblogged_by: [];
	}
	export interface BeneficiaryRoute {
	    account: string;
	    weight: number;
	}

}
declare module 'dblurt/chain/operation' {
	/**
	 * @file Blurt operation type definitions.
	 * @author BeBlurt <https://beblurt.com/@beblurt>
	 * @description adaptation of Johan Nordberg <code@johan-nordberg.com> Blurt operation type definitions.
	 * @license
	 * Copyright (c) 2017 Johan Nordberg. All Rights Reserved.
	 *
	 * Redistribution and use in source and binary forms, with or without modification,
	 * are permitted provided that the following conditions are met:
	 *
	 *  1. Redistribution of source code must retain the above copyright notice, this
	 *     list of conditions and the following disclaimer.
	 *
	 *  2. Redistribution in binary form must reproduce the above copyright notice,
	 *     this list of conditions and the following disclaimer in the documentation
	 *     and/or other materials provided with the distribution.
	 *
	 *  3. Neither the name of the copyright holder nor the names of its contributors
	 *     may be used to endorse or promote products derived from this software without
	 *     specific prior written permission.
	 *
	 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
	 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
	 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
	 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
	 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
	 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
	 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
	 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
	 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
	 * OF THE POSSIBILITY OF SUCH DAMAGE.
	 *
	 * You acknowledge that this software is not designed, licensed or intended for use
	 * in the design, construction, operation or maintenance of any military facility.
	 */
	/// <reference types="node" />
	import { PublicKey } from 'dblurt/crypto';
	import { AuthorityType } from 'dblurt/chain/account';
	import { Asset } from 'dblurt/chain/asset';
	import { BeneficiaryRoute } from 'dblurt/chain/comment';
	import { WitnessUpdateProperties, HexBuffer } from 'dblurt/chain/misc';
	/**
	 * Operation name.
	 * Ref: https://gitlab.com/blurt/blurt/-/blob/dev/libraries/protocol/include/blurt/protocol/operations.hpp
	 */
	export type OperationName = 'vote' | 'comment' | 'transfer' | 'transfer_to_vesting' | 'withdraw_vesting' | 'account_create' | 'account_update' | 'witness_update' | 'account_witness_vote' | 'account_witness_proxy' | 'custom' | 'delete_comment' | 'custom_json' | 'comment_options' | 'set_withdraw_vesting_route' | 'claim_account' | 'create_claimed_account' | 'request_account_recovery' | 'recover_account' | 'change_recovery_account' | 'escrow_transfer' | 'escrow_dispute' | 'escrow_release' | 'escrow_approve' | 'transfer_to_savings' | 'transfer_from_savings' | 'cancel_transfer_from_savings' | 'custom_binary' | 'decline_voting_rights' | 'reset_account' | 'set_reset_account' | 'claim_reward_balance' | 'delegate_vesting_shares' | 'witness_set_properties' | 'create_proposal' | 'update_proposal_votes' | 'remove_proposal';
	export const OperationNameRegexp: RegExp;
	/**
	 * Virtual operation name.
	 */
	export type VirtualOperationName = 'author_reward' | 'curation_reward' | 'comment_reward' | 'fill_vesting_withdraw' | 'shutdown_witness' | 'fill_transfer_from_savings' | 'hardfork' | 'comment_payout_update' | 'return_vesting_delegation' | 'comment_benefactor_reward' | 'producer_reward' | 'clear_null_account_balance' | 'proposal_pay' | 'sps_fund' | 'fee_pay';
	export const VirtualOperationNameRegexp: RegExp;
	/**  Generic operation. */
	export interface Operation {
	    0: OperationName | VirtualOperationName;
	    1: {
	        [key: string]: any;
	    };
	}
	export interface AppliedOperation {
	    trx_id: string;
	    block: number;
	    trx_in_block: number;
	    op_in_trx: number;
	    virtual_op: number;
	    timestamp: string;
	    op: Operation;
	}
	/** Account Create. */
	export interface AccountCreateOperation extends Operation {
	    0: 'account_create';
	    1: {
	        fee: string | Asset;
	        creator: string;
	        new_account_name: string;
	        owner: AuthorityType;
	        active: AuthorityType;
	        posting: AuthorityType;
	        memo_key: string | PublicKey;
	        json_metadata: string;
	    };
	}
	/** Account Update. */
	export interface AccountUpdateOperation extends Operation {
	    0: 'account_update';
	    1: {
	        account: string;
	        owner?: AuthorityType;
	        active?: AuthorityType;
	        posting?: AuthorityType;
	        memo_key?: string | PublicKey;
	        json_metadata: string | '';
	        posting_json_metadata: string | '';
	        extensions: any[];
	    };
	}
	export interface AccountWitnessProxyOperation extends Operation {
	    0: 'account_witness_proxy';
	    1: {
	        account: string;
	        proxy: string;
	    };
	}
	/** Vote/Unvote for a Witness. */
	export interface AccountWitnessVoteOperation extends Operation {
	    0: 'account_witness_vote';
	    1: {
	        account: string;
	        witness: string;
	        approve: boolean;
	    };
	}
	/** Author receive reward for his content. */
	export interface AuthorRewardOperation extends Operation {
	    0: 'author_reward';
	    1: {
	        author: string;
	        permlink: string;
	        blurt_payout: string | Asset;
	        vesting_payout: string | Asset;
	    };
	}
	export interface CancelTransferFromSavingsOperation extends Operation {
	    0: 'cancel_transfer_from_savings';
	    1: {
	        from: string;
	        request_id: number;
	    };
	}
	export interface ChangeRecoveryAccountOperation extends Operation {
	    0: 'change_recovery_account';
	    1: {
	        /** The account that would be recovered in case of compromise. */
	        account_to_recover: string;
	        /** The account that creates the recover request. */
	        new_recovery_account: string;
	        /** Extensions. Not currently used. */
	        extensions: any[];
	    };
	}
	export interface ClaimAccountOperation extends Operation {
	    0: 'claim_account';
	    1: {
	        creator: string;
	        fee: string | Asset;
	        /** Extensions. Not currently used. */
	        extensions: any[];
	    };
	}
	export interface ClaimRewardBalanceOperation extends Operation {
	    0: 'claim_reward_balance';
	    1: {
	        account: string;
	        reward_blurt: string | Asset;
	        reward_vests: string | Asset;
	    };
	}
	export interface CommentBenefactorRewardOperation extends Operation {
	    0: 'comment_benefactor_reward';
	    1: {
	        benefactor: string;
	        author: string;
	        permlink: string;
	        blurt_payout: string | Asset;
	        vesting_payout: string | Asset;
	    };
	}
	export interface CommentOperation extends Operation {
	    0: 'comment';
	    1: {
	        /** the author that comment is being submitted to, when posting a new blog this is an empty string. */
	        parent_author: string;
	        /** specific post that comment is being submitted to, when posting a new blog this become the category of the post (tags[0]). */
	        parent_permlink: string;
	        /** author of the post/comment being submitted (account name). */
	        author: string;
	        /** unique string identifier for the post, linked to the author of the post. */
	        permlink: string;
	        /** human readable title of the post being submitted, this is often blank when commenting. */
	        title: string;
	        /** body of the post/comment being submitted, or diff-match-patch when updating. */
	        body: string;
	        /** JSON object string. */
	        json_metadata: string;
	    };
	}
	export interface CommentOptionsOperation extends Operation {
	    0: 'comment_options';
	    1: {
	        /** author of the post/comment being submitted (account name). */
	        author: string;
	        /** human readable title of the post being submitted, this is often blank when commenting. */
	        permlink: string;
	        /** the maximum payout this post will receive. asset( 1000000000, BLURT_SYMBOL ) */
	        max_accepted_payout: Asset | string;
	        /** The percent of BLURT to key, unkept amounts will be received as BLURT Power. */
	        /** Whether to allow post to receive votes. */
	        allow_votes: boolean;
	        /** Whether to allow post to recieve curation rewards. If false rewards return to reward fund. */
	        allow_curation_rewards: boolean;
	        extensions: [number, 
	        /** Must be specified in sorted order (account ascending; no duplicates) */
	        {
	            beneficiaries: BeneficiaryRoute[];
	        } | {
	            percent_blurt: number;
	        }][];
	    };
	}
	export interface CreateClaimedAccountOperation extends Operation {
	    0: 'create_claimed_account';
	    1: {
	        creator: string;
	        new_account_name: string;
	        owner: AuthorityType;
	        active: AuthorityType;
	        posting: AuthorityType;
	        memo_key: string | PublicKey;
	        json_metadata: string;
	        /** Extensions. Not currently used. */
	        extensions: any[];
	    };
	}
	export interface CreateProposalOperation extends Operation {
	    0: 'create_proposal';
	    1: {
	        creator: string;
	        receiver: string;
	        start_date: string;
	        end_date: string;
	        daily_pay: Asset | string;
	        subject: string;
	        permlink: string;
	        /** Extensions. Not currently used. */
	        extensions: any[];
	    };
	}
	/** Account receive curation reward for his upvote. */
	export interface CurationRewardOperation extends Operation {
	    0: 'curation_reward';
	    1: {
	        curator: string;
	        reward: string | Asset;
	        comment_author: string;
	        comment_permlink: string;
	    };
	}
	/**
	 * @brief provides a generic way to add higher level protocols on top of witness consensus
	 * @ingroup operations
	 *
	 * There is no validation for this operation other than that required auths are valid
	 */
	export interface CustomOperation extends Operation {
	    0: 'custom';
	    1: {
	        required_auths: string[];
	        id: number;
	        data: Buffer | HexBuffer | number[];
	    };
	}
	export interface CustomJsonOperation extends Operation {
	    0: 'custom_json';
	    1: {
	        required_auths: string[];
	        required_posting_auths: string[];
	        /** ID string, must be less than 32 characters long. */
	        id: string;
	        /** JSON encoded string, must be valid JSON. */
	        json: string;
	    };
	}
	export interface DeclineVotingRightsOperation extends Operation {
	    0: 'decline_voting_rights';
	    1: {
	        account: string;
	        decline: boolean;
	    };
	}
	export interface DelegateVestingSharesOperation extends Operation {
	    0: 'delegate_vesting_shares';
	    1: {
	        /** The account delegating vesting shares. */
	        delegator: string;
	        /** The account receiving vesting shares. */
	        delegatee: string;
	        /** The amount of vesting shares delegated. */
	        vesting_shares: string | Asset;
	    };
	}
	export interface DeleteCommentOperation extends Operation {
	    0: 'delete_comment';
	    1: {
	        author: string;
	        permlink: string;
	    };
	}
	/**
	 * The agent and to accounts must approve an escrow transaction for it to be valid on
	 * the blockchain. Once a part approves the escrow, the cannot revoke their approval.
	 * Subsequent escrow approve operations, regardless of the approval, will be rejected.
	 */
	export interface EscrowApproveOperation extends Operation {
	    0: 'escrow_approve';
	    1: {
	        from: string;
	        to: string;
	        agent: string;
	        /** Either to or agent. */
	        who: string;
	        escrow_id: number;
	        approve: boolean;
	    };
	}
	/**
	 * If either the sender or receiver of an escrow payment has an issue, they can
	 * raise it for dispute. Once a payment is in dispute, the agent has authority over
	 * who gets what.
	 */
	export interface EscrowDisputeOperation extends Operation {
	    0: 'escrow_dispute';
	    1: {
	        from: string;
	        to: string;
	        agent: string;
	        who: string;
	        escrow_id: number;
	    };
	}
	/**
	 * This operation can be used by anyone associated with the escrow transfer to
	 * release funds if they have permission.
	 *
	 * The permission scheme is as follows:
	 * If there is no dispute and escrow has not expired, either party can release funds to the other.
	 * If escrow expires and there is no dispute, either party can release funds to either party.
	 * If there is a dispute regardless of expiration, the agent can release funds to either party
	 *    following whichever agreement was in place between the parties.
	 */
	export interface EscrowReleaseOperation extends Operation {
	    0: 'escrow_release';
	    1: {
	        from: string;
	        /** The original 'to'. */
	        to: string;
	        agent: string;
	        /** The account that is attempting to release the funds, determines valid 'receiver'. */
	        who: string;
	        /** The account that should receive funds (might be from, might be to). */
	        receiver: string;
	        escrow_id: number;
	        /** The amount of blurt to release. */
	        blurt_amount: Asset | string;
	    };
	}
	/**
	 * The purpose of this operation is to enable someone to send money contingently to
	 * another individual. The funds leave the *from* account and go into a temporary balance
	 * where they are held until *from* releases it to *to* or *to* refunds it to *from*.
	 *
	 * In the event of a dispute the *agent* can divide the funds between the to/from account.
	 * Disputes can be raised any time before or on the dispute deadline time, after the escrow
	 * has been approved by all parties.
	 *
	 * This operation only creates a proposed escrow transfer. Both the *agent* and *to* must
	 * agree to the terms of the arrangement by approving the escrow.
	 *
	 * The escrow agent is paid the fee on approval of all parties. It is up to the escrow agent
	 * to determine the fee.
	 *
	 * Escrow transactions are uniquely identified by 'from' and 'escrow_id', the 'escrow_id' is defined
	 * by the sender.
	 */
	export interface EscrowTransferOperation extends Operation {
	    0: 'escrow_transfer';
	    1: {
	        from: string;
	        to: string;
	        agent: string;
	        escrow_id: number;
	        blurt_amount: Asset | string;
	        fee: Asset | string;
	        ratification_deadline: string;
	        escrow_expiration: string;
	        json_meta: string;
	    };
	}
	export interface FillTransferFromSavingOperation extends Operation {
	    0: 'fill_transfer_from_savings';
	    1: {
	        from: string;
	        to: string;
	        amount: Asset | string;
	        request_id: number;
	        memo: string;
	    };
	}
	export interface FillVestingWithdrawOperation extends Operation {
	    0: 'fill_vesting_withdraw';
	    1: {
	        from_account: string;
	        to_account: string;
	        withdrawn: Asset | string;
	        deposited: Asset | string;
	    };
	}
	export interface HardforkOperation extends Operation {
	    0: 'hardfork';
	    1: {
	        hardfork_id: number;
	    };
	}
	export interface ProducerRewardOperation extends Operation {
	    0: 'producer_reward';
	    1: {
	        /** The witness account. */
	        producer: string;
	        /** Reward in VESTS. */
	        vesting_shares: Asset | string;
	    };
	}
	/** Payment of a proposal */
	export interface ProposalPayOperation extends Operation {
	    0: 'proposal_pay';
	    1: {
	        receiver: string;
	        payment: Asset | string;
	        trx_id: string;
	        op_in_trx: number;
	    };
	}
	export interface RecoverAccountOperation extends Operation {
	    0: 'recover_account';
	    1: {
	        /** The account to be recovered. */
	        account_to_recover: string;
	        /** The new owner authority as specified in the request account recovery operation. */
	        new_owner_authority: AuthorityType;
	        /**
	         * A previous owner authority that the account holder will use to prove
	         * past ownership of the account to be recovered.
	         */
	        recent_owner_authority: AuthorityType;
	        /** Extensions. Not currently used. */
	        extensions: any[];
	    };
	}
	export interface RemoveProposalOperation extends Operation {
	    0: 'remove_proposal';
	    1: {
	        proposal_owner: string;
	        /** IDs of proposals to be removed. Nonexisting IDs are ignored. */
	        proposal_ids: number[];
	        extensions: any[];
	    };
	}
	/**
	 * All account recovery requests come from a listed recovery account. This
	 * is secure based on the assumption that only a trusted account should be
	 * a recovery account. It is the responsibility of the recovery account to
	 * verify the identity of the account holder of the account to recover by
	 * whichever means they have agreed upon. The blockchain assumes identity
	 * has been verified when this operation is broadcast.
	 *
	 * This operation creates an account recovery request which the account to
	 * recover has 24 hours to respond to before the request expires and is
	 * invalidated.
	 *
	 * There can only be one active recovery request per account at any one time.
	 * Pushing this operation for an account to recover when it already has
	 * an active request will either update the request to a new new owner authority
	 * and extend the request expiration to 24 hours from the current head block
	 * time or it will delete the request. To cancel a request, simply set the
	 * weight threshold of the new owner authority to 0, making it an open authority.
	 *
	 * Additionally, the new owner authority must be satisfiable. In other words,
	 * the sum of the key weights must be greater than or equal to the weight
	 * threshold.
	 *
	 * This operation only needs to be signed by the the recovery account.
	 * The account to recover confirms its identity to the blockchain in
	 * the recover account operation.
	 */
	export interface RequestAccountRecoveryOperation extends Operation {
	    0: 'request_account_recovery';
	    1: {
	        /** The recovery account is listed as the recovery account on the account to recover. */
	        recovery_account: string;
	        /** The account to recover. This is likely due to a compromised owner authority. */
	        account_to_recover: string;
	        /**
	         * The new owner authority the account to recover wishes to have. This is secret
	         * known by the account to recover and will be confirmed in a recover_account_operation.
	         */
	        new_owner_authority: AuthorityType;
	        /** Extensions. Not currently used. */
	        extensions: any[];
	    };
	}
	/**
	 * This operation allows recovery_account to change account_to_reset's owner authority to
	 * new_owner_authority after 60 days of inactivity.
	 */
	export interface ResetAccountOperation extends Operation {
	    0: 'reset_account';
	    1: {
	        reset_account: string;
	        account_to_reset: string;
	        new_owner_authority: AuthorityType;
	    };
	}
	export interface ReturnVestingDelegationOperation extends Operation {
	    0: 'return_vesting_delegation';
	    1: {
	        account: string;
	        vesting_shares: Asset | string;
	    };
	}
	/**
	 * This operation allows 'account' owner to control which account has the power
	 * to execute the 'reset_account_operation' after 60 days.
	 */
	export interface SetResetAccountOperation extends Operation {
	    0: 'set_reset_account';
	    1: {
	        account: string;
	        current_reset_account: string;
	        reset_account: string;
	    };
	}
	/**
	 * Allows an account to setup a vesting withdraw but with the additional
	 * request for the funds to be transferred directly to another account's
	 * balance rather than the withdrawing account. In addition, those funds
	 * can be immediately vested again, circumventing the conversion from
	 * vests to blurt and back, guaranteeing they maintain their value.
	 */
	export interface SetWithdrawVestingRouteOperation extends Operation {
	    0: 'set_withdraw_vesting_route';
	    1: {
	        from_account: string;
	        to_account: string;
	        percent: number;
	        auto_vest: boolean;
	    };
	}
	export interface SpsFundOperation extends Operation {
	    0: 'sps_fund';
	    1: {
	        additional_funds: Asset | string;
	    };
	}
	/** Transfers asset from one account to another. */
	export interface TransferOperation extends Operation {
	    0: 'transfer';
	    1: {
	        from: string;
	        to: string;
	        amount: string | Asset;
	        memo: string;
	    };
	}
	export interface TransferFromSavingsOperation extends Operation {
	    0: 'transfer_from_savings';
	    1: {
	        from: string;
	        request_id: number;
	        to: string;
	        amount: string | Asset;
	        memo: string;
	    };
	}
	export interface TransferToSavingsOperation extends Operation {
	    0: 'transfer_to_savings';
	    1: {
	        from: string;
	        to: string;
	        amount: string | Asset;
	        memo: string;
	    };
	}
	/**
	 *  This operation converts liquid token (BLURT or liquid SMT) into VFS (Vesting Fund Shares,
	 *  VESTS or vesting SMT) at the current exchange rate. With this operation it is possible to
	 *  give another account vesting shares so that faucets can pre-fund new accounts with vesting shares.
	 */
	export interface TransferToVestingOperation extends Operation {
	    0: 'transfer_to_vesting';
	    1: {
	        from: string;
	        to: string;
	        /** must be BLURT or liquid variant of SMT */
	        amount: string | Asset;
	    };
	}
	export interface UpdateProposalVotesOperation extends Operation {
	    0: 'update_proposal_votes';
	    1: {
	        /** IDs of proposals to vote for/against. Nonexisting IDs are ignored. */
	        voter: string;
	        proposal_ids: number[];
	        approve: boolean;
	        extensions: any[];
	    };
	}
	/** Vote on a comment. */
	export interface VoteOperation extends Operation {
	    0: 'vote';
	    1: {
	        voter: string;
	        author: string;
	        permlink: string;
	        /**
	         * Voting weight, 100% = 10000 (100_PERCENT).
	         */
	        weight: number;
	    };
	}
	export interface WithdrawVestingOperation extends Operation {
	    0: 'withdraw_vesting';
	    1: {
	        account: string;
	        /** Amount to power down, must be VESTS. */
	        vesting_shares: string | Asset;
	    };
	}
	export interface WitnessSetPropertiesOperation extends Operation {
	    0: 'witness_set_properties';
	    1: {
	        owner: string;
	        props: [string, Buffer][];
	        extensions: any[];
	    };
	}
	/**
	 * Users who wish to become a witness must pay a fee acceptable to
	 * the current witnesses to apply for the position and allow voting
	 * to begin.
	 *
	 * If the owner isn't a witness they will become a witness.  Witnesses
	 * are charged a fee equal to 1 weeks worth of witness pay which in
	 * turn is derived from the current share supply.  The fee is
	 * only applied if the owner is not already a witness.
	 *
	 * If the block_signing_key is null then the witness is removed from
	 * contention.  The network will pick the top 21 witnesses for
	 * producing blocks.
	 */
	export interface WitnessUpdateOperation extends Operation {
	    0: 'witness_update';
	    1: {
	        owner: string;
	        /** URL for witness, usually a link to a post in the witness-category tag. */
	        url: string;
	        block_signing_key: string | PublicKey | null;
	        props: WitnessUpdateProperties;
	        /** The fee paid to register a new witness, should be 10x current block production pay. */
	        fee: string | Asset;
	    };
	}

}
declare module 'dblurt/helpers/account_history' {
	/**
	 * @file Blurt Account History API helpers.
	 * @author BeBlurt <https://beblurt.com/@beblurt>
	 * @description adaptation from Johan Nordberg <code@johan-nordberg.com> Database API helpers.
	 * @license
	 * Copyright (c) 2017 Johan Nordberg. All Rights Reserved.
	 *
	 * Redistribution and use in source and binary forms, with or without modification,
	 * are permitted provided that the following conditions are met:
	 *
	 *  1. Redistribution of source code must retain the above copyright notice, this
	 *     list of conditions and the following disclaimer.
	 *
	 *  2. Redistribution in binary form must reproduce the above copyright notice,
	 *     this list of conditions and the following disclaimer in the documentation
	 *     and/or other materials provided with the distribution.
	 *
	 *  3. Neither the name of the copyright holder nor the names of its contributors
	 *     may be used to endorse or promote products derived from this software without
	 *     specific prior written permission.
	 *
	 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
	 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
	 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
	 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
	 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
	 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
	 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
	 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
	 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
	 * OF THE POSSIBILITY OF SUCH DAMAGE.
	 *
	 * You acknowledge that this software is not designed, licensed or intended for use
	 * in the design, construction, operation or maintenance of any military facility.
	 */
	import { AppliedOperation } from 'dblurt/chain/operation';
	import { Client } from 'dblurt/client'; type PARAM_ENUM_VIRTUAL_OPS = {
	    block_range_begin: number;
	    block_range_end: number;
	    include_reversible: boolean;
	    group_by_block?: boolean;
	    operation_begin?: number;
	    limit?: number;
	    filter?: number;
	};
	export class AccountHistoryAPI {
	    readonly client: Client;
	    constructor(client: Client);
	    /** Convenience for calling `account_history_api`. */
	    call(method: string, params?: any[] | {
	        [key: string]: any;
	    }): Promise<any>;
	    /**
	     * Returns specific range of blocks to retrieve virtual operations for
	     *
	     * @param block_range_begin:int starting block number (inclusive) to search for virtual operations
	     * @param block_range_end:int last block number (exclusive) to search for virtual operations
	     * @param include_reversible:boolean (optional) If set to true also operations from reversible block will be included if block_num points to such block.
	     * @param group_by_block (optional) true/false
	     * @param operation_begin (optional) starting virtual operation in given block (inclusive)
	     * @param limit (optional) a limit of retrieved operations
	     * @param filter (optional) a filter that decides which an operation matches - Generated by utils.makeBitMaskFilter() - see example below (not yet usable)
	     *
	     */
	    enumVirtualOps(params: PARAM_ENUM_VIRTUAL_OPS): Promise<{
	        ops: AppliedOperation[];
	        next_block_range_begin: number;
	        next_operation_begin: number;
	    }>;
	    /**
	     * Returns all operations contained in a block.
	     * @param block_num:int
	     * @param only_virtual:boolean
	     * @param include_reversible:boolean (optional) If set to true also operations from reversible block will be included if block_num points to such block.
	     */
	    getOpsInBlock(block_num: number, only_virtual?: boolean, include_reversible?: boolean): Promise<{
	        ops: AppliedOperation[];
	    }>;
	}
	export {};

}
declare module 'dblurt/chain/block' {
	/**
	 * @file Blurt block type definitions.
	 * @author Johan Nordberg <code@johan-nordberg.com>
	 * @license
	 * Copyright (c) 2017 Johan Nordberg. All Rights Reserved.
	 *
	 * Redistribution and use in source and binary forms, with or without modification,
	 * are permitted provided that the following conditions are met:
	 *
	 *  1. Redistribution of source code must retain the above copyright notice, this
	 *     list of conditions and the following disclaimer.
	 *
	 *  2. Redistribution in binary form must reproduce the above copyright notice,
	 *     this list of conditions and the following disclaimer in the documentation
	 *     and/or other materials provided with the distribution.
	 *
	 *  3. Neither the name of the copyright holder nor the names of its contributors
	 *     may be used to endorse or promote products derived from this software without
	 *     specific prior written permission.
	 *
	 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
	 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
	 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
	 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
	 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
	 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
	 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
	 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
	 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
	 * OF THE POSSIBILITY OF SUCH DAMAGE.
	 *
	 * You acknowledge that this software is not designed, licensed or intended for use
	 * in the design, construction, operation or maintenance of any military facility.
	 */
	import { SignedTransaction } from 'dblurt/chain/transaction';
	/**
	* Unsigned block header.
	*/
	export interface BlockHeader {
	    previous: string;
	    timestamp: string;
	    witness: string;
	    transaction_merkle_root: string;
	    extensions: any[];
	}
	/**
	* Signed block header.
	*/
	export interface SignedBlockHeader extends BlockHeader {
	    witness_signature: string;
	}
	/**
	* Full signed block.
	*/
	export interface SignedBlock extends SignedBlockHeader {
	    block_id: string;
	    signing_key: string;
	    transaction_ids: string[];
	    transactions: SignedTransaction[];
	}

}
declare module 'dblurt/helpers/blockchain' {
	/**
	 * @file Blurt Blockchain helpers.
	 * @author BeBlurt <https://beblurt.com/@beblurt>
	 * @description adaptation from Johan Nordberg <code@johan-nordberg.com> Blockchain helpers
	 * @license
	 * Copyright (c) 2017 Johan Nordberg. All Rights Reserved.
	 *
	 * Redistribution and use in source and binary forms, with or without modification,
	 * are permitted provided that the following conditions are met:
	 *
	 *  1. Redistribution of source code must retain the above copyright notice, this
	 *     list of conditions and the following disclaimer.
	 *
	 *  2. Redistribution in binary form must reproduce the above copyright notice,
	 *     this list of conditions and the following disclaimer in the documentation
	 *     and/or other materials provided with the distribution.
	 *
	 *  3. Neither the name of the copyright holder nor the names of its contributors
	 *     may be used to endorse or promote products derived from this software without
	 *     specific prior written permission.
	 *
	 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
	 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
	 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
	 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
	 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
	 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
	 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
	 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
	 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
	 * OF THE POSSIBILITY OF SUCH DAMAGE.
	 *
	 * You acknowledge that this software is not designed, licensed or intended for use
	 * in the design, construction, operation or maintenance of any military facility.
	 */
	/// <reference types="node" />
	import { BlockHeader, SignedBlock } from 'dblurt/chain/block';
	import { AppliedOperation } from 'dblurt/chain/operation';
	import { Client } from 'dblurt/client';
	export enum BlockchainMode {
	    /** Only get irreversible blocks. */
	    Irreversible = 0,
	    /** Get all blocks. */
	    Latest = 1
	}
	export interface BlockchainStreamOptions {
	    /** Start block number, inclusive. If omitted generation will start from current block height. */
	    from?: number;
	    /** End block number, inclusive. If omitted stream will continue indefinitely. */
	    to?: number;
	    /**
	     * Streaming mode, if set to `Latest` may include blocks that are not applied to the final chain.
	     * Defaults to `Irreversible`.
	     */
	    mode?: BlockchainMode;
	}
	export class Blockchain {
	    readonly client: Client;
	    constructor(client: Client);
	    /** Get latest block number. */
	    getCurrentBlockNum(mode?: BlockchainMode): Promise<number>;
	    /** Get latest block header. */
	    getCurrentBlockHeader(mode?: BlockchainMode): Promise<BlockHeader>;
	    /** Get latest block. */
	    getCurrentBlock(mode?: BlockchainMode): Promise<SignedBlock>;
	    /**
	     * Return a asynchronous block number iterator.
	     * @param options Feed options, can also be a block number to start from.
	     */
	    getBlockNumbers(options?: BlockchainStreamOptions | number): AsyncGenerator<number, any, unknown>;
	    /** Return a stream of block numbers, accepts same parameters as {@link getBlockNumbers}. */
	    getBlockNumberStream(options?: BlockchainStreamOptions | number): NodeJS.ReadableStream;
	    /** Return a asynchronous block iterator, accepts same parameters as {@link getBlockNumbers}. */
	    getBlocks(options?: BlockchainStreamOptions | number): AsyncGenerator<SignedBlock, any, unknown>;
	    /** Return a stream of blocks, accepts same parameters as {@link getBlockNumbers}. */
	    getBlockStream(options?: BlockchainStreamOptions | number): NodeJS.ReadableStream;
	    /** Return a asynchronous operation iterator, accepts same parameters as {@link getBlockNumbers}. */
	    getOperations(options?: BlockchainStreamOptions | number): AsyncGenerator<AppliedOperation, any, unknown>;
	    /** Return a stream of operations, accepts same parameters as {@link getBlockNumbers}. */
	    getOperationsStream(options?: BlockchainStreamOptions | number): NodeJS.ReadableStream;
	}

}
declare module 'dblurt/chain/nexus' {
	/**
	 * @file Blurt type definitions related to Nexus.
	 * @author BeBlurt <https://beblurt.com/@beblurt>
	 * @description adaptation from Johan Nordberg <code@johan-nordberg.com> type definitions related to comments and posting.
	 * @license
	 * Copyright (c) 2017 Johan Nordberg. All Rights Reserved.
	 *
	 * Redistribution and use in source and binary forms, with or without modification,
	 * are permitted provided that the following conditions are met:
	 *
	 *  1. Redistribution of source code must retain the above copyright notice, this
	 *     list of conditions and the following disclaimer.
	 *
	 *  2. Redistribution in binary form must reproduce the above copyright notice,
	 *     this list of conditions and the following disclaimer in the documentation
	 *     and/or other materials provided with the distribution.
	 *
	 *  3. Neither the name of the copyright holder nor the names of its contributors
	 *     may be used to endorse or promote products derived from this software without
	 *     specific prior written permission.
	 *
	 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
	 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
	 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
	 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
	 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
	 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
	 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
	 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
	 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
	 * OF THE POSSIBILITY OF SUCH DAMAGE.
	 *
	 * You acknowledge that this software is not designed, licensed or intended for use
	 * in the design, construction, operation or maintenance of any military facility.
	 */
	import { Asset } from 'dblurt/chain/asset';
	import { BeneficiaryRoute } from 'dblurt/chain/comment';
	/** Community Identifier */
	export const communityAccountRegexp: RegExp;
	export const isValidCommunityAccountRegexp: (communityAccount: string) => boolean;
	export type NexusTypeNotification = 'new_community' | 'set_role' | 'set_props' | 'set_label' | 'mute_post' | 'unmute_post' | 'pin_post' | 'unpin_post' | 'flag_post' | 'error' | 'subscribe' | 'reply' | 'reblog' | 'follow' | 'mention' | 'vote';
	export interface NexusAccountNotifications {
	    id: number;
	    type: NexusTypeNotification;
	    score: number;
	    date: string;
	    msg: string;
	    url: string;
	}
	export interface NexusPost {
	    post_id: number;
	    author: string;
	    permlink: string;
	    category: string;
	    title: string;
	    body: string;
	    json_metadata: {
	        tags?: string[];
	        users?: string[];
	        image?: string[];
	        links?: string[];
	        app?: string;
	        format?: string;
	        description?: string;
	        community?: string;
	        flow?: {
	            tags: string[];
	            pictures: {
	                id: number;
	                name: string;
	                mime: string;
	                url: string;
	                tags: string[];
	                caption: string;
	            }[];
	        };
	        type?: string | '3speak/video';
	        video: {
	            info: {
	                platform: string | '3speak';
	                title: string;
	                author: string;
	                permlink: string;
	                duration: number;
	                filesize: number;
	                file: string;
	                lang: string;
	                firstUpload: boolean;
	                ipfs: string | null;
	                ipfsThumbnail: string | null;
	                video_v2: string | null;
	                sourceMap: {
	                    type: string | 'video';
	                    url: string;
	                    format: string | 'm3u8';
	                }[];
	            };
	            content: {
	                description: string;
	                tags: string[];
	            };
	        };
	    };
	    created: string;
	    updated: string;
	    depth: number;
	    children: number;
	    net_rshares: number | string;
	    is_paidout: boolean;
	    payout_at: string;
	    payout: number;
	    pending_payout_value: Asset | string;
	    author_payout_value: Asset | string;
	    curator_payout_value: Asset | string;
	    promoted: Asset | string;
	    replies: [];
	    active_votes: {
	        voter: string;
	        rshares: number | string;
	    }[];
	    stats: {
	        hide: boolean;
	        gray: boolean;
	        total_votes: number;
	        flag_weight: number;
	        is_pinned?: boolean;
	        is_muted?: boolean;
	    };
	    beneficiaries: BeneficiaryRoute[];
	    max_accepted_payout: Asset | string;
	    percent_blurt: number;
	    parent_author?: string;
	    parent_permlink?: string;
	    url: string;
	    blacklists: [];
	    reblogged_by?: string[];
	    community?: string;
	    community_title?: string;
	    author_role?: string;
	    author_title?: string;
	}
	export interface NexusCommunityContext {
	    role: string;
	    subscribed: boolean;
	    title: string;
	}
	export interface NexusProfile {
	    id: number;
	    name: string;
	    created: string;
	    active: string;
	    post_count: number;
	    blacklists: [];
	    stats: {
	        sp: number;
	        rank: number;
	        following: number;
	        followers: number;
	    };
	    metadata: {
	        profile: {
	            name: string;
	            about: string;
	            website: string;
	            location: string;
	            cover_image: string;
	            profile_image: string;
	        };
	    };
	    context?: {
	        followed: boolean;
	    };
	}
	export interface NexusCommunity {
	    id: number;
	    name: string;
	    title: string;
	    about: string;
	    lang: string;
	    type_id: number;
	    is_nsfw: boolean;
	    subscribers: number;
	    sum_pending: number;
	    num_pending: number;
	    num_authors: number;
	    created_at: string;
	    avatar_url: string;
	    cover_url?: string;
	    context: {
	        subscribed: boolean;
	        role: string;
	        title: string;
	    };
	    admins: string[];
	}
	export interface NexusCommunityExtended extends Omit<NexusCommunity, 'admins'> {
	    description: string;
	    flag_text: string;
	    settings: {
	        avatar_url: string;
	        cover_url?: string;
	        default_view: 'list' | 'blog' | 'grid';
	        comment_display: 'votes' | 'trending' | 'age' | 'forum';
	    };
	    team: [string, string, string | ''][];
	}
	export interface NexusPayoutStats {
	    items: [string, // account (community or user)
	    string, // title
	    number, // payout
	    number, // number of posts
	    // number of posts
	    number | null][];
	    total: number;
	    blogs: number;
	}
	/** Possible sort for `get_ranked_posts`. */
	export type NexusSortGetRankedPost = 'trending' | 'hot' | 'created' | 'promoted' | 'payout' | 'payout_comments' | 'muted';
	export interface NexusUpdateProps {
	    community: string;
	    props: {
	        /** 32 chars maximum */
	        title: string;
	        /** 120 chars maximum */
	        about: string;
	        /** ISO 639-1 (en, fr, de) */
	        lang: string;
	        /** true if this community is 18+. UI to automatically tag all posts/comments nsfw */
	        is_nsfw: boolean;
	        /** a blob of markdown to describe purpose, enumerate rules, etc. (5000 chars) */
	        description: string;
	        /** custom text for reporting content */
	        flag_text: string;
	        settings: {
	            /** same format as account avatars; usually rendered as a circle */
	            avatar_url: string;
	            /** same format as account covers; used as header background image */
	            cover_url: string;
	            default_view: 'list' | 'blog' | 'grid';
	        };
	    };
	}
	export interface NexusUpdatePropsOperation {
	    0: 'updateProps';
	    1: NexusUpdateProps;
	}
	export interface NexusSetRoleOperation {
	    0: 'setRole';
	    1: {
	        community: string;
	        account: string;
	        role: 'muted' | 'guest' | 'member' | 'mod' | 'admin' | 'owner';
	    };
	}
	export interface NexusSetUserTitleOperation {
	    0: 'setUserTitle';
	    1: {
	        community: string;
	        account: string;
	        title: string;
	    };
	}
	export interface NexusMutePostOperation {
	    0: 'mutePost';
	    1: {
	        community: string;
	        account: string;
	        permlink: string;
	        notes: string;
	    };
	}
	export interface NexusUnMutePostOperation {
	    0: 'unmutePost';
	    1: {
	        community: string;
	        account: string;
	        permlink: string;
	        notes: string;
	    };
	}
	export interface NexusPinPostOperation {
	    0: 'pinPost';
	    1: {
	        community: string;
	        account: string;
	        permlink: string;
	    };
	}
	export interface NexusUnPinPostOperation {
	    0: 'unpinPost';
	    1: {
	        community: string;
	        account: string;
	        permlink: string;
	    };
	}
	export interface NexusFlagPostOperation {
	    0: 'flagPost';
	    1: {
	        community: string;
	        account: string;
	        permlink: string;
	        notes: string;
	    };
	}

}
declare module 'dblurt/helpers/broadcast' {
	/**
	 * @file Blurt Broadcast API helpers.
	 * @author BeBlurt <https://beblurt.com/@beblurt>
	 * @description adaptation from Johan Nordberg <code@johan-nordberg.com> Broadcast API helpers.
	 * @license
	 * Copyright (c) 2017 Johan Nordberg. All Rights Reserved.
	 *
	 * Redistribution and use in source and binary forms, with or without modification,
	 * are permitted provided that the following conditions are met:
	 *
	 *  1. Redistribution of source code must retain the above copyright notice, this
	 *     list of conditions and the following disclaimer.
	 *
	 *  2. Redistribution in binary form must reproduce the above copyright notice,
	 *     this list of conditions and the following disclaimer in the documentation
	 *     and/or other materials provided with the distribution.
	 *
	 *  3. Neither the name of the copyright holder nor the names of its contributors
	 *     may be used to endorse or promote products derived from this software without
	 *     specific prior written permission.
	 *
	 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
	 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
	 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
	 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
	 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
	 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
	 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
	 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
	 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
	 * OF THE POSSIBILITY OF SUCH DAMAGE.
	 *
	 * You acknowledge that this software is not designed, licensed or intended for use
	 * in the design, construction, operation or maintenance of any military facility.
	 */
	import { AccountCreateOperation, AccountUpdateOperation, AccountWitnessVoteOperation, CancelTransferFromSavingsOperation, ChangeRecoveryAccountOperation, ClaimAccountOperation, ClaimRewardBalanceOperation, CommentOperation, CommentOptionsOperation, CreateClaimedAccountOperation, CustomJsonOperation, DelegateVestingSharesOperation, Operation, TransferOperation, VoteOperation } from 'dblurt/chain/operation';
	import { NexusUpdateProps } from 'dblurt/chain/nexus';
	import { SignedTransaction, Transaction, TransactionConfirmation } from 'dblurt/chain/transaction';
	import { Client } from 'dblurt/client';
	import { PrivateKey } from 'dblurt/crypto';
	export class BroadcastAPI {
	    readonly client: Client;
	    /**
	     * How many milliseconds in the future to set the expiry time to when
	     * broadcasting a transaction, defaults to 1 minute.
	     */
	    expireTime: number;
	    constructor(client: Client);
	    /** Convenience for calling `condenser_api`. */
	    call(method: string, params?: any[]): Promise<any>;
	    /**
	     * Create account.
	     * Broadcasted to the blockchain to create a new account.
	     * @param data The account_create payload. See {@link AccountCreateOperation}
	     * @param key The Active or Owner private key of the account creator.
	     */
	    accountCreate(data: AccountCreateOperation[1], key: PrivateKey): Promise<TransactionConfirmation>;
	    /**
	     * Update account.
	     * Updates account information.
	     * @param data The account_update payload. See {@link AccountUpdateOperation}
	     * @param key The Active or Owner private key of the account.
	     */
	    accountUpdate(data: AccountUpdateOperation[1], key: PrivateKey): Promise<TransactionConfirmation>;
	    /**
	     * Witness vote.
	     * Vote from an account to a witness.
	     * @param data The account_witness_vote payload. See {@link AccountWitnessVoteOperation}
	     * @param key The private key of the account, should be the Active key at least.
	     * @remarks Since HF 0.8 the formula VS/N where VP is the Vesting Share of the account and N the number of witnesses upvoted by the account is applied.
	     */
	    accountWitnessVote(data: AccountWitnessVoteOperation[1], key: PrivateKey): Promise<TransactionConfirmation>;
	    /**
	     * Cancel transfer from saving.
	     * Funds withdrawals from the savings can be canceled at any time before it is executed.
	     * @param data The cancel_transfer_from_savings payload. See {@link CancelTransferFromSavingsOperation}
	     * @param key The private key of the account, should be the Active key at least.
	     */
	    cancelTransferFromSavings(data: CancelTransferFromSavingsOperation[1], key: PrivateKey): Promise<TransactionConfirmation>;
	    /**
	     * Change recovery account.
	     * Funds withdrawals from the savings can be canceled at any time before it is executed.
	     * @param data The change_recovery_account payload. See {@link ChangeRecoveryAccountOperation}
	     * @param key The Owner private key of the account.
	     */
	    changeRecoveryAccount(data: ChangeRecoveryAccountOperation[1], key: PrivateKey): Promise<TransactionConfirmation>;
	    /**
	     * Claim account.
	     * Requests from users or dApps to get an account creation ticket in order to create new accounts.
	     * These operations are issued before the actual creation of the account on the blockchain.
	     * @param data The claim_account payload. See {@link ClaimAccountOperation}
	     * @param key The Active or Owner private key of the account.
	     * @remarks Deactivated Since HF 0.2
	     */
	    claimAccount(data: ClaimAccountOperation[1], key: PrivateKey): Promise<TransactionConfirmation>;
	    /**
	     * Claim reward balance.
	     * Author and curator rewards are not automatically transferred to the account’s balances.
	     * One has to issue a `claim_reward_balance` operation to trigger the transfer from the reward pool to its balance.
	     * @param data The claim_reward_balance payload. See {@link ClaimRewardBalanceOperation}
	     * @param key The Posting, Active or Owner private key of the account.
	     */
	    claimRewardBalance(data: ClaimRewardBalanceOperation[1], key: PrivateKey): Promise<TransactionConfirmation>;
	    /**
	     * Comment.
	     * Creates a post/comment.
	     * @param data The comment payload. See {@link CommentOperation}
	     * @param key The Posting, Active or Owner private key of the account.
	     * @param options The comment_options payload [optional]. See {@link CommentOptionsOperation}
	     * @remarks Rules:
	     * - The “title” must not be longer than 256 bytes
	     * - The “title” must be UTF-8
	     * - The “body” must be larger than 0 bytes
	     * - The “body” much also be UTF-8
	     * @remarks json_metadata: There is no blockchain enforced validation on `json_metadata`, but the community has adopted a particular structure:
	     * - tags - An array of up to 5 strings. Although the blockchain will accept more than 5, the tags plugin only looks at the first five
	     * - app - A user agent style application identifier. Typically app_name/version, e.g. beblurt/0.1
	     * - format - The format of the body, e.g. markdown
	     * @remarks In addition to the above keys, application developers are free to add any other keys they want to help manage the content they broadcast.
	     * @remarks When a comment is first broadcast, the permlink must be unique for the author. Otherwise, it is interpreted as an update operation.
	     *  Updating will either replace the entire body with the latest operation or patch the body if using {@link [diff-match-patch](https://github.com/google/diff-match-patch)}.
	     */
	    comment(data: CommentOperation[1], key: PrivateKey, options?: CommentOptionsOperation[1]): Promise<TransactionConfirmation>;
	    /**
	     * Comment options.
	     * Authors of posts may not want all of the benefits that come from creating a post. This operation allows authors to update properties associated with their post.
	     * Typically, these options will accompany a comment operation in the same transaction.
	     * @param data The comment_options payload. See {@link CommentOptionsOperation}
	     * @param key The Active or Owner private key of the account creator.
	     */
	    commentOptions(data: CommentOptionsOperation[1], key: PrivateKey): Promise<TransactionConfirmation>;
	    /**
	     * Create claimed account.
	     * When used with `claim_account`, works identically to `account_create` See {@link accountCreate}.
	     * @param data The create_claimed_account payload. See {@link CreateClaimedAccountOperation}
	     * @param key The Active or Owner private key of the account creator.
	     * @remarks Deactivated Since HF 0.2
	     */
	    createClaimedAccount(data: CreateClaimedAccountOperation[1], key: PrivateKey): Promise<TransactionConfirmation>;
	    /**
	     * Broadcast custom JSON.
	     * Serves the same purpose as custom but also supports required posting authorities. Unlike custom, this operation is designed to be human readable/developer friendly.
	     * @param data The custom_json operation payload. See {@link CustomJsonOperation}
	     * @param key The Posting, Active or Owner private key of the account.
	     */
	    customJson(data: CustomJsonOperation[1], key: PrivateKey): Promise<TransactionConfirmation>;
	    /**
	     * Delegate vesting shares from one account to the other. The vesting shares are still owned
	     * by the original account, but content voting rights and bandwidth allocation are transferred
	     * to the receiving account. This sets the delegation to `vesting_shares`, increasing it or
	     * decreasing it as needed. (i.e. a delegation of 0 removes the delegation)
	     *
	     * When a delegation is removed the shares are placed in limbo for a week to prevent a satoshi
	     * of VESTS from voting on the same content twice.
	     *
	     * @param options Delegation options. See {@link DelegateVestingSharesOperation}
	     * @param key Private active key of the delegator.
	     */
	    delegateVestingShares(options: DelegateVestingSharesOperation[1], key: PrivateKey): Promise<TransactionConfirmation>;
	    /**
	     * Prepare transaction with operations for Sign and broadcast to the network.
	     * @param operations List of operations to send.
	     */
	    prepareTransaction(operations: Operation[]): Promise<Transaction>;
	    /**
	     * Reblurt/Undo Reblurt a post
	     * @param account The account submitting the custom_json operation.
	     * @param author The author of the post.
	     * @param permlink The permlink of the post.
	     * @param undo if true Undo Reblurt else Reblurt
	     * @param key The Posting, Active or Owner private key of the account.
	     **/
	    reblurt(account: string, author: string, permlink: string, undo: boolean | undefined, key: PrivateKey): Promise<TransactionConfirmation>;
	    /** Broadcast a read notification to the network. */
	    readNotification(account: string, date: string, key: PrivateKey): Promise<TransactionConfirmation>;
	    /** Broadcast a signed transaction to the network. */
	    send(transaction: SignedTransaction): Promise<TransactionConfirmation>;
	    /**
	     * Sign and broadcast transaction with operations to the network. Throws if the transaction expires.
	     * @param operations List of operations to send.
	     * @param key Private key(s) used to sign transaction.
	     */
	    sendOperations(operations: Operation[], key: PrivateKey | PrivateKey[]): Promise<TransactionConfirmation>;
	    /** Sign a transaction with key(s) */
	    sign(transaction: Transaction, key: PrivateKey | PrivateKey[]): SignedTransaction;
	    /**
	     * Broadcast a transfer.
	     * @param data The transfer operation payload.
	     * @param key Private active key of sender.
	     */
	    transfer(data: TransferOperation[1], key: PrivateKey): Promise<TransactionConfirmation>;
	    /**
	     * Broadcast a vote.
	     * @param vote The vote to send.
	     * @param key Private posting key of the voter.
	     */
	    vote(vote: VoteOperation[1], key: PrivateKey): Promise<TransactionConfirmation>;
	    /**
	     * Mute a post (Mods or higher). Can be a topic or a comment.
	     * @param community The community account concerned.
	     * @param authority The account submitting the custom_json operation.
	     * @param account The author of the post.
	     * @param permlink The permlink of the post.
	     * @param notes short notes
	     * @param key The Posting, Active or Owner private key of the account.
	     */
	    nexusMutePost(community: string, authority: string, account: string, permlink: string, notes: string, key: PrivateKey): Promise<TransactionConfirmation>;
	    /**
	     * Stickies a post to the top of the community homepage (Mods or higher). If multiple posts are stickied, the newest ones are shown first.
	     * @param community The community account concerned.
	     * @param authority The account submitting the custom_json operation.
	     * @param account The author of the post.
	     * @param permlink The permlink of the post.
	     * @param key The Posting, Active or Owner private key of the account.
	     */
	    nexusPinPost(community: string, authority: string, account: string, permlink: string, key: PrivateKey): Promise<TransactionConfirmation>;
	    /**
	     * Stickies a post to the top of the community homepage (Mods or higher). If multiple posts are stickied, the newest ones are shown first.
	     * @param community The community account concerned.
	     * @param authority The account submitting the custom_json operation.
	     * @param account The account submitting the custom_json operation.
	     * @param role The author of the post.
	     * @param key The Posting, Active or Owner private key of the account.
	     */
	    nexusSetRole(community: string, authority: string, account: string, role: 'admin' | 'mod' | 'member' | 'guest' | 'muted', key: PrivateKey): Promise<TransactionConfirmation>;
	    /**
	     * Stickies a post to the top of the community homepage (Mods or higher). If multiple posts are stickied, the newest ones are shown first.
	     * @param community The community account concerned.
	     * @param authority The account submitting the custom_json operation.
	     * @param account The account submitting the custom_json operation.
	     * @param title title for the account
	     * @param key The Posting, Active or Owner private key of the account.
	     */
	    nexusSetUserTitle(community: string, authority: string, account: string, title: string, key: PrivateKey): Promise<TransactionConfirmation>;
	    /**
	     * Unmute a post (Mods or higher).
	     * @param community The community account concerned.
	     * @param authority The account submitting the custom_json operation.
	     * @param account The author of the post.
	     * @param permlink The permlink of the post.
	     * @param notes short notes
	     * @param key The Posting, Active or Owner private key of the account.
	     */
	    nexusUnmutePost(community: string, authority: string, account: string, permlink: string, notes: string, key: PrivateKey): Promise<TransactionConfirmation>;
	    /**
	     * Removes a post to the top of the community homepage (Mods or higher).
	     * @param community The community account concerned.
	     * @param authority The account submitting the custom_json operation.
	     * @param account The author of the post.
	     * @param permlink The permlink of the post.
	     * @param key The Posting, Active or Owner private key of the account.
	     */
	    nexusUnpinPost(community: string, authority: string, account: string, permlink: string, key: PrivateKey): Promise<TransactionConfirmation>;
	    /**
	     * Broadcast Nexus update properties (Admin Operations).
	     * Update display settings of a Community.
	     * @param data The custom_json operation payload. See {@link NexusUpdateProps}
	     * @param authority The account submitting the custom_json operation.
	     * @param key The Posting, Active or Owner private key of the account.
	     */
	    nexusUpdateProps(data: NexusUpdateProps, authority: string, key: PrivateKey): Promise<TransactionConfirmation>;
	    /**
	     * Used by guests to suggest a post for the review queue. It’s up to the community to define what constitutes flagging.
	     * @param community The community account concerned.
	     * @param authority The account submitting the custom_json operation.
	     * @param author The author of the post.
	     * @param permlink The permlink of the post.
	     * @param notes short notes
	     * @param key The Posting, Active or Owner private key of the account.
	     */
	    nexusFlagPost(community: string, authority: string, account: string, permlink: string, notes: string, key: PrivateKey): Promise<TransactionConfirmation>;
	    /**
	     * Un/subscribe to a community (Guest Operations)
	     * @param community The community account concerned.
	     * @param action Un/subscribe to a community.
	     * @param account The account submitting the custom_json operation.
	     * @param key The Posting, Active or Owner private key of the account.
	     **/
	    nexusSubscription(community: string, action: 'subscribe' | 'unsubscribe', account: string, key: PrivateKey): Promise<TransactionConfirmation>;
	}

}
declare module 'dblurt/chain/blog' {
	/**
	 * @file Blurt type definitions related to Blog.
	 * @author BeBlurt <https://beblurt.com/@beblurt>
	 * @description adaptation from Johan Nordberg <code@johan-nordberg.com> type definitions related to comments and posting.
	 * @license
	 * Copyright (c) 2017 Johan Nordberg. All Rights Reserved.
	 *
	 * Redistribution and use in source and binary forms, with or without modification,
	 * are permitted provided that the following conditions are met:
	 *
	 *  1. Redistribution of source code must retain the above copyright notice, this
	 *     list of conditions and the following disclaimer.
	 *
	 *  2. Redistribution in binary form must reproduce the above copyright notice,
	 *     this list of conditions and the following disclaimer in the documentation
	 *     and/or other materials provided with the distribution.
	 *
	 *  3. Neither the name of the copyright holder nor the names of its contributors
	 *     may be used to endorse or promote products derived from this software without
	 *     specific prior written permission.
	 *
	 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
	 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
	 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
	 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
	 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
	 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
	 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
	 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
	 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
	 * OF THE POSSIBILITY OF SUCH DAMAGE.
	 *
	 * You acknowledge that this software is not designed, licensed or intended for use
	 * in the design, construction, operation or maintenance of any military facility.
	 */
	export interface BlogEntry {
	    blog: string;
	    entry_id: number;
	    author: string;
	    permlink: string;
	    reblogged_on: string;
	}

}
declare module 'dblurt/chain/proposal' {
	/**
	 * @file Blurt type definitions related to Proposals.
	 * @author BeBlurt <https://beblurt.com/@beblurt>
	 * @description adaptation from Johan Nordberg <code@johan-nordberg.com> type definitions related to comments and posting.
	 * @license
	 * Copyright (c) 2017 Johan Nordberg. All Rights Reserved.
	 *
	 * Redistribution and use in source and binary forms, with or without modification,
	 * are permitted provided that the following conditions are met:
	 *
	 *  1. Redistribution of source code must retain the above copyright notice, this
	 *     list of conditions and the following disclaimer.
	 *
	 *  2. Redistribution in binary form must reproduce the above copyright notice,
	 *     this list of conditions and the following disclaimer in the documentation
	 *     and/or other materials provided with the distribution.
	 *
	 *  3. Neither the name of the copyright holder nor the names of its contributors
	 *     may be used to endorse or promote products derived from this software without
	 *     specific prior written permission.
	 *
	 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
	 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
	 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
	 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
	 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
	 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
	 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
	 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
	 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
	 * OF THE POSSIBILITY OF SUCH DAMAGE.
	 *
	 * You acknowledge that this software is not designed, licensed or intended for use
	 * in the design, construction, operation or maintenance of any military facility.
	 */
	import { Asset } from 'dblurt/chain/asset';
	export interface Proposal {
	    id: number;
	    proposal_id: number;
	    creator: string;
	    receiver: string;
	    start_date: string;
	    end_date: string;
	    daily_pay: Asset | string;
	    subject: string;
	    permlink: string;
	    total_votes: number | string;
	}
	export interface ProposalExtended extends Omit<Proposal, 'daily_pay'> {
	    daily_pay: {
	        amount: number | string;
	        precision: number;
	        nai: string;
	    };
	    status: 'active' | 'inactive' | 'expired';
	}
	export interface ProposalVote {
	    id: number;
	    voter: string;
	    proposal: ProposalExtended;
	}

}
declare module 'dblurt/chain/witness' {
	/**
	 * @file Blurt type definitions related to witness.
	 * @author BeBlurt <https://beblurt.com/@beblurt>
	 * @description adaptation from Johan Nordberg <code@johan-nordberg.com> type definitions related to comments and posting.
	 * @license
	 * Copyright (c) 2017 Johan Nordberg. All Rights Reserved.
	 *
	 * Redistribution and use in source and binary forms, with or without modification,
	 * are permitted provided that the following conditions are met:
	 *
	 *  1. Redistribution of source code must retain the above copyright notice, this
	 *     list of conditions and the following disclaimer.
	 *
	 *  2. Redistribution in binary form must reproduce the above copyright notice,
	 *     this list of conditions and the following disclaimer in the documentation
	 *     and/or other materials provided with the distribution.
	 *
	 *  3. Neither the name of the copyright holder nor the names of its contributors
	 *     may be used to endorse or promote products derived from this software without
	 *     specific prior written permission.
	 *
	 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
	 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
	 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
	 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
	 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
	 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
	 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
	 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
	 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
	 * OF THE POSSIBILITY OF SUCH DAMAGE.
	 *
	 * You acknowledge that this software is not designed, licensed or intended for use
	 * in the design, construction, operation or maintenance of any military facility.
	 */
	import { Asset } from 'dblurt/chain/asset';
	export interface Witness {
	    _id: number;
	    owner: string;
	    created: string;
	    url: string | '';
	    votes: string;
	    virtual_last_update: string;
	    virtual_position: string;
	    virtual_scheduled_time: string;
	    total_missed: number;
	    last_aslot: number;
	    last_confirmed_block_num: number;
	    signing_key: string;
	    props: {
	        account_creation_fee: Asset | string;
	        maximum_block_size: number;
	        account_subsidy_budget: number;
	        account_subsidy_decay: number;
	        operation_flat_fee: Asset | string;
	        bandwidth_kbytes_fee: Asset | string;
	        proposal_fee: Asset | string;
	    };
	    running_version: string;
	    hardfork_version_vote: string;
	    hardfork_time_vote: string;
	    available_witness_account_subsidies: number;
	}

}
declare module 'dblurt/helpers/condenser' {
	/**
	 * @file Blurt Condenser API helpers.
	 * @author BeBlurt <https://beblurt.com/@beblurt>
	 * @description adaptation from Johan Nordberg <code@johan-nordberg.com> Database API helpers.
	 * @license
	 * Copyright (c) 2022 BeBlurt. All Rights Reserved.
	 *
	 * Redistribution and use in source and binary forms, with or without modification,
	 * are permitted provided that the following conditions are met:
	 *
	 *  1. Redistribution of source code must retain the above copyright notice, this
	 *     list of conditions and the following disclaimer.
	 *
	 *  2. Redistribution in binary form must reproduce the above copyright notice,
	 *     this list of conditions and the following disclaimer in the documentation
	 *     and/or other materials provided with the distribution.
	 *
	 *  3. Neither the name of the copyright holder nor the names of its contributors
	 *     may be used to endorse or promote products derived from this software without
	 *     specific prior written permission.
	 *
	 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
	 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
	 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
	 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
	 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
	 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
	 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
	 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
	 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
	 * OF THE POSSIBILITY OF SUCH DAMAGE.
	 *
	 * You acknowledge that this software is not designed, licensed or intended for use
	 * in the design, construction, operation or maintenance of any military facility.
	 */
	import { ExtendedAccount } from 'dblurt/chain/account';
	import { BlockHeader, SignedBlock } from 'dblurt/chain/block';
	import { BlogEntry } from 'dblurt/chain/blog';
	import { Post, Discussion } from 'dblurt/chain/comment';
	import { DynamicGlobalProperties, ChainProperties, RpcNodeConfig, VestingDelegation, RewardFund } from 'dblurt/chain/misc';
	import { AppliedOperation } from 'dblurt/chain/operation';
	import { Proposal, ProposalVote } from 'dblurt/chain/proposal';
	import { SignedTransaction } from 'dblurt/chain/transaction';
	import { Witness } from 'dblurt/chain/witness';
	import { Client } from 'dblurt/client';
	/** Possible categories for `get_discussions_by_*`. */
	export type DiscussionQueryCategory = 'active' | 'blog' | 'cashout' | 'children' | 'comments' | 'feed' | 'hot' | 'promoted' | 'trending' | 'votes' | 'created';
	export interface DisqussionQuery {
	    /** Name of author or tag to fetch. */
	    tag?: string;
	    /** Number of results, max 100. */
	    limit: number;
	    filter_tags?: string[];
	    select_authors?: string[];
	    select_tags?: string[];
	    /** Number of bytes of post body to fetch, default 0 (all) */
	    truncate_body?: number;
	    /**
	     * Name of author to start from, used for paging.
	     * Should be used in conjunction with `start_permlink`.
	     */
	    start_author?: string;
	    /**
	     * Permalink of post to start from, used for paging.
	     * Should be used in conjunction with `start_author`.
	     */
	    start_permlink?: string;
	    parent_author?: string;
	    parent_permlink?: string;
	}
	export interface FollowCount {
	    account: string;
	    follower_count: number;
	    following_count: number;
	}
	export interface Followers {
	    follower: string;
	    following: string;
	    what: ['blog' | 'ignore' | null];
	}
	export interface Following {
	    follower: string;
	    following: string;
	    what: ['blog' | 'ignore' | null];
	}
	export class CondenserAPI {
	    readonly client: Client;
	    constructor(client: Client);
	    /** Convenience for calling `condenser_api`. */
	    call(method: string, params?: any[] | {
	        [key: string]: any;
	    }): Promise<any>;
	    /**
	     * Returns one or more account history objects for account operations
	     *
	     * @param account The account to fetch
	     * @param from The starting index
	     * @param limit The maximum number of results to return
	     * @param operations_bitmask Generated by utils.makeBitMaskFilter() - see example below (not yet usable)
	     * @example
	     * ```typescript
	     * import { utils } from "@beblurt/dblurt"
	     * const op = dblurt.utils.operationOrders
	     * const operationsBitmask = dblurt.utils.makeBitMaskFilter([
	     *   op.vote,
	     *   op.comment,
	     *   op.delete_comment,
	     *   op.comment_options,
	     *   op.claim_reward_balance,
	     *   op.author_reward,
	     *   op.curation_reward,
	     *   op.comment_reward,
	     *   op.producer_reward,
	     * ])
	     * const accountHistory = await client.condenser.getAccountHistory('beblurt', -1, 10, operationsBitmask)
	     * ```
	     */
	    getAccountHistory(account: string, from: number, limit: number, operation_bitmask?: (number | null)[]): Promise<[[number, AppliedOperation]]>;
	    /** Return array of account info objects for the usernames passed. @param usernames The accounts to fetch. */
	    getAccounts(usernames: string[]): Promise<ExtendedAccount[]>;
	    /** Returns all votes for the given post. */
	    getActiveVotes(author: string, permlink: string): Promise<Post['active_votes']>;
	    /** Returns the list of active witnesses */
	    getActiveWitnesses(): Promise<Witness[]>;
	    /** Return block *blockNum*. */
	    getBlock(blockNum: number): Promise<SignedBlock>;
	    /** Return header for *blockNum*. */
	    getBlockHeader(blockNum: number): Promise<BlockHeader>;
	    /** Returns a list of blog entries for an account. */
	    getBlogEntries(account: string, start_entry_id: number, limit: number): Promise<BlogEntry[]>;
	    /** Return median chain properties decided by witness. */
	    getChainProperties(): Promise<ChainProperties>;
	    /** Return server config. See:
	     * https://gitlab.com/blurt/blurt/-/blob/master/libraries/protocol/include/blurt/protocol/config.hpp
	     */
	    getConfig(): Promise<RpcNodeConfig>;
	    /** Returns a content (post or comment). */
	    getContent(author: string, permlink: string): Promise<Post>;
	    /** Returns a list of replies. */
	    getContentReplies(author: string, permlink: string): Promise<Post[]>;
	    /** Return Dynamic Global Properties. */
	    getDynamicGlobalProperties(): Promise<DynamicGlobalProperties>;
	    /**
	     * Return array of discussions (a.k.a. posts).
	     * @param by The type of sorting for the discussions, valid options are:
	     *           `active` `blog` `cashout` `children` `comments` `created`
	     *           `feed` `hot` `promoted` `trending` `votes`. Note that
	     *           for `blog` and `feed` the tag is set to a username.
	     */
	    getDiscussions(by: DiscussionQueryCategory, query: DisqussionQuery): Promise<Discussion[]>;
	    /** return the count of followers/following for an account */
	    getFollowCount(accounts: string[]): Promise<FollowCount>;
	    /** return the list of the followers of an account */
	    getFollowers(account: string, start: string | null, type: 'blog' | 'ignore' | null, limit: number): Promise<Followers[]>;
	    /** return the list of accounts that are following an account */
	    getFollowing(account: string, start: string | null, type: 'blog' | 'ignore' | null, limit: number): Promise<Following[]>;
	    /** Returns the list of accounts that are reblogged a content (post). */
	    getRebloggedBy(author: string, permlink: string): Promise<string[]>;
	    /** Return all applied operations in *blockNum*. */
	    getOperations(blockNum: number, onlyVirtual?: boolean): Promise<AppliedOperation[]>;
	    /**
	     * Returns an array of proposals filtered by the specified parameters.
	     * @param start - Depends on order (see below)
	     * - creator - creator of the proposal (account name string)
	     * - start_date - start date of the proposal (date string)
	     * - end_date - end date of the proposal (date string)
	     * - total_votes - total votes of the proposal (int)
	     * @param limit - The maximum number of proposals to return (max 1000).
	     * @param order - can be one of:
	     * - by_creator - order by proposal creator
	     * - by_start_date - order by proposal start date
	     * - by_end_date - order by proposal end date
	     * - by_total_votes - order by proposal total votes
	     * @param order_direction - The direction in which to order the results. Can be ascending or descending
	     * @param status - The status of proposals to return.
	     * @returns A Promise that resolves to an array of Proposal objects.
	     */
	    getProposals(start: string[] | number[], limit: number, order: 'by_creator' | 'by_start_date' | 'by_end_date' | 'by_total_votes', order_direction: 'ascending' | 'descending', status: 'all' | 'inactive' | 'active' | 'expired' | 'votable'): Promise<Proposal[]>;
	    /**
	     * Returns all proposal votes, starting with the specified voter or proposal.id.
	     * @param start - Depends on order (see below)
	     * - voter - voter of the proposal (account name string)
	     * - proposal.id - id the proposal (int)
	     * @param limit - The maximum number of proposals to return (max 1000).
	     * @param order - can be one of:
	     * - by_voter_proposal - order by proposal voter
	     * - by_proposal_voter - order by proposal.id
	     * @param order_direction - The direction in which to order the results. Can be ascending or descending
	     * @param status - The status of proposals to return.
	     * @returns A Promise that resolves to an array of ProposalVote objects.
	     */
	    getProposalVotes(start: string[] | number[], limit: number, order: 'by_voter_proposal' | 'by_proposal_voter', order_direction: 'ascending' | 'descending', status: 'all' | 'inactive' | 'active' | 'expired' | 'votable'): Promise<ProposalVote[]>;
	    /** Returns information about the current reward funds */
	    getRewardFund(fund: 'post'): Promise<RewardFund>;
	    /**
	     * Return all of the state required for a particular url path.
	     * @param path Path component of url conforming to condenser's scheme
	     *             e.g. `@beblurt` or `trending/travel`
	     */
	    getState(path: string): Promise<any>;
	    /** Returns the details of a transaction based on a transaction id. */
	    getTransaction(txId: string): Promise<SignedTransaction>;
	    /** return rpc node version */
	    getVersion(): Promise<object>;
	    /**
	     * Get list of delegations made by account.
	     * @param account Account delegating
	     * @param from Delegatee start offset, used for paging.
	     * @param limit Number of results, max 1000.
	     */
	    getVestingDelegations(account: string, from?: string, limit?: number): Promise<VestingDelegation[]>;
	    /** Returns the current witness schedule */
	    getWitnesseSchedule(): Promise<Witness[]>;
	    /** Returns the current number of witnesses */
	    getWitnessesCount(): Promise<Witness[]>;
	    /** Returns current witnesses by vote */
	    getWitnessesByVote(account: string | null, limit: number): Promise<Witness[]>;
	    /** Looks up accounts starting with name */
	    lookupAccounts(account: string, limit: number): Promise<string[]>;
	    /** Looks up accounts starting with name */
	    lookupWitnessAccounts(account: string, limit: number): Promise<string[]>;
	    /** Verify signed transaction. */
	    verifyAuthority(stx: SignedTransaction): Promise<boolean>;
	}

}
declare module 'dblurt/helpers/database' {
	/**
	 * @file Blurt Database API helpers.
	 * @author BeBlurt <https://beblurt.com/@beblurt>
	 * @description adaptation from Johan Nordberg <code@johan-nordberg.com> Database API helpers.
	 * @license
	 * Copyright (c) 2017 Johan Nordberg. All Rights Reserved.
	 *
	 * Redistribution and use in source and binary forms, with or without modification,
	 * are permitted provided that the following conditions are met:
	 *
	 *  1. Redistribution of source code must retain the above copyright notice, this
	 *     list of conditions and the following disclaimer.
	 *
	 *  2. Redistribution in binary form must reproduce the above copyright notice,
	 *     this list of conditions and the following disclaimer in the documentation
	 *     and/or other materials provided with the distribution.
	 *
	 *  3. Neither the name of the copyright holder nor the names of its contributors
	 *     may be used to endorse or promote products derived from this software without
	 *     specific prior written permission.
	 *
	 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
	 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
	 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
	 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
	 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
	 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
	 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
	 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
	 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
	 * OF THE POSSIBILITY OF SUCH DAMAGE.
	 *
	 * You acknowledge that this software is not designed, licensed or intended for use
	 * in the design, construction, operation or maintenance of any military facility.
	 */
	import { Account } from 'dblurt/chain/account';
	import { RpcNodeConfig } from 'dblurt/chain/misc';
	import { Client } from 'dblurt/client';
	export interface WitnessVotes {
	    id: number;
	    witness: string;
	    account: string;
	}
	export class DatabaseAPI {
	    readonly client: Client;
	    constructor(client: Client);
	    /** Convenience for calling `database_api`. */
	    call(method: string, params?: any[] | {
	        [key: string]: any;
	    }): Promise<any>;
	    /** Return server config. */
	    getConfig(): Promise<RpcNodeConfig>;
	    /**
	     * Return a list of accounts.
	     * @param start String or array for pagination
	     * @param limit Number of results, max 1000
	     * @param order by_next_vesting_withdrawal: in this case start is a date eg: ["1970-01-01T00:00:00", ""]
	     * @param delayed_votes_active Default true (optional)
	     */
	    getListAccounts(start: string | string[], limit: number, order: 'by_name' | 'by_proxy' | 'by_next_vesting_withdrawal', delayed_votes_active?: boolean): Promise<{
	        accounts: Account[];
	    }>;
	    /**
	     * Returns a list of witness votes.
	     * @param start Depends on order (see below)
	     * @param limit Number of results, max 1000
	     * @param order by_account_witness: start is an array of two values: account, witness |
	     *              by_witness_account: start is an array of two values: witness, account
	     * @example
	     * Voters of a witness
	     * ```typescript
	     * const accounts = await client.database.getListWitnessVotes(['nalexadre', null], 10, 'by_witness_account')
	     * ```
	     */
	    getListWitnessVotes(start: [string | null, string | null], limit: number, order: 'by_account_witness' | 'by_witness_account'): Promise<{
	        votes: WitnessVotes[];
	    }>;
	    /** return rpc node version */
	    getVersion(): Promise<object>;
	}

}
declare module 'dblurt/helpers/nexus' {
	/**
	 * @file Blurt Nexus (A.k.a. bridge) helpers.
	 * @author BeBlurt <https://beblurt.com/@beblurt>
	 * @description adaptation from Johan Nordberg <code@johan-nordberg.com> Database API helpers
	 * @license
	 * Copyright (c) 2022 BeBlurt. All Rights Reserved.
	 *
	 * Redistribution and use in source and binary forms, with or without modification,
	 * are permitted provided that the following conditions are met:
	 *
	 *  1. Redistribution of source code must retain the above copyright notice, this
	 *     list of conditions and the following disclaimer.
	 *
	 *  2. Redistribution in binary form must reproduce the above copyright notice,
	 *     this list of conditions and the following disclaimer in the documentation
	 *     and/or other materials provided with the distribution.
	 *
	 *  3. Neither the name of the copyright holder nor the names of its contributors
	 *     may be used to endorse or promote products derived from this software without
	 *     specific prior written permission.
	 *
	 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
	 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
	 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
	 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
	 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
	 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
	 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
	 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
	 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
	 * OF THE POSSIBILITY OF SUCH DAMAGE.
	 *
	 * You acknowledge that this software is not designed, licensed or intended for use
	 * in the design, construction, operation or maintenance of any military facility.
	 */
	import { NexusAccountNotifications, NexusPost, NexusCommunity, NexusCommunityExtended, NexusCommunityContext, NexusProfile, NexusPayoutStats, NexusSortGetRankedPost } from 'dblurt/chain/nexus';
	import { Client } from 'dblurt/client';
	export class Nexus {
	    readonly client: Client;
	    constructor(client: Client);
	    /** Convenience for calling `bridge`. */
	    call(method: string, params?: any[] | {
	        [key: string]: any;
	    }): Promise<any>;
	    /**
	     * Return array of account notification objects for the account passed.
	     *
	     * Returned values for type:
	     * - new_community - a new community was created
	     * - set_role - mod/admin adds a role to an account
	     * - set_props - properties set for a community
	     * - set_label - a title/badge/label has been set for an account
	     * - mute_post - a post has been muted, with a reason
	     * - unmute_post - a post has been unmuted, with a reason
	     * - pin_post - a post has been pinned
	     * - unpin_post - a post has been unpinned
	     * - flag_post - a post has been flagged by a member, with a reason
	     * - error - provides feedback to developers for ops that cannot be interpreted
	     * - subscribe - an account has subscribed to a community
	     * - reply - a post has been replied to
	     * - reblog - a post has been reblogged/reblogged
	     * - follow - an account has followed another account
	     * - mention - author mentions an account
	     * - vote - voter votes for an author
	     *
	     * The score value is based on the originating account’s rank.
	     * @param account The account to fetch.
	     * @param min_score Minimum score of notification, default: 25 [optional].
	     * @param last_id Last notification ID, paging mechanism [optional].
	     * @param limit Number of results, default: 100 [optional].
	     */
	    accountNotifications(account: string, min_score?: number, last_id?: number | null, limit?: number): Promise<NexusAccountNotifications[]>;
	    /**
	     * Return a list of posts related to a given account.
	     * @param sort Values accepted:
	     * - blog: top posts authored by given account (excluding posts to communities - unless explicitely reblogged) plus reblogs ranked by creation/reblog time.
	     * - feed: top posts from blogs of accounts that given account is following ranked by creation/reblog time, not older than last month.
	     * - posts: top posts authored by given account, newer first comments - replies authored by given account, newer first.
	     * - comments:
	     * - replies: replies to posts of given account, newer first.
	     * - payout: all posts authored by given account that were not yet cashed out.
	     * @param account The account to fetch.
	     * @param start_author Name of author to start from, used for paging. Should be used in conjunction with `start_permlink`.
	     * @param start_permlink Permalink of post to start from, used for paging. Should be used in conjunction with `start_author`.
	     * @param limit Number of results, max 1000.
	     * @param observer A valid account [optional].
	     */
	    getAccountPosts(sort: 'blog' | 'feed' | 'posts' | 'comments' | 'replies' | 'payout', account: string, start_author: string | null, start_permlink: string | null, limit: number, observer?: string | null): Promise<NexusPost[]>;
	    /**
	     * Gets the full community object. Includes metadata, leadership team. If `observer` is provided, get subcription status, user title, user role.
	     * @param name The community account to fetch.
	     * @param observer A valid account [optional].
	     */
	    getCommunity(name: string, observer?: string | null): Promise<NexusCommunityExtended>;
	    /**
	     * For a community/account: returns role, title, subscribed state.
	     * @param name The community account concerned.
	     * @param account The account to fetch.
	     */
	    getCommunityContext(name: string, account: string): Promise<NexusCommunityContext>;
	    /**
	     * Gives a flattened discussion tree starting at given post. Modified `get_state` thread implementation.
	     * Returns an Object whose key is "author/permlink".
	     */
	    getDiscussion(author: string, permlink: string): Promise<{
	        [key: string]: NexusPost;
	    }>;
	    /**
	     * Get payout stats for building treemap.
	     * @param limit Number of result, default: 250 [optional].
	     */
	    getPayoutStats(limit?: number): Promise<NexusPayoutStats>;
	    /**
	     * Fetch a single post.
	     */
	    getPost(author: string, permlink: string, observer?: string | null): Promise<NexusPost>;
	    /**
	     * Returns a resume of a profile with metadata parsed.
	     */
	    getProfile(account: string, observer?: string | null): Promise<NexusProfile>;
	    /**
	     * Query posts, sorted by given method.
	     * @param sort Sorting of results, valid options are: 'trending', 'hot', 'created', 'promoted', 'payout', 'payout_comments', 'muted'.
	     * @param start_author Name of author to start from, used for paging. Should be used in conjunction with `start_permlink` [optional].
	     * @param start_permlink Permalink of post to start from, used for paging. Should be used in conjunction with `start_author` [optional].
	     * @param limit Number of results, default: 20 [optional].
	     * @param tag Specify a Tag [optional].
	     * @param observer A valid account [optional].
	     */
	    getRankedPosts(sort: NexusSortGetRankedPost, start_author?: string | null, start_permlink?: string | null, limit?: number, tag?: string | null, observer?: string | null): Promise<NexusPost[]>;
	    /**
	     * Returns a lists of all communities `account` subscribes to, plus role and title in each.
	     * The content of a community `account` subscribes to is [community name, community title, role_id, title].
	     * @param account The account to fetch.
	     */
	    listAllSubscriptions(account: string): Promise<[string, string, string, string | null][]>;
	    /**
	     * Returns a list of communities.
	     * @param last Name of community, paging mechanism [optional].
	     * @param limit Number of listed communities, default: 100 [optional].
	     * @param query Filters against title and about community fields [optional].
	     * @param sort Sorting of results, default: rank [optional].
	     * - rank: sort by community rank.
	     * - new: sort by newest community.
	     * - subs: sort by subscriptions.
	     * @param observer A valid account [optional].
	     */
	    listCommunities(last?: string | null, limit?: number, query?: string | null, sort?: 'rank' | 'new' | 'subs', observer?: string | null): Promise<NexusCommunity[]>;
	    /**
	     * Returns a list of community account-roles (anyone with non-guest status).
	     * The content of a community account-roles array is [community name, role_id, title].
	     * @param community Community category name (account).
	     * @param last Name of subscriber, paging mechanism [optional].
	     * @param limit limit Number of listed communities, default: 100 [optional].
	     */
	    listCommunityRoles(community: string, last?: string | null, limit?: number): Promise<[string, string, string | null][]>;
	    /**
	     * Returns a list communities by new subscriber count.
	     * The content of a community array is [community name, community title].
	     * @param limit Number of listed communities, default: 25 [optional].
	     */
	    listPopComunities(limit?: number): Promise<[string, string][]>;
	    /**
	     * Returns a list of subscribers for a given community.
	     * The content of a subscriber array is [account, role_id, title, created_at].
	     * @param community Community category name (account).
	     * @param last  Last account, paging mechanism [optional].
	     * @param limit Number of results, default: 100 [optional].
	     */
	    listSubscribers(community: string, last: string | null, limit?: number): Promise<[string, string, string | null, string][]>;
	    /**
	     * Load notifications for a specific post.
	     * @param author Name of author.
	     * @param permlink Permlink of post.
	     * @param min_score Minimum score of notification, default: 25 [optional].
	     * @param last_id Last notification ID, paging mechanism [optional].
	     * @param limit Number of results, default: 100 [optional].
	     */
	    postNotifications(author: string, permlink: string, min_score?: number, last_id?: number | null, limit?: number): Promise<NexusAccountNotifications[]>;
	    /**
	     * Load notifications for a specific post.
	     * @param account The account to fetch.
	     * @param min_score Minimum score of notification, default: 25 [optional].
	     */
	    unreadNotifications(account: string, min_score?: number): Promise<{
	        lastread: string;
	        unread: number;
	    }>;
	}

}
declare module 'dblurt/helpers/tools' {
	/**
	 * @file Account helpers.
	 * @author BeBlurt <https://beblurt.com/@beblurt>
	 * @description Special account functions
	 * @license
	 * Copyright (c) 2022 BeBlurt. All Rights Reserved.
	 *
	 * Redistribution and use in source and binary forms, with or without modification,
	 * are permitted provided that the following conditions are met:
	 *
	 *  1. Redistribution of source code must retain the above copyright notice, this
	 *     list of conditions and the following disclaimer.
	 *
	 *  2. Redistribution in binary form must reproduce the above copyright notice,
	 *     this list of conditions and the following disclaimer in the documentation
	 *     and/or other materials provided with the distribution.
	 *
	 *  3. Neither the name of the copyright holder nor the names of its contributors
	 *     may be used to endorse or promote products derived from this software without
	 *     specific prior written permission.
	 *
	 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
	 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
	 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
	 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
	 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
	 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
	 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
	 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
	 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
	 * OF THE POSSIBILITY OF SUCH DAMAGE.
	 *
	 * You acknowledge that this software is not designed, licensed or intended for use
	 * in the design, construction, operation or maintenance of any military facility.
	 */
	/// <reference types="node" />
	import { DynamicGlobalProperties, RewardFund, WitnessSetProperties } from 'dblurt/chain/misc';
	import { WitnessSetPropertiesOperation } from 'dblurt/chain/operation';
	import { Serializer } from 'dblurt/chain/serializer';
	import { Client } from 'dblurt/client';
	export class Tools {
	    readonly client: Client;
	    constructor(client: Client);
	    /** Convenience for calling `condenser_api`. */
	    call(method: string, params?: any[] | {
	        [key: string]: any;
	    }): Promise<any>;
	    /**
	     * Get Mana of an account.
	     * @param account Account of user
	     */
	    getAccountMana(name: string): Promise<{
	        current_mana: number;
	        max_mana: number;
	    }>;
	    /**
	     * Get the Vote value of an account for a specific post.
	     * @param account Account of user.
	     * @param voteWeight % of the vote.
	     * @param net_rshares Net Rshares of the post.
	     * @param cashout_time Cashout time of the post (in millisecond).
	     */
	    getAccountVoteValue(voteWeight: number, mana: {
	        current_mana: number;
	        max_mana: number;
	    }, net_rshares: string | number, cashout_time: number, DGP: DynamicGlobalProperties, REWARD_FUND: RewardFund): number;
	    /**
	     * Get the conversion of VESTS to BLURT.
	     * @param VESTS VESTS value.
	     * @param DGP Dynamic Global Properties. See {@link getDynamicGlobalProperties}
	     */
	    convertVESTS(VESTS: number, DGP: DynamicGlobalProperties): number;
	    serialize(serializer: Serializer, data: any): Buffer;
	    buildWitnessSetPropertiesOp(owner: string, props: WitnessSetProperties): WitnessSetPropertiesOperation;
	}

}
declare module 'dblurt' {
	/**
	 * @file dblurt exports.
	 * @author Johan Nordberg <code@johan-nordberg.com>
	 * @license
	 * Copyright (c) 2017 Johan Nordberg. All Rights Reserved.
	 *
	 * Redistribution and use in source and binary forms, with or without modification,
	 * are permitted provided that the following conditions are met:
	 *
	 *  1. Redistribution of source code must retain the above copyright notice, this
	 *     list of conditions and the following disclaimer.
	 *
	 *  2. Redistribution in binary form must reproduce the above copyright notice,
	 *     this list of conditions and the following disclaimer in the documentation
	 *     and/or other materials provided with the distribution.
	 *
	 *  3. Neither the name of the copyright holder nor the names of its contributors
	 *     may be used to endorse or promote products derived from this software without
	 *     specific prior written permission.
	 *
	 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
	 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
	 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
	 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
	 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
	 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
	 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
	 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
	 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
	 * OF THE POSSIBILITY OF SUCH DAMAGE.
	 *
	 * You acknowledge that this software is not designed, licensed or intended for use
	 * in the design, construction, operation or maintenance of any military facility.
	 */
	import * as utils from 'dblurt/utils';
	export { utils };
	export * from 'dblurt/helpers/account_history';
	export * from 'dblurt/helpers/blockchain';
	export * from 'dblurt/helpers/condenser';
	export * from 'dblurt/helpers/database';
	export * from 'dblurt/helpers/nexus';
	export * from 'dblurt/helpers/tools';
	export * from 'dblurt/chain/account';
	export * from 'dblurt/chain/asset';
	export * from 'dblurt/chain/block';
	export * from 'dblurt/chain/blog';
	export * from 'dblurt/chain/comment';
	export * from 'dblurt/chain/deserializer';
	export * from 'dblurt/chain/nexus';
	export * from 'dblurt/chain/misc';
	export * from 'dblurt/chain/proposal';
	export * from 'dblurt/chain/operation';
	export * from 'dblurt/chain/serializer';
	export * from 'dblurt/chain/transaction';
	export * from 'dblurt/chain/witness';
	export * from 'dblurt/client';
	export * from 'dblurt/crypto';

}
declare module 'dblurt/index-browser' {
	/**
	 * @file dblurt entry point for browsers.
	 * @author Johan Nordberg <code@johan-nordberg.com>
	 * @license
	 * Copyright (c) 2017 Johan Nordberg. All Rights Reserved.
	 *
	 * Redistribution and use in source and binary forms, with or without modification,
	 * are permitted provided that the following conditions are met:
	 *
	 *  1. Redistribution of source code must retain the above copyright notice, this
	 *     list of conditions and the following disclaimer.
	 *
	 *  2. Redistribution in binary form must reproduce the above copyright notice,
	 *     this list of conditions and the following disclaimer in the documentation
	 *     and/or other materials provided with the distribution.
	 *
	 *  3. Neither the name of the copyright holder nor the names of its contributors
	 *     may be used to endorse or promote products derived from this software without
	 *     specific prior written permission.
	 *
	 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
	 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
	 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
	 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
	 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
	 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
	 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
	 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
	 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
	 * OF THE POSSIBILITY OF SUCH DAMAGE.
	 *
	 * You acknowledge that this software is not designed, licensed or intended for use
	 * in the design, construction, operation or maintenance of any military facility.
	 */
	import 'core-js/features/array/from';
	import 'core-js/features/map';
	import 'core-js/features/number';
	import 'core-js/features/promise';
	import 'core-js/features/symbol';
	import 'core-js/features/symbol/async-iterator';
	import 'whatwg-fetch';
	export * from 'dblurt';

}
declare module 'dblurt/client' {
	/// <reference types="node" />
	import { AccountHistoryAPI } from 'dblurt/helpers/account_history';
	import { Blockchain } from 'dblurt/helpers/blockchain';
	import { BroadcastAPI } from 'dblurt/helpers/broadcast';
	import { CondenserAPI } from 'dblurt/index-browser';
	import { DatabaseAPI } from 'dblurt/helpers/database';
	import { Nexus } from 'dblurt/helpers/nexus';
	import { Tools } from 'dblurt/helpers/tools';
	/** Library version. */
	export const VERSION: string;
	/** Main Blurt network chain id */
	export const DEFAULT_CHAIN_ID: Buffer;
	/** Main Blurt network address prefix */
	export const DEFAULT_ADDRESS_PREFIX = "BLT";
	/**
	 * RPC Client options
	 * ------------------
	 */
	export interface ClientOptions {
	    /** Blurt chain id. Defaults `cd8d90f29ae273abec3eaa7731e25934c63eb654d55080caff2ebb7f5df6381f` */
	    chainId?: string;
	    /** Blurt address prefix. Defaults to main network: `BLT`*/
	    addressPrefix?: string;
	    /** how long to wait in milliseconds before giving up on a rpc call. Defaults to 60 * 1000 ms. */
	    timeout?: number;
	    /**
	     * Specifies the amount of times the urls (RPC nodes) should be
	     * iterated and retried in case of timeout errors.
	     * (important) Requires url parameter to be an array (string[])!
	     * Can be set to 0 to iterate and retry forever. Defaults to 3 rounds.
	     */
	    failoverThreshold?: number;
	    /** Whether a console.log should be made when RPC failed over to another one */
	    consoleOnFailover?: boolean;
	    /** Retry backoff function, returns milliseconds. Default = {@link defaultBackoff}. */
	    backoff?: (tries: number) => number;
	    /** Node.js http(s) agent, use if you want http keep-alive. @see https://nodejs.org/api/http.html#http_new_agent_options. */
	    agent?: any;
	}
	/**
	 * RPC Client
	 * ----------
	 * Can be used in both node.js and the browser. Also see {@link ClientOptions}.
	 */
	export class Client {
	    /** Client options, *read-only*. */
	    readonly options: ClientOptions;
	    /** Address to Blurt RPC server. String or String[] *read-only* */
	    address: string | string[];
	    /** Account History API helper */
	    readonly accountHistory: AccountHistoryAPI;
	    /** Blockchain helper */
	    readonly blockchain: Blockchain;
	    /** Broadcast API helper */
	    readonly broadcast: BroadcastAPI;
	    /** Condenser API helper */
	    readonly condenser: CondenserAPI;
	    /** Database API helper. */
	    readonly database: DatabaseAPI;
	    /** Nexus (A.k.a. Bridge) helper */
	    readonly nexus: Nexus;
	    /** Tools helper */
	    readonly tools: Tools;
	    /** Chain ID for current network. */
	    readonly chainId: Buffer;
	    /** Address prefix for current network. */
	    readonly addressPrefix: string;
	    currentAddress: string;
	    private timeout;
	    private backoff;
	    private failoverThreshold;
	    private consoleOnFailover;
	    /**
	     * @param address The address to the Blurt RPC server,
	     * e.g. `https://rpc.blurt.world`. or [`https://rpc.blurt.world`, `https://another.api.com`]
	     * @param options Client options.
	     */
	    constructor(address: string | string[], options?: ClientOptions);
	    /**
	     * Make a RPC call to the server.
	     *
	     * @param api     The API to call, e.g. `database_api`.
	     * @param method  The API method, e.g. `get_dynamic_global_properties`.
	     * @param params  Array of parameters to pass to the method, optional.
	     *
	     */
	    call(api: string, method: string, params?: any): Promise<any>;
	}

}
declare module 'dblurt/index-node' {
	/**
	 * @file dblurt entry point for node.js.
	 * @author Johan Nordberg <code@johan-nordberg.com>
	 * @license
	 * Copyright (c) 2017 Johan Nordberg. All Rights Reserved.
	 *
	 * Redistribution and use in source and binary forms, with or without modification,
	 * are permitted provided that the following conditions are met:
	 *
	 *  1. Redistribution of source code must retain the above copyright notice, this
	 *     list of conditions and the following disclaimer.
	 *
	 *  2. Redistribution in binary form must reproduce the above copyright notice,
	 *     this list of conditions and the following disclaimer in the documentation
	 *     and/or other materials provided with the distribution.
	 *
	 *  3. Neither the name of the copyright holder nor the names of its contributors
	 *     may be used to endorse or promote products derived from this software without
	 *     specific prior written permission.
	 *
	 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
	 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
	 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
	 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
	 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
	 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
	 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
	 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
	 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
	 * OF THE POSSIBILITY OF SUCH DAMAGE.
	 *
	 * You acknowledge that this software is not designed, licensed or intended for use
	 * in the design, construction, operation or maintenance of any military facility.
	 */
	export * from 'dblurt';

}
declare module 'dblurt/chain/broadcast' {
	/**
	 * @file Blurt Broadcast type definitions and helpers.
	 * @author BeBlurt <https://beblurt.com/@beblurt>
	 * @description adaptation from Johan Nordberg <code@johan-nordberg.com> Blurt account type definitions.
	 * @license
	 * Copyright (c) 2017 Johan Nordberg. All Rights Reserved.
	 *
	 * Redistribution and use in source and binary forms, with or without modification,
	 * are permitted provided that the following conditions are met:
	 *
	 *  1. Redistribution of source code must retain the above copyright notice, this
	 *     list of conditions and the following disclaimer.
	 *
	 *  2. Redistribution in binary form must reproduce the above copyright notice,
	 *     this list of conditions and the following disclaimer in the documentation
	 *     and/or other materials provided with the distribution.
	 *
	 *  3. Neither the name of the copyright holder nor the names of its contributors
	 *     may be used to endorse or promote products derived from this software without
	 *     specific prior written permission.
	 *
	 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
	 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
	 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
	 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
	 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
	 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
	 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
	 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
	 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
	 * OF THE POSSIBILITY OF SUCH DAMAGE.
	 *
	 * You acknowledge that this software is not designed, licensed or intended for use
	 * in the design, construction, operation or maintenance of any military facility.
	 */
	import { Asset } from 'dblurt/chain/asset';
	import { AuthorityType } from 'dblurt/chain/account';
	export interface BroadcastAccount {
	    creator: string;
	    new_account_name: string;
	    fee: string | Asset;
	    owner: AuthorityType;
	    active: AuthorityType;
	    posting: AuthorityType;
	    memo_key: string;
	    json_metadata: string;
	}

}
